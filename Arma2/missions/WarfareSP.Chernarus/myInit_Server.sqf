scriptName format ["%1Scripts\Server\Init\Init_Server.sqf",BIS_WFdPath];
//Last modified 10/29/10
//*****************************************************************************************
//Description: This is run by the server when it starts a mission.
//*****************************************************************************************
//Compile function loops.
BIS_WF_StrategicUpdateCommander = Compile PreprocessFile (corepath + "Server\AI\Commander\Commander_StrategicUpdate.sqf");
BIS_WF_UpdateBaseVehicle = Compile PreprocessFile (corepath + "Server\Server_UpdateBaseVehicle.sqf");
BIS_WF_UpdateCamp = Compile PreprocessFile (corepath + "Server\Server_UpdateCamp.sqf");
BIS_WF_UpdateConstructedCamp = Compile PreprocessFile (corepath + "Server\Server_UpdateConstructedCamp.sqf");
BIS_WF_UpdateCleanup = Compile PreprocessFile (corepath + "Server\Server_UpdateCleanup.sqf");
BIS_WF_UpdateResources = Compile PreprocessFile (corepath + "Server\Server_UpdateResources.sqf");
BIS_WF_UpdateTownVirtualDefenses = Compile PreprocessFile (corepath + "Server\Server_UpdateTownVirtualDefenses.sqf");
BIS_WF_UpdateCommander = Compile PreprocessFile (corepath + "Server\AI\Commander\Commander_Update.sqf");
BIS_WF_UpdateCommandsFromClients = Compile PreprocessFile (corepath + "Server\Server_UpdateCommandsFromClients.sqf");
BIS_WF_UpdateSalvager = Compile PreprocessFile (corepath + "Server\Server_UpdateSalvager.sqf");
BIS_WF_UpdateSalvagerAI = Compile PreprocessFile (corepath + "Server\AI\AI_Salvager.sqf");
BIS_WF_UpdateSupplyTruckAI = Compile PreprocessFile (corepath + "Server\AI\AI_SupplyTruck.sqf");
BIS_WF_UpdateServer = Compile PreprocessFile (corepath + "Server\Server_Update.sqf");
BIS_WF_UpdateServerTime = Compile PreprocessFile (corepath + "Server\Server_UpdateTime.sqf");
BIS_WF_UpdateTeam = Compile PreprocessFile (corepath + "Server\AI\Team\Team_Update.sqf");
BIS_WF_UpdateTown = Compile PreprocessFile (corepath + "Server\Server_UpdateTown.sqf");
BIS_WF_UpdateTownPatrols = Compile PreprocessFile (corepath + "Server\Server_UpdateTownPatrols.sqf");
BIS_WF_UpdateTransport = Compile PreprocessFile (corepath + "Server\AI\AI_Transport.sqf");
BIS_WF_UpdateVehicleServicePoint = Compile PreprocessFile (corepath + "Server\Server_UpdateVehicleServicePoint.sqf");
BIS_WF_VoteForCommander = Compile PreprocessFile (corepath + "Server\Functions\Server_VoteForCommander.sqf");

//Compile regular functions
BIS_WF_AddFundsToSide = Compile PreprocessFile (corepath + "Server\Functions\Server_AddFundsToSide.sqs");
BIS_WF_AITeamBuyUnits = Compile PreprocessFile (corepath + "Server\Functions\Server_AITeamBuyUnits.sqf");
BIS_WF_AITeamDisembarkTransport = Compile PreprocessFile (corepath + "Server\Functions\Server_AITeamDisembarkTransport.sqf");
BIS_WF_AITeamMove = Compile PreprocessFile (corepath + "Server\Functions\Server_AITeamMove.sqf");
BIS_WF_AITeamMoveByType = Compile PreprocessFile (corepath + "Server\Functions\Server_AITeamMoveByType.sqf");
BIS_WF_AITeamFastTravel = Compile PreprocessFile (corepath + "Server\Functions\Server_AITeamFastTravel.sqf");
BIS_WF_AIVehicleAbandoned = Compile PreprocessFile (corepath + "Server\Functions\Server_AIVehicleAbandoned.sqf");
BIS_WF_BaseStructuresExist = Compile PreprocessFile (corepath + "Server\Functions\Server_BaseStructuresExist.sqf");
BIS_WF_BuyServerUnit = Compile PreprocessFile (corepath + "Server\Functions\Server_BuyUnit.sqf");
BIS_WF_CommanderBuildTownDefenses = Compile PreprocessFile (corepath + "Server\AI\Commander\Commander_BuildTownDefenses.sqf");
BIS_WF_GetAITeamID = Compile PreprocessFile (corepath + "Server\Functions\Server_GetAITeamID.sqf");
BIS_WF_GetLocations = Compile PreprocessFile (corepath + "Server\Functions\Server_GetLocations.sqf");
BIS_WF_InitCamp = Compile PreprocessFile (corepath + "Server\Init\Init_Camp.sqf");
BIS_WF_InitDefenseLayout = Compile PreprocessFile (corepath + "Server\Init\Init_DefenseLayout.sqf");
BIS_WF_PlaceSides = Compile PreprocessFile (corepath + "Server\Functions\Server_PlaceSides.sqf");
BIS_WF_SideMessage = Compile PreprocessFile (corepath + "Server\Functions\Server_SideMessage.sqf");
BIS_WF_SideMessageChat = Compile PreprocessFile (corepath + "Server\Functions\Server_SideMessageChat.sqf");
BIS_WF_SendGameStats = Compile PreprocessFile (corepath + "Server\Functions\Server_SendGameStats.sqf");
BIS_WF_SetCampsToSide = Compile PreprocessFile (corepath + "Server\Functions\Server_SetCampsToSide.sqf");
BIS_WF_SetTeamMaxSize = Compile PreprocessFile (corepath + "Server\Functions\Server_SetTeamMaxSize.sqf");
BIS_WF_StructureDamaged = Compile PreprocessFile (corepath + "Server\Functions\Server_StructureDamaged.sqf");
BIS_WF_StructureDestroyed = Compile PreprocessFile (corepath + "Server\Functions\Server_StructureDestroyed.sqf");
BIS_WF_TeamDefendBaseAction = Compile PreprocessFile (corepath + "Server\AI\Team\Team_DefendBaseAction.sqf");
BIS_WF_TeamFindFactoriesAction = Compile PreprocessFile (corepath + "Server\AI\Team\Team_FindFactoriesAction.sqf");
BIS_WF_TeamReinforceAction = Compile PreprocessFile (corepath + "Server\AI\Team\Team_ReinforceAction.sqf");
BIS_WF_TeamUpdateRespawn = Compile PreprocessFile (corepath + "Server\AI\Team\Team_UpdateRespawn.sqf");
BIS_WF_TeamUpgradeAction = Compile PreprocessFile (corepath + "Server\AI\Team\Team_UpgradeAction.sqf");

BIS_WF_OldSupportUsed = Compile PreprocessFile (corepath + "Server\Functions\Server_SupportUsed.sqf");
BIS_WF_OldSupportUpdate = Compile PreprocessFile (corepath + "Server\Functions\Server_SupportUpdate.sqf");

//Should be called with spawn.
BIS_WF_CommandToClient = Compile PreprocessFile (corepath + "Server\Functions\Server_CommandToClient.sqf");
BIS_WF_CommandToClients = Compile PreprocessFile (corepath + "Server\Functions\Server_CommandToClients.sqf");
BIS_WF_CommandToSide = Compile PreprocessFile (corepath + "Server\Functions\Server_CommandToSide.sqf");

_recompile = false;
if (Count _this > 0) then {_recompile = _this Select 0};
if (_recompile) ExitWith {};

//Create centers for any empty sides.
{
	Private["_sideCreated"];
	_sideCreated = _x;

	if ((_x CountSide AllUnits) < 1) then
	{
		CreateCenter _x;
		{
			//Set new side enemy to all sides.
			if (_x != _sideCreated) then
			{
				_x SetFriend [_sideCreated,0];
				_sideCreated SetFriend [_x,0];
			};
		} ForEach [East,West,Resistance];
	};
} ForEach [East,West,Resistance];

//HQ identities
BIS_WF_HQEASTgrp = createGroup east;
BIS_WF_HQWESTgrp = createGroup west;
BIS_WF_HQRESISTANCEgrp = createGroup resistance;

BIS_WF_HQEASTgrp2 = createGroup east;
BIS_WF_HQWESTgrp2 = createGroup west;
BIS_WF_HQRESISTANCEgrp2 = createGroup resistance;

BIS_WF_HQEAST = BIS_WF_GroupLogic CreateUnit ["Logic",[1000,10,0],[],0,"NONE"]; [BIS_WF_HQEAST] joinSilent BIS_WF_HQEASTgrp;
BIS_WF_HQWEST = BIS_WF_GroupLogic CreateUnit ["Logic",[1000,10,0],[],0,"NONE"]; [BIS_WF_HQWEST] joinSilent BIS_WF_HQWESTgrp;
BIS_WF_HQRESISTANCE = BIS_WF_GroupLogic CreateUnit ["Logic",[1000,10,0],[],0,"NONE"]; [BIS_WF_HQRESISTANCE] joinSilent BIS_WF_HQRESISTANCEgrp;

BIS_WF_HQEAST2 = BIS_WF_GroupLogic CreateUnit ["Logic",[1000,10,0],[],0,"NONE"]; [BIS_WF_HQEAST2] joinSilent BIS_WF_HQEASTgrp2;
BIS_WF_HQWEST2 = BIS_WF_GroupLogic CreateUnit ["Logic",[1000,10,0],[],0,"NONE"]; [BIS_WF_HQWEST2] joinSilent BIS_WF_HQWESTgrp2;
BIS_WF_HQRESISTANCE2 = BIS_WF_GroupLogic CreateUnit ["Logic",[1000,10,0],[],0,"NONE"]; [BIS_WF_HQRESISTANCE2] joinSilent BIS_WF_HQRESISTANCEgrp2;

//Set identity for voice announcements.
BIS_WF_HQEAST setVariable ["_topic_identity",BIS_WF_Common GetVariable "EastHQIdentity",true];
BIS_WF_HQWEST setVariable ["_topic_identity",BIS_WF_Common GetVariable "WestHQIdentity",true];
BIS_WF_HQRESISTANCE setVariable ["_topic_identity",BIS_WF_Common GetVariable "ResistanceHQIdentity",true];

{
  _x setIdentity ((_x getVariable "_topic_identity") + str (floor random 3));
  _x setRank "COLONEL";
  _x setGroupId [localize "STR_DN_WARFARE_HQ_BASE_UNFOLDED"]; //also on client?!
  _x kbAddTopic [(_x getVariable "_topic_identity"),  "\ca\warfare2\kb\hq.bikb",  "\ca\warfare2\kb\hq.fsm", {call compile preprocessFileLineNumbers "\ca\warfare2\kb\hq.sqf"}];  
} forEach [BIS_WF_HQEAST, BIS_WF_HQWEST, BIS_WF_HQRESISTANCE];

{
  publicVariable _x; //publicvariabling so other clients can set identity to HQ
} forEach ["BIS_WF_HQEAST", "BIS_WF_HQWEST", "BIS_WF_HQRESISTANCE"];



if (IsNil {BIS_WF_Common GetVariable "customTownsScript"}) then {BIS_WF_Common SetVariable ["customTownsScript",""]};

if ((BIS_WF_Common GetVariable "customTownsScript") != "") then
{
	[] Call Compile PreprocessFile (BIS_WF_Common GetVariable "customTownsScript");
}
else
{
	[] Call Compile PreprocessFile (corepath + "Server\Config\Config_MapTowns.sqf");
};

//for missions that check that base is destroyed
BIS_WF_EastBaseDestroyed = false;
BIS_WF_WestBaseDestroyed = false;
BIS_WF_ResistanceBaseDestroyed = false;

westKnownSalvage = [];
eastKnownSalvage = [];
resistanceKnownSalvage = [];

eastAISalvagers = [];
westAISalvagers = [];
resistanceAISalvagers = [];

eastAISupplyTrucks = [];
westAISupplyTrucks = [];
resistanceAISupplyTrucks = [];

eastBaseStructures = [];
westBaseStructures = [];
resistanceBaseStructures = [];

_eastVehiclePool = [];
_westVehiclePool = [];
_resistanceVehiclePool = [];

EastPoints = 0;
WestPoints = 0;
ResistancePoints = 0;

PublicVariable "EastPoints";
PublicVariable "WestPoints";
PublicVariable "ResistancePoints";

//BIS_WF_Server = BIS_WF_GroupLogic CreateUnit ["Logic",[1000,10,0],[],0,"NONE"];

//Objects that must be destroyed to win game.
BIS_WF_Server SetVariable ["WestMissionObjects",[]];	//Objects that belong to West.
BIS_WF_Server SetVariable ["EastMissionObjects",[]];
BIS_WF_Server SetVariable ["ResistanceMissionObjects",[]];

//Enemy structures that have been spotted.
BIS_WF_Server SetVariable ["WestSpottedStructures",[]];
BIS_WF_Server SetVariable ["EastSpottedStructures",[]];
BIS_WF_Server SetVariable ["ResistanceSpottedStructures",[]];

BIS_WF_Server SetVariable ["WestBaseTimeUnderAttack",-10000];
BIS_WF_Server SetVariable ["EastBaseTimeUnderAttack",-10000];
BIS_WF_Server SetVariable ["ResistanceBaseTimeUnderAttack",-10000];

//Average area of influence (presence) by teams.
BIS_WF_Server SetVariable ["EastAverageAreaOfInfluence",[0,0]];
BIS_WF_Server SetVariable ["WestAverageAreaOfInfluence",[0,0]];
BIS_WF_Server SetVariable ["ResistanceAverageAreaOfInfluence",[0,0]];

//Hostile candidates for generating missions potentially far from the base.
//Examples: An enemy base, or a town near the average area of influence.
BIS_WF_Server SetVariable ["EastExpeditionaryHostileLocations",[]];
BIS_WF_Server SetVariable ["WestExpeditionaryHostileLocations",[]];
BIS_WF_Server SetVariable ["ResistanceExpeditionaryHostileLocations",[]];

BIS_WF_Server SetVariable ["disconnectedPlayerData",[]];

//Sides that will not spawn defenses in towns.
if (IsNil {BIS_WF_Server GetVariable "disabledTownSpawnSides"}) then {BIS_WF_Server SetVariable ["disabledTownSpawnSides",[]]};

Private["_group","_logic"];

CreateCenter SideLogic;

if (IsNil "BIS_functions_mainscope") then
{
	if (BIS_WF_Classic) then
	{
		_logic = BIS_WF_GroupLogic CreateUnit ["FunctionsManager",[1000,10,0],[],0,"NONE"];
	};
};

//Create any missing modules.
if (!BIS_WF_Classic) then
{ 
  activateAddons["CA_HighCommand"];
  
	if (IsNil "BIS_HC_mainscope") then	{		
		_logic = BIS_WF_GroupLogic CreateUnit ["HighCommand",[1000,10,0],[],0,"NONE"];
		_logic SetVariable ["addAllGroups",false];
		BIS_HC_mainscope = _logic;
		publicVariable "BIS_HC_mainscope";
	};
};

//INIT WF TEAMS (OLD METHOD).

_eastTotal = 0;
_westTotal = 0;
_resistanceTotal = 0;

//Find all playable positions.
for [{_count = 1},{_count < 32},{_count = _count + 1}] do
{
	if (!IsNil Format["EastSlot%1",_count]) then {_eastTotal = _eastTotal + 1};
	if (!IsNil Format["WestSlot%1",_count]) then {_westTotal = _westTotal + 1};
	if (!IsNil Format["ResistanceSlot%1",_count]) then {_resistanceTotal = _resistanceTotal + 1};
};

EastTeams = [];
WestTeams = [];
ResistanceTeams = [];

EastDefenseTeams = [];
WestDefenseTeams = [];
ResistanceDefenseTeams = [];

//Any sides defeated in a 3 sided game.
BIS_WF_Common SetVariable ["defeatedSides",[],true];

//INIT WF TEAMS, STRUCTURES, AND VEHICLES.
BIS_WF_CustomLocations = [];//Editor-placed locations (behave like a town).

_customCamps = [];			//Editor-placed camps. Will be owned by nearest town in range.
_customOwners = [];			//Editor-placed ownership. Will set nearest town in range to specified owner.
_customStartingLocations = [];	//Editor-placed locations that sides can start at.

//If no teams have been defined by the old method then check for teams synchronized to Warfare logic.
if (_eastTotal == 0 && _westTotal == 0 && _resistanceTotal == 0) then
{
	Private["_camps","_nearLocations","_objects"];
	_objects = SynchronizedObjects BIS_WF_Common;

	//Get any custom locations.
	{
		if (TypeOf _x == "LocationLogicCity" && !IsNil {_x GetVariable "name"}) then
		{
			BIS_WF_CustomLocations = BIS_WF_CustomLocations + [_x];
			_location = _x;
			_camps = [];
			_nearLocations = [];

			//Get logics for location.
			{
				switch (TypeOf _x) do
				{
					case "LocationLogicCamp": {_camps = _camps + [_x]};
					case "LocationLogicCityLink": {_nearLocations = _nearLocations + [_x]};
					case "LocationLogicCityFlatArea": {_location SetVariable["fastTravelArea",_x,true]};
					case "LocationLogicDepot": {_location SetVariable["depot",_x,true]};
					case "LocationLogicOwnerCivilian": {_location SetVariable["side",Civilian]};
					case "LocationLogicOwnerEast": {_location SetVariable["side",East]};
					case "LocationLogicOwnerWest": {_location SetVariable["side",West]};
					case "LocationLogicOwnerResistance": {_location SetVariable["side",Resistance]};
				};
			} ForEach SynchronizedObjects _location;

			_location SetVariable ["camps",_camps,true];
			_location SetVariable ["neighborLinks",_nearLocations];
		}
		else
		{
			switch (TypeOf _x) do
			{
				//Camp will be owned by nearest town in range.
				case "LocationLogicCamp": {_customCamps = _customCamps + [_x]};

				//If owner is not attached to object then it can be used to set nearest town's ownership.
				case "LocationLogicOwnerCivilian": {_customOwners = _customOwners + [_x]};
				case "LocationLogicOwnerEast": {_customOwners = _customOwners + [_x]};
				case "LocationLogicOwnerWest": {_customOwners = _customOwners + [_x]};
				case "LocationLogicOwnerResistance": {_customOwners = _customOwners + [_x]};

				//Custom starting location.
				case "LocationLogicStartingLocation": {_customStartingLocations = _customStartingLocations + [_x]};
			};
		};
	} ForEach _objects;

	{
		Private["_count","_objects","_side","_teams","_vehicles"];

		_teams = [];
		_defenseTeams = [];
		_objects = SynchronizedObjects BIS_WF_Common;

		_side = _x;
		_count = 0;
		_vehicles = [];

		{
			//Check to add a team or base defense team.
			if (_x Call BIS_WF_IsSoldier) then
			{
				//Add team to side (if not already added).
				if (Call Compile Format["Side _x == %1 && !(Group _x In (_teams + _defenseTeams))",_side]) then
				{
					if (IsNil {Group _x GetVariable "dedicatedMission"}) then
					{
						_teams = _teams + [Group _x];
						_count = _count + 1;
						Call Compile Format["%1Team%2 = Group _x;PublicVariable ""%1Team%2""",_side,_count];
					}
					else
					{
						_defenseTeams = _defenseTeams + [Group _x];
					};
				};
			}
			//Check to add a base structure, or base pool vehicle.
			else
			{
				//Get any MHQs.
				if (Call Compile Format["TypeOf Vehicle _x == %1MHQNAME || (TypeOf Vehicle _x In %1StructureNames)",_side]) then
				{
					if (Call Compile Format["TypeOf Vehicle _x == %1MHQNAME",_side]) then
					{
						Call Compile Format["%1MHQ = Vehicle _x",_side];
						[0,0,Vehicle _x,true] Call SRVFNCRequestVehicleLock;
					}
					//Add any prebuilt base structures.
					else
					{
						Private["_objectives","_type"];
						_type = Call Compile Format["%1StructureNames Find TypeOf _x",_side];
						_x SetVehicleInit Format["[this,%1,%2] spawn Compile PreprocessFile (corePath + ""Client\Init\Init_BaseStructure.sqf"")",_type,GetDir _x];
						_x AddEventHandler ["hit",{_this Call BIS_WF_StructureDamaged}];
						_x AddEventHandler ["killed",{_this Call BIS_WF_StructureDestroyed}];

						Call Compile Format["%1BaseStructures = %1BaseStructures + [_x]",_side];

						//Set default allowed factions for structure (unless already set in editor).
						if (IsNil {_x GetVariable "factions"}) then
						{
							_data = [_x] Call BIS_WF_GetDataOnBaseStructure;
							
							if (Count _data > 0) then {_x SetVariable["factions",_data Select 7,true]};
						};

						if (!IsNil {_x GetVariable "isObjective"}) then
						{
							if (_x GetVariable "isObjective") then
							{
								_objectives = BIS_WF_Server GetVariable Format["%1MissionObjects",_side];
								_objectives = _objectives + [_x];
								BIS_WF_Server SetVariable[Format["%1MissionObjects",_side],_objectives];
							};
						};
					};
				}
				else
				{
					//Any empty vehicles are added to side's respawnable pool of vehicles at base.
					if (Count Crew _x < 1) then
					{
						if (Call Compile Format["TypeOf _x In (%1DepotUnits + %1LightUnits + %1HeavyUnits + %1AircraftUnits + %1WingedAircraftUnits)",_side]) then
						{
							Call Compile Format["[_x,%1] Call BIS_WF_InitUnit",_side];
							Call Compile Format["[_x,%1] Spawn BIS_WF_UpdateBaseVehicle",_side];
							_vehicles = _vehicles + [_x];
						};
					}
					//If not an empty vehicle then it must be a team.
					else
					{
						if (Call Compile Format["Side _x == %1 && !(Group _x In (_teams + _defenseTeams))",_side]) then
						{
							if (IsNil {Group _x GetVariable "dedicatedMission"}) then
							{
								_teams = _teams + [Group _x];
								_count = _count + 1;
								Call Compile Format["%1Team%2 = Group _x;PublicVariable ""%1Team%2""",_side,_count];
							}
							else
							{
								_defenseTeams = _defenseTeams + [Group _x];
							};
						};
					};
				};
			};
		} ForEach _objects;

		Call Compile Format["_%1VehiclePool = _vehicles",_side];
		Call Compile Format["%1DefenseTeams = _defenseTeams",_side];
		Call Compile Format["%1Teams = _teams",_side];
	} ForEach ["East","West","Resistance"];

	ProcessInitCommands;
};

BIS_WF_Server SetVariable ["totalEastTeams",Count EastTeams];
BIS_WF_Server SetVariable ["totalWestTeams",Count WestTeams];
BIS_WF_Server SetVariable ["totalResistanceTeams",Count ResistanceTeams];

//Get rid of these when all missions have been converted.
BIS_WF_Common SetVariable ["totalEastTeams",Count EastTeams];
BIS_WF_Common SetVariable ["totalWestTeams",Count WestTeams];
BIS_WF_Common SetVariable ["totalResistanceTeams",Count ResistanceTeams];

//Calculate sides that are not active (do not have a base and commander).
_sides = [];
if ((BIS_WF_Server GetVariable "totalEastTeams") < 1) then {_sides = _sides + [East]};
if ((BIS_WF_Server GetVariable "totalWestTeams") < 1) then {_sides = _sides + [West]};
if ((BIS_WF_Server GetVariable "totalResistanceTeams") < 1) then {_sides = _sides + [Resistance]};

BIS_WF_Common SetVariable ["inactiveSides",_sides,true];

PublicVariable "EastTeams";
PublicVariable "WestTeams";
PublicVariable "ResistanceTeams";

eastCommanderVoteTime = VOTETIME;
westCommanderVoteTime = VOTETIME;
resistanceCommanderVoteTime = VOTETIME;

PublicVariable "eastCommanderVoteTime";
PublicVariable "westCommanderVoteTime";
PublicVariable "resistanceCommanderVoteTime";

eastAITeams = eastTeams;
westAITeams = westTeams;
resistanceAITeams = resistanceTeams;

civilianCasualties = 0;
eastCasualties = 0;
westCasualties = 0;
resistanceCasualties = 0;

westVehiclesCreated = 0;
westVehiclesLost = 0;
westUnitsCreated = 0;

eastVehiclesCreated = 0;
eastVehiclesLost = 0;
eastUnitsCreated = 0;

resistanceVehiclesCreated = 0;
resistanceVehiclesLost = 0;
resistanceUnitsCreated = 0;

PublicVariable "civilianCasualties";
PublicVariable "eastCasualties";
PublicVariable "westCasualties";
PublicVariable "resistanceCasualties";

PublicVariable "westVehiclesCreated";
PublicVariable "westVehiclesLost";
PublicVariable "westUnitsCreated";

PublicVariable "eastVehiclesCreated";
PublicVariable "eastVehiclesLost";
PublicVariable "eastUnitsCreated";

PublicVariable "resistanceVehiclesCreated";
PublicVariable "resistanceVehiclesLost";
PublicVariable "resistanceUnitsCreated";

BIS_WF_Bodies = [];

[] Spawn
{
	WaitUntil {gameInitialized};
	PublicVariable "gameInitialized";
};

{
	//Generate any HQs if needed.
	if (IsNil Format["%1MHQ",_x]) then
	{
		Call Compile Format["%1MHQ = ObjNull",_x];

		//If team is present in game then create MHQ.
		if ((BIS_WF_Server GetVariable Format["total%1Teams",_x]) > 0) then
		{
			Call Compile Format["%1MHQ = %1MHQNAME CreateVehicle (GetPos Leader (%1Teams Select 0));",_x];
			[0,0,Call Compile Format["%1MHQ",_x],true] Call SRVFNCRequestVehicleLock;
		};
	};

	//Generate side's base/HQ variables.
	Call Compile Format["%1AIBase = false",_x];
	if (IsNil Format["%1Base",_x]) then {Call Compile Format["%1Base = %1MHQ",_x]};
	PublicVariable Format["%1Base",_x];
	PublicVariable Format["%1MHQ",_x];

	if (IsNil Format["%1MHQDeployed",_x]) then {Call Compile Format["%1MHQDeployed = false",_x]};
	PublicVariable Format["%1MHQDeployed",_x];
} ForEach [WFSideText East,WFSideText West,WFSideText Resistance];

[] Call Compile PreprocessFile (corepath1 + "Server\Config\Config_Compositions.sqf");

if (IsNil {BIS_WF_Common GetVariable "customBaseLayoutsScript"}) then {BIS_WF_Common SetVariable ["customBaseLayoutsScript",""]};

if ((BIS_WF_Common GetVariable "customBaseLayoutsScript") != "") then
{
	[] Call Compile PreprocessFile (BIS_WF_Common GetVariable "customBaseLayoutsScript");
}
else
{
	[] Call Compile PreprocessFile (corepath + "Server\Config\Config_BaseLayouts.sqf");
};

[] Spawn BIS_WF_UpdateCleanup;
[] Spawn BIS_WF_UpdateCommandsFromClients;
[] Spawn BIS_WF_UpdateServer;
[] Spawn BIS_WF_UpdateServerTime;
[] Spawn BIS_WF_UpdateResources;

if (IsNil {BIS_WF_Common GetVariable "customInitServerScript"}) ExitWith {};

if (IsNil {BIS_WF_Common GetVariable "sidePlacement"}) then {BIS_WF_Common SetVariable["sidePlacement",[true,-1,-1]]};

[BIS_WF_Common GetVariable "sidePlacement"] Call BIS_WF_GetLocations;

//To start both sides at a fixed location just delete all the start location logics in mission & call function with false.
(BIS_WF_Common GetVariable "sidePlacement") Call BIS_WF_PlaceSides;

//ToDo: Implement new method after all missions have been converted.
//[_customStartingLocations] Call BIS_WF_PlaceSides;

//Move vehicle pool to the base.
{
	Private["_location"];
	_location = Call Compile Format["%1StartingLocation",_x];
	{
		if (_x Distance _location > (BIS_WF_Constants GetVariable "BASERANGE") && IsNil {_x GetVariable "stayAtPosition"}) then
		{
			_x SetPos ([[_location,10,25] Call GetRandomPosition,15] Call BIS_WF_GetSafePosition);
			_x SetVelocity [0,0,-1];
		};
	} ForEach (Call Compile Format["_%1VehiclePool",_x]);
} ForEach ["East","West","Resistance"];

//MP game if coop set or not SP game.
_MPGame = BIS_WF_Common GetVariable "CoopGame";

if (BIS_WF_Common GetVariable "SPGame") then
{
	//Single player game. True = player commander, false = AI commander.
	[BIS_WF_Common GetVariable "SPGamePlayerIsCommander"] Call Compile PreprocessFile (corePath + "Server\Init\Init_SPGame.sqf");

	eastCommanderVoteTime = 0;
	westCommanderVoteTime = 0;
	resistanceCommanderVoteTime = 0;
}
else
{
	_MPGame = true;
};

if (!Local player) then
{
	CreateMarkerLocal ["Respawn_east",GetPos EastBase];
	"Respawn_east" SetMarkerTypeLocal "Empty";

	CreateMarkerLocal ["Respawn_west",GetPos WestBase];
	"Respawn_west" SetMarkerTypeLocal "Empty";

	CreateMarkerLocal ["respawn_guerrila",GetPos ResistanceBase];
	"respawn_guerrila" SetMarkerTypeLocal "Empty";
}
else
{
	if (Side player == East) then
	{
		CreateMarkerLocal ["Respawn_west",GetPos WestBase];
		"Respawn_west" SetMarkerTypeLocal "Empty";

		CreateMarkerLocal ["respawn_guerrila",GetPos ResistanceBase];
		"respawn_guerrila" SetMarkerTypeLocal "Empty";
	}
	else
	{
		if (Side player == West) then
		{
			CreateMarkerLocal ["Respawn_east",GetPos EastBase];
			"Respawn_east" SetMarkerTypeLocal "Empty";

			CreateMarkerLocal ["respawn_guerrila",GetPos ResistanceBase];
			"respawn_guerrila" SetMarkerTypeLocal "Empty";
		}
		else
		{
			CreateMarkerLocal ["Respawn_west",GetPos WestBase];
			"Respawn_west" SetMarkerTypeLocal "Empty";

			CreateMarkerLocal ["Respawn_east",GetPos EastBase];
			"Respawn_east" SetMarkerTypeLocal "Empty";
		};
	};
};

"Respawn_east" SetMarkerPosLocal GetPos EastBase;
"Respawn_west" SetMarkerPosLocal GetPos WestBase;
"respawn_guerrila" SetMarkerPosLocal GetPos ResistanceBase;

if (BIS_WF_Server GetVariable "totalWestTeams" > 0) then
{
	if (_MPGame) then {[West] Call BIS_WF_VoteForCommander};

	//Enable AI commander strategic analysis and mission generation. Will remain inactive if player is the commander.
	[West] Spawn BIS_WF_StrategicUpdateCommander;
	[West] Spawn BIS_WF_UpdateCommander;
	[WestStartingLocation,West] Exec (corePath + "Server\AI\Commander\Commander_UpdateBase.sqs");

	{
		//If faction was not defined then set it according to what team leader belongs to.
		if (IsNil {_x GetVariable "faction"}) then
		{
			_x SetVariable ["faction",GetText (configFile >> "CfgVehicles" >> (TypeOf Leader _x) >> "faction")];
		};

		_template = -1;
		if (!IsNil {_x GetVariable "teamTemplate"}) then
		{
			_template = [West,_x GetVariable "teamTemplate",_x GetVariable "faction"] Call BIS_WF_GetTeamTemplateData;
			if (Count _template > 0) then {_template = _template Select 0};
		};

		_dedicatedMission = -1;
		if (!IsNil {_x GetVariable "dedicatedMission"}) then {_dedicatedMission = _x GetVariable "dedicatedMission"};

		[_x,_template,_dedicatedMission] Spawn BIS_WF_UpdateTeam;
	} ForEach westTeams + westDefenseTeams;
};

if (BIS_WF_Server GetVariable "totalEastTeams" > 0) then
{
	if (_MPGame) then {[East] Call BIS_WF_VoteForCommander};

	//Enable AI commander strategic analysis and mission generation. Will remain inactive if player is the commander.
	[East] Spawn BIS_WF_StrategicUpdateCommander;
	[East] Spawn BIS_WF_UpdateCommander;
	[EastStartingLocation,East] Exec (corePath + "Server\AI\Commander\Commander_UpdateBase.sqs");

	{
		//If faction was not defined then set it according to what team leader belongs to.
		if (IsNil {_x GetVariable "faction"}) then
		{
			_x SetVariable ["faction",GetText (configFile >> "CfgVehicles" >> (TypeOf Leader _x) >> "faction")];
		};

		_template = -1;
		if (!IsNil {_x GetVariable "teamTemplate"}) then
		{
			_template = [East,_x GetVariable "teamTemplate",_x GetVariable "faction"] Call BIS_WF_GetTeamTemplateData;
			if (Count _template > 0) then {_template = _template Select 0};
		};

		_dedicatedMission = -1;
		if (!IsNil {_x GetVariable "dedicatedMission"}) then {_dedicatedMission = _x GetVariable "dedicatedMission"};

		[_x,_template,_dedicatedMission] Spawn BIS_WF_UpdateTeam;
	} ForEach eastTeams + eastDefenseTeams;
};

if (BIS_WF_Server GetVariable "totalResistanceTeams" > 0) then
{
	if (_MPGame) then {[Resistance] Call BIS_WF_VoteForCommander};

	//Enable AI commander strategic analysis and mission generation. Will remain inactive if player is the commander.
	[Resistance] Spawn BIS_WF_StrategicUpdateCommander;
	[Resistance] Spawn BIS_WF_UpdateCommander;
	[ResistanceStartingLocation,Resistance] Exec (corePath + "Server\AI\Commander\Commander_UpdateBase.sqs");

	{
		//If faction was not defined then set it according to what team leader belongs to.
		if (IsNil {_x GetVariable "faction"}) then
		{
			_x SetVariable ["faction",GetText (configFile >> "CfgVehicles" >> (TypeOf Leader _x) >> "faction")];
		};

		_template = -1;
		if (!IsNil {_x GetVariable "teamTemplate"}) then
		{
			_template = [Resistance,_x GetVariable "teamTemplate",_x GetVariable "faction"] Call BIS_WF_GetTeamTemplateData;
			if (Count _template > 0) then {_template = _template Select 0};
		};

		_dedicatedMission = -1;
		if (!IsNil {_x GetVariable "dedicatedMission"}) then {_dedicatedMission = _x GetVariable "dedicatedMission"};

		[_x,_template,_dedicatedMission] Spawn BIS_WF_UpdateTeam;
	} ForEach resistanceTeams + resistanceDefenseTeams;
};

//Init_Bases.sqf is to set up starting vehicles at base and base's defense/patrol teams.
if (!IsNil {BIS_WF_Common GetVariable "customInitBasesScript"}) then
{
	if ((BIS_WF_Common GetVariable "customInitBasesScript") != "") then
	{
		[] Call Compile PreprocessFile (BIS_WF_Common GetVariable "customInitBasesScript");
	};
};

Private["_path"];

_path = corePath;
if (!IsNil {BIS_WF_Common GetVariable "customTownPath"}) then {_path = BIS_WF_Common GetVariable "customTownPath"};

[_path,_customCamps,_customOwners] Call Compile PreprocessFile (corePath + "Server\Init\Init_AllTowns.sqf");

//Run any server-side support scripts.
if (BIS_WF_SupportLevel != 0) then
{
	{
		[] Spawn _x;
	} ForEach (BIS_WF_Constants GetVariable "supportServerUpdateScripts");
}
else
{
	//Set AIs not to build certain defenses if supports are disabled.
	//For example, mortars (or any indirect-fire only weapons) are useless if supports are disabled.
	Private["_index","_side"];
	{
		_side = _x;

		{
			_index = Call Compile Format["%1DefenseNames Find _x",_side];
			if (_index != -1) then
			{
				Call Compile Format["%1DefenseChances Set[_index,0]",_side];
				Call Compile Format["%1DefenseTownChances Set[_index,0]",_side];
			};
		} ForEach BIS_WF_IndirectFireOnlyDefenses;
	} ForEach [WFSideText East,WFSideText West,WFSideText Resistance];
};

Private["_group"];
BIS_WF_Common SynchronizeObjectsRemove _customOwners;

{
	_group = Group _x;
	DeleteVehicle _x;
	if (Count Units _group < 1) then
	{
		DeleteGroup _group;
	};
} ForEach _customOwners;

//ALICE/SILVIE
if (BIS_WF_EnableCivilians) then
{
	if (IsNil "BIS_alice_mainscope") then 
	{
		Private["_civilianCountFormula"];

		_civilianCountFormula = "5 Min (1 + Round (%1 / 20))";
		if (BIS_WF_UrbanWarfare) then {_civilianCountFormula = "Round Random 5"};

		"AliceManager" CreateUnit 
		[
			[1000,10,0], (CreateGroup sideLogic),
			Format["
				BIS_alice_mainscope = this;
				this SetVariable ['townlist', towns];
				this SetVariable ['civilianCount','%1'];
				PublicVariable 'BIS_alice_mainscope';
			",_civilianCountFormula], 0.6, "corporal"
		];
	};

	if (IsNil "BIS_silvie_mainscope") then
	{
		Private["_vehicleCountFormula"];

		_vehicleCountFormula = "5 Min (1 + Round (%1 / 25))";
		if (BIS_WF_UrbanWarfare) then {_vehicleCountFormula = "Round Random 3"};

		"SilvieManager" CreateUnit
		[
			[1000,10,0], (CreateGroup sideLogic),
			Format["
				BIS_silvie_mainscope = this;
				this SetVariable ['townlist', towns];
				this SetVariable ['vehicleCount','%1'];
				PublicVariable 'BIS_silvie_mainscope';
			",_vehicleCountFormula],0.6, "corporal"
		];
	};
};

//Set enhanced coop mode values.
if (BIS_WF_EnhancedCoopMode) then
{
	//Wait for server to start.
	[] Spawn
	{
		Sleep 2;

		Private["_eastPlayers","_resistancePlayers","_westPlayers"];

		_eastPlayers = false;
		_westPlayers = false;
		_resistancePlayers = false;

		//Find sides with players.
		{
			if (IsPlayer Leader _x) ExitWith {_eastPlayers = true};
		} ForEach EastTeams;

		{
			if (IsPlayer Leader _x) ExitWith {_westPlayers = true};
		} ForEach WestTeams;

		{
			if (IsPlayer Leader _x) ExitWith {_resistancePlayers = true};
		} ForEach ResistanceTeams;

		Private["_sidesToAdd"];
		_sidesToAdd = [];

		if (!_eastPlayers && !(East In (BIS_WF_Common GetVariable "inactiveSides"))) then {_sidesToAdd = _sidesToAdd + [East]};
		if (!_westPlayers && !(West In (BIS_WF_Common GetVariable "inactiveSides"))) then {_sidesToAdd = _sidesToAdd + [West]};
		if (!_resistancePlayers && !(Resistance In (BIS_WF_Common GetVariable "inactiveSides"))) then {_sidesToAdd = _sidesToAdd + [Resistance]};

		//Defend the Base mode.
		if (BIS_WF_DefendTheBaseMode) then
		{
			if ((BIS_WF_Constants GetVariable "SIDESTARTINGDISTANCE") < 1000) then
			{
				Private["_structures"];

				//Reveal player base(s) to any AI-only sides.
				{
					_structures = BIS_WF_Server GetVariable Format["%1SpottedStructures",WFSideText _x];
					if (_eastPlayers) then {_structures = _structures + [EastBase]};
					if (_westPlayers) then {_structures = _structures + [WestBase]};
					if (_resistancePlayers) then {_structures = _structures + [ResistanceBase]};

					BIS_WF_Server SetVariable[Format["%1SpottedStructures",WFSideText _x],_structures];
				} ForEach _sidesToAdd;
			};
		};

		Private["_teamsToAdd"];
		_teamsToAdd = Round ((BIS_WF_Constants GetVariable "OPPOSITIONMULTIPLIER") * (BIS_WF_Constants GetVariable "COOPTEAMSMODIFIER"));
		if (_teamsToAdd > 16) then {_teamsToAdd = 16};

		//Add more teams to AI sides.
		{
			Private["_ammo","_base","_leader","_leaderType","_team","_weapons"];

			_base = Call Compile Format["%1Base",WFSideText _x];
			_leaderType = Call Compile ((_x Call GetSideLetter) + "SOLDIER");
			_weapons = Call Compile (WFSideText _x + "DefaultWeapons");
			_ammo = Call Compile (WFSideText _x + "DefaultAmmo");

			//Create additional teams.
			for [{_count = 0},{_count < _teamsToAdd},{_count = _count + 1}] do
			{
				_team = CreateGroup _x;
				_leader = _team CreateUnit [_leaderType,Position _base,[],0.5,"FORM"];
				_leader SetVariable ["faction",GetText (configFile >> "CfgVehicles" >> _leaderType >> "faction")];

				[_leader,_weapons,_ammo] Call EquipLoadout;
				[_leader,_x] Call BIS_WF_InitUnit;
				[_leader,[_base,5,25] Call GetRandomPosition,35] Call PlaceSafe;

				Call Compile Format["%1Team%2 = _team;PublicVariable ""%1Team%2""",WFSideText _x,_count];
				Call Compile Format["%1Teams = %1Teams + [_team]",WFSideText _x];
				[_team,-1,-1] Spawn BIS_WF_UpdateTeam;
			};

			Call Compile Format["PublicVariable '%1Teams'",WFSideText _x];
		} ForEach _sidesToAdd;
	};
};

//*****************************************************************************************
//1/17/7 MM - Created file.
