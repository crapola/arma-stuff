diag_log "--> myInit_Client.sqf";

//Last modified 11/17/10
//*****************************************************************************************
//Description: This is run when a client joins a mission.
//*****************************************************************************************

//Compile function loops.
BIS_WF_UpdateAutoSalvageTruck = Compile PreprocessFile (corePath + "Client\Client_UpdateAutoSalvageTruck.sqf");
BIS_WF_UpdateAutoSupplyTruck = Compile PreprocessFile (corePath + "Client\Client_UpdateAutoSupplyTruck.sqf");
BIS_WF_UpdateAvailableActions = Compile PreprocessFile (corePath + "Client\Client_UpdateAvailableActions.sqf");
BIS_WF_UpdateAvailableObjects = Compile PreprocessFile (corePath + "Client\Client_UpdateAvailableObjects.sqf");
BIS_WF_UpdateMissionMarker = Compile PreprocessFile (corePath + "Client\Client_UpdateMissionMarker.sqf");
BIS_WF_UpdateMissionMarkers = Compile PreprocessFile (corePath + "Client\Client_UpdateMissionMarkers.sqf");
BIS_WF_UpdateTownAttackedMarker = Compile PreprocessFile (corePath + "Client\Functions\Client_UpdateTownAttackedMarker.sqf");
BIS_WF_UpdateTownMarkers = Compile PreprocessFile (corePath + "Client\Client_UpdateTownMarkers.sqf");
BIS_WF_UpdateDefenseMarker = Compile PreprocessFile (corePath + "Client\Client_UpdateDefenseMarker.sqf");
BIS_WF_UpdateClient = Compile PreprocessFile (corePath + "Client\Client_Update.sqf");
BIS_WF_UpdateCommanderVoteMenu = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateCommanderVoteMenu.sqf");
BIS_WF_UpdateCommanderVoteMenuClassic = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateCommanderVoteMenuClassic.sqf");
BIS_WF_UpdateCommandsFromServer = Compile PreprocessFile (corePath + "Client\Client_UpdateCommandsFromServer.sqf");
BIS_WF_UpdateHelpMenu = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateHelpMenu.sqf");
BIS_WF_UpdateOptions = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateOptions.sqf");
BIS_WF_UpdateSupplyTruck = Compile PreprocessFile (corePath + "Client\Client_UpdateSupplyTruck.sqf");
BIS_WF_UpdateTeamMarkers = Compile PreprocessFile (corePath + "Client\Client_UpdateTeamMarkers.sqf");
BIS_WF_UpdateTeamMenuClassic = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateTeamMenu.sqf");
BIS_WF_UpdateTime = Compile PreprocessFile (corePath + "Client\Client_UpdateTime.sqf");
BIS_WF_UpdateTownCapture = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateTownCapture.sqf");
BIS_WF_UpdateUnitMarker = Compile PreprocessFile (corePath + "Client\Client_UpdateUnitMarker.sqf");

//GUI
BIS_WF_MapClickEH = Compile PreprocessFile (corePath + "Client\GUI\GUI_MapClickEH.sqf");
BIS_WF_MapDiaryEH = Compile PreprocessFile (corePath + "Client\GUI\GUI_MapDiaryEH.sqf");
BIS_WF_OpenCityMenu = Compile PreprocessFile (corePath + "Client\GUI\GUI_OpenCityMenu.sqf");
BIS_WF_OpenDiplomacyMenu = Compile PreprocessFile (corePath + "Client\GUI\GUI_OpenDiplomacyMenu.sqf");
BIS_WF_OpenEndOfGameMenu = Compile PreprocessFile (corePath + "Client\GUI\GUI_OpenEndOfGameMenu.sqf");
BIS_WF_OpenFactoryMenu = Compile PreprocessFile (corePath + "Client\GUI\GUI_OpenFactoryMenu.sqf");
BIS_WF_OpenGearMenu = Compile PreprocessFile (corePath + "Client\GUI\GUI_OpenGearMenu.sqf");
BIS_WF_OpenTeamMenu = Compile PreprocessFile (corePath + "Client\GUI\GUI_OpenTeamMenu.sqf");
BIS_WF_UpdateCityMenuEvent = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateCityMenuEvent.sqf");
BIS_WF_UpdateDiplomacyMenu = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateDiplomacyMenu.sqf");
BIS_WF_UpdateDiplomacyMenuEvent = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateDiplomacyMenuEvent.sqf");
BIS_WF_UpdateEndOfGameMenu = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateEndOfGameMenu.sqf");
BIS_WF_UpdateFactoryMenu = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateFactoryMenu.sqf");
BIS_WF_UpdateFactoryMenuDescription = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateFactoryMenuDescription.sqf");
BIS_WF_UpdateFactoryMenuEvent = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateFactoryMenuEvent.sqf");
BIS_WF_UpdateFactoryMenuFilters = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateFactoryMenuFilters.sqf");
BIS_WF_UpdateFactoryMenuList = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateFactoryMenuList.sqf");
BIS_WF_UpdateFactoryMenuQueue = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateFactoryMenuQueue.sqf");
BIS_WF_UpdateGearMenu = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateGearMenu.sqf");
BIS_WF_UpdateGearMenuCosts = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateGearMenuCosts.sqf");
BIS_WF_UpdateGearMenuDescription = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateGearMenuDescription.sqf");
BIS_WF_UpdateGearMenuEvent = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateGearMenuEvent.sqf");
BIS_WF_UpdateGearMenuList = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateGearMenuList.sqf");
BIS_WF_UpdateGearMenuMain = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateGearMenuMain.sqf");
BIS_WF_UpdateSupports = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateSupports.sqf");
BIS_WF_UpdateTeamMenu = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateTeamMenu.sqf");
BIS_WF_UpdateTeamMenuEvent = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateTeamMenuEvent.sqf");
BIS_WF_UpdateTeamMenuStatus = Compile PreprocessFile (corePath + "Client\GUI\GUI_UpdateTeamMenuStatus.sqf");

//Coin functions
BIS_WF_CoinOnConstruct = Compile PreprocessFile (corepath + "Client\Functions\Client_CoinOnConstruct.sqf");
BIS_WF_CoinOnPurchase = Compile PreprocessFile (corepath + "Client\Functions\Client_CoinOnPurchase.sqf");
BIS_WF_CoinOnStart = Compile PreprocessFile (corepath + "Client\Functions\Client_CoinOnStart.sqf");
BIS_WF_CoinOnSelect = Compile PreprocessFile (corepath + "Client\Functions\Client_CoinOnSelect.sqf");
BIS_WF_CoinUpdateItems = Compile PreprocessFile (corepath + "Client\Functions\Client_CoinUpdateItems.sqf");
BIS_WF_UpdateCoin = Compile PreprocessFile (corepath + "Client\Client_UpdateCoin.sqf");

//Compile regular functions
BIS_WF_ActionOpenCoin = Compile PreprocessFile (corePath + "Client\Action\Action_OpenCoin.sqf");
BIS_WF_ActionUseNearbyStructure = Compile PreprocessFile (corePath + "Client\Action\Action_UseNearbyStructure.sqf");
BIS_WF_AddToMessageLog = Compile PreprocessFile (corePath + "Client\Functions\Client_AddToMessageLog.sqf");
BIS_WF_AirportCaptured = Compile PreprocessFile (corePath + "Client\Functions\Client_AirportCaptured.sqf");
BIS_WF_BuyClientUnit = Compile PreprocessFile (corePath + "Client\Functions\Client_BuyUnit.sqf");
BIS_WF_ClientUpdateTown = Compile PreprocessFile (corePath + "Client\Functions\Client_UpdateTown.sqf");
BIS_WF_CreateObjectMarkers = Compile PreprocessFile (corePath + "Client\Functions\Client_CreateObjectMarkers.sqf");
BIS_WF_DefenseDestroyed = Compile PreprocessFile (corePath + "Client\Functions\Client_DefenseDestroyed.sqf");
BIS_WF_DisplayInventory = Compile PreprocessFile (corePath + "Client\Functions\Client_DisplayInventory.sqf");
BIS_WF_DisplayMiscInventory = Compile PreprocessFile (corePath + "Client\Functions\Client_DisplayMiscInventory.sqf");
BIS_WF_DisplayInfoBar = Compile PreprocessFile (corePath + "Client\Functions\Client_DisplayInfoBar.sqf");
BIS_WF_ShowGUIMessage = Compile PreprocessFile (corePath + "Client\Functions\Client_ShowGUIMessage.sqf");
BIS_WF_ShowWaypoints = Compile PreprocessFile (corePath + "Client\Functions\Client_ShowWaypoints.sqf");
BIS_WF_ChangePlayerFunds = Compile PreprocessFile (corePath + "Client\Functions\Client_ChangePlayerFunds.sqf");
BIS_WF_GetGearCost = Compile PreprocessFile (corePath + "Client\Functions\Client_GetGearCost.sqf");
BIS_WF_GetPlayerFunds = Compile PreprocessFile (corePath + "Client\Functions\Client_GetPlayerFunds.sqf");
BIS_WF_GetPlayerRank = Compile PreprocessFile (corePath + "Client\Functions\Client_GetPlayerRank.sqf");
BIS_WF_GetIncome = Compile PreprocessFile (corePath + "Client\Functions\Client_GetIncome.sqf");
BIS_WF_GroupChatMessage = Compile PreprocessFile (corePath + "Client\Functions\Client_GroupChatMessage.sqf");
BIS_WF_InitInfoBar = Compile PreprocessFile (corePath + "Client\Functions\Client_InitInfoBar.sqf");
BIS_WF_PlayerMissionEnded = Compile PreprocessFile (corePath + "Client\Functions\Client_PlayerMissionEnded.sqf");
BIS_WF_PlayerRespawn = Compile PreprocessFile ("myClient_PlayerRespawn.sqf"); // Modified this
BIS_WF_ReplaceInventoryAmmo = Compile PreprocessFile (corePath + "Client\Functions\Client_ReplaceInventoryAmmo.sqf");
BIS_WF_ReplaceSecondaryInventoryAmmo = Compile PreprocessFile (corePath + "Client\Functions\Client_ReplaceSecondaryInventoryAmmo.sqf");
BIS_WF_ReplaceSidearmInventoryAmmo = Compile PreprocessFile (corePath + "Client\Functions\Client_ReplaceSidearmInventoryAmmo.sqf");
BIS_WF_RequestFireMission = Compile PreprocessFile (corePath + "Client\Functions\Client_RequestFireMission.sqf");
BIS_WF_RequestNearestSupport = Compile PreprocessFile (corePath + "Client\Functions\Client_RequestNearestSupport.sqf");
BIS_WF_SetHCTeamType = Compile PreprocessFile (corePath + "Client\Functions\Client_SetHCTeamType.sqf");
BIS_WF_SideChatMessage = Compile PreprocessFile (corePath + "Client\Functions\Client_SideChatMessage.sqf");
BIS_WF_TitleTextMessage = Compile PreprocessFile (corePath + "Client\Functions\Client_TitleTextMessage.sqf");

BIS_WF_ClientKilled = Compile PreprocessFile (corePath + "Client\Functions\Client_Killed.sqf");
BIS_WF_InitAllTowns = Compile PreprocessFile (corePath + "Client\Functions\Client_InitAllTowns.sqf");
BIS_WF_MapSingleClick = Compile PreprocessFile (corePath + "Client\Functions\Client_MapSingleClick.sqf");

//Should be called with spawn.
BIS_WF_CommandToServer = Compile PreprocessFile (corePath + "Client\Functions\Client_CommandToServer.sqf");

//Functions for FT
BIS_WF_FTLOOP = compile preprocessFile (corePath + "Client\GUI\GUI_FTLoop.sqf");
BIS_WF_FTUPDATE = compile preprocessFile (corePath + "Client\GUI\GUI_FTUpdate.sqf");
BIS_FNC_GETFTTIME = compile preprocessFile (corePath + "Client\Functions\Client_GetFTTime.sqf");
BIS_WF_FTtoDestination = compile preprocessFile (corePath + "Client\GUI\GUI_FTtoDestination.sqf");
BIS_WF_FTCONDITIONS = compile preprocessFile (corePath + "Client\GUI\GUI_FTconditions.sqf");

//Function(s) for communication
BIS_FNC_CMDMENUCOMM_TEAMSELECT = compile preprocessFile (corePath + "SQC\cmdMenuCommTeamSelect.sqf");
//squad join/disband
BIS_WF_SQUADJOIN = compile preprocessFile (corePath + "SQC\squadJoin.sqf");
BIS_WF_SQUADDISMISS = compile preprocessFile (corePath + "SQC\squadDismiss.sqf");

//Script(s) for communication
BIS_FNC_CMDMENUCOMM = (corePath + "SQC\cmdMenuComm.sqf");

BIS_FNC_GUIset = {UInamespace setVariable [_this select 0, _this select 1];}; //setting variable to gui namespace
BIS_FNC_GUIget = {UInamespace getVariable (_this select 0);}; //getting variable from gui namespace

_recompile = false;
if (Count _this > 0) then {_recompile = _this Select 0};
if (_recompile) ExitWith {};

/*
[] spawn {
  waitUntil {!isNil "BIS_FNC_Init"};
  waitUntil {BIS_FNC_Init};
  //Menu - Communication - menu displayed from root groop menu -> 0 (Communication)
  //Creating the global comms menu and showing / enabling the correct section for WF.
  
  call BIS_fnc_commsMenuCreate;
  ["wf", 1] call BIS_fnc_commsMenuToggleVisibility;
  ["wf", 1] call BIS_fnc_commsMenuToggleAvailability;
};
*/

//Team menu.
BIS_WF_TeamCommunicationMenu = 
[
	[Localize "STR_SOM_COMMUNICATIONS",true],
	[Localize "STR_SQLCOMMDISBANDSEL" + " (%SELECTED_UNIT_ID)",[4],"",-5,[["expression","BIS_SQL_SelectedUnits = GroupSelectedUnits player;ShowCommandingMenu '';['DismissSelected'] ExecVM BIS_FNC_CMDMENUCOMM;"]],"1","1",-1],
	[Localize "STR_SQLSENDUNITS" + " (%SELECTED_UNIT_ID)",[5],"",-5,[["expression","ShowCommandingMenu '';BIS_WF_GROUPSELECTEDUNITS = GroupSelectedUnits player;['SendUnits'] ExecVM BIS_FNC_CMDMENUCOMM;"]],"1","1",-1],
	[Localize "STR_SQLSENDMONEY",[6],"",-5,[["expression","ShowCommandingMenu '';['SendMoney'] ExecVM BIS_FNC_CMDMENUCOMM;"]],"1","1",4]
	//Not yet properly supported.
	//[Localize "STR_SQLREQJOIN", [7], "", -5, [["expression","['Join'] execVM BIS_FNC_CMDMENUCOMM"]], "1", "1", -1],
	//[Localize "STR_SQLREQDISBAND", [8], "", -5, [["expression", "showCommandingMenu ''; ['Dismiss'] execVM BIS_FNC_CMDMENUCOMM; debugLog format ['SQL_comm Disband menu'];"]], "1", "1",-1],
];

//Commander menu.
BIS_WF_CommanderCommunicationMenu = 
[
	[Localize "STR_SOM_COMMUNICATIONS",true],
	//["",[],"",-1,[],"0","0",-1],
	[Localize "STRWFSETTYPE" + " (%SELECTED_UNIT_ID)",[2],"",-5,[["expression","ShowCommandingMenu '';BIS_SQL_SelectedUnits = HCSelected player;['HCTeamType'] ExecVM BIS_FNC_CMDMENUCOMM;"]],"1","1",-1],
	[Localize "STRWFENABLEAI" + " (%SELECTED_UNIT_ID)",[3],"",-5,[["expression","BIS_SQL_SelectedUnits = hcSelected player;['HCAutomaticBehavior'] ExecVM BIS_FNC_CMDMENUCOMM"]],"1","1",-1],
	[Localize "STR_DISP_INT_RESPAWN" + " (%SELECTED_UNIT_ID)",[4],"",-5,[["expression","BIS_SQL_SelectedUnits = hcSelected player;ShowCommandingMenu '';['HCRespawnSelected'] ExecVM BIS_FNC_CMDMENUCOMM;"]],"1","1",-1]
];

_supportMenu = "";
BIS_WF_SupportMenu = [];

if (BIS_WF_SupportLevel > 0) then
{
	Private["_menuEntry"];

	BIS_WF_SupportMenu = [[Localize "STR_SOM_REQUEST_SUPPORT",false]];
	_menuEntry = 0;

	{
		BIS_WF_SupportMenu = BIS_WF_SupportMenu +
		[
			[_x,[_menuEntry + 2],"",-5,[["expression",Format["[%1] Spawn BIS_WF_RequestNearestSupport;",_menuEntry]]],"1","1"]
		];

		_menuEntry = _menuEntry + 1;
	} ForEach (BIS_WF_Constants GetVariable "supportNames");

	_supportMenu = "#USER:BIS_WF_SupportMenu";
}
else
{
	if (BIS_WF_OldSupportLevel > 0) then
	{
		_supportMenu = "#USER:BIS_MENU_SOM_Support";
	};
};

if (_supportMenu != "") then
{
	BIS_WF_TeamCommunicationMenu = BIS_WF_TeamCommunicationMenu +
	[
		["", [], "", -1, [], "0", "0", -1],
		[Localize "STR_SOM_REQUEST_SUPPORT",[8],_supportMenu,-5,[["expression", ""]],"1","1",-1]
	];

	BIS_WF_CommanderCommunicationMenu = BIS_WF_CommanderCommunicationMenu +
	[
		["", [], "", -1, [], "0", "0", -1],
		[Localize "STR_SOM_REQUEST_SUPPORT",[8],_supportMenu,-5,[["expression", ""]],"1","1",-1]
	];
};

//Toggle menu.
[] Spawn
{
	while {!gameOver} do
	{
		if (HCShownBar) then {BIS_MENU_GroupCommunication = BIS_WF_CommanderCommunicationMenu}
		else {BIS_MENU_GroupCommunication = BIS_WF_TeamCommunicationMenu};

		Sleep 0.25;
	};
};

if (IsNil "BIS_WF_MissionsCompleted") then {BIS_WF_MissionsCompleted = 0};
if (IsNil "BIS_WF_MissionsFailed") then {BIS_WF_MissionsFailed = 0};

IDCLOADOUTFUNDS					= 200;
IDCLOADOUTSPACE					= 201;
IDCLOADOUTTRADEIN				= 202;
IDCLOADOUTCOST					= 203;
IDCLOADOUTREFUND				= 204;
IDCLOADOUTTEMPLATELIST			= 205;
IDCLOADOUTPRIMARY				= 206;
IDCLOADOUTPRIMARYAMMO			= 207;
IDCLOADOUTPRIMARYLIST			= 208;
IDCLOADOUTSECONDARY				= 209;
IDCLOADOUTSECONDARYAMMO			= 210;
IDCLOADOUTSECONDARYLIST			= 211;
IDCLOADOUTSIDEARM				= 212;
IDCLOADOUTSIDEARMLIST			= 213;
IDCLOADOUTMISC					= 214;
IDCLOADOUTMISCAMMO				= 215;
IDCLOADOUTMISCLIST				= 216;
IDCLOADOUTBUY					= 217;

//Factory menu GUI components
IDCUNITMENUINFO					= 170;
IDCUNITMENUUNITLIST				= 171;
IDCUNITMENUTEAMLIST				= 172;

//Default Menu IDCs.
IDCMENUINFO						= 500;
IDCMENUTEAMLIST					= 501;
IDCMENULIST						= 502;
IDCMENULIST1					= 503;
IDCMENULIST2					= 504;
IDCMENUTITLE					= 510;
IDCMENUSTATUSLABEL				= 511;
IDCMENUBUTTON					= 512;
IDCMENUBUTTON1					= 513;
IDCMENUBUTTON2					= 514;
IDCMENUIMAGE					= 515;
IDCMENUSLIDER					= 516;
IDCMENUCHECKBOX					= 517;
IDCMENUSLIDER					= 518;

IDCCONSTRUCTIONBUTTON			= 600;
IDCCONSTRUCTIONIMAGE			= 700;
IDCCONSTRUCTIONLABEL			= 800;

IDCGENERICBUTTON				= 3400;
IDCGENERICIMAGE					= 3500;
IDCGENERICLABEL					= 3600;
IDCGENERICLIST					= 3700;
IDCGENERICTEXT					= 3800;
IDCGENERICCHECKBOX				= 3900;

IDCERRORMESSAGE					= 3990;

IDCDEFAULT						= 5000;

IDCWEST							= 6000;
IDCEAST							= 7000;

BIS_WF_GEARADDTEMPLATE			= 0;
BIS_WF_GEARADDAMMO				= 1;
BIS_WF_GEARADDPRIMARY			= 2;
BIS_WF_GEARADDSECONDARY			= 3;
BIS_WF_GEARADDSIDEARM			= 4;
BIS_WF_GEARADDMISC				= 5;
BIS_WF_GEARREMOVEPRIMARY		= 6;
BIS_WF_GEARREMOVESECONDARY		= 7;
BIS_WF_GEARREMOVESIDEARM		= 8;
BIS_WF_GEARREMOVEMISC			= 9;
BIS_WF_GEARREMOVESIDEARMMISC	= 10;
BIS_WF_GEARADDPRIMARYAMMO		= 11;
BIS_WF_GEARADDSECONDARYAMMO		= 12;
BIS_WF_GEARADDSIDEARMAMMO		= 13;
BIS_WF_GEARADDMISCAMMO			= 14;
BIS_WF_GEARUPGRADE				= 15;
BIS_WF_GEARRESET				= 16;
BIS_WF_GEARBUY					= 17;
BIS_WF_GEARADD					= 18;
BIS_WF_GEARFILTER				= 19;
BIS_WF_GEARSELECT				= 20;
BIS_WF_GEARREARM				= 21;

BIS_WF_FILTERCLASSIC			= -1;
BIS_WF_FILTERTEMPLATE			= 0;
BIS_WF_FILTERALL				= 1;
BIS_WF_FILTERPRIMARY			= 2;
BIS_WF_FILTERSECONDARY			= 3;
BIS_WF_FILTERSIDEARM			= 4;
BIS_WF_FILTERMISC				= 5;

BIS_WF_FACTORYUNITSELECT		= 0;
BIS_WF_FACTORYUNITADD			= 1;
BIS_WF_QUEUESELECT				= 2;
BIS_WF_QUEUEREMOVE				= 3;
BIS_WF_FACTORYCLEARQUEUE		= 4;
BIS_WF_FACTORYFILTER			= 5;

BIS_WF_FILTERBARRACKS			= 0;
BIS_WF_FILTERLIGHT				= 1;
BIS_WF_FILTERHEAVY				= 2;
BIS_WF_FILTERAIRCRAFT			= 3;
BIS_WF_FILTERAIRPORT			= 4;
BIS_WF_FILTERDEPOT				= 5;
BIS_WF_FILTERCAMP				= 6;

BIS_WF_GEARLISTAMMO				= 1000;

BIS_WF_TEAMTRANSFERFUNDS		= 0;
BIS_WF_TEAMLISTSELECT			= 1;
BIS_WF_TEAMSETTYPE				= 2;
BIS_WF_TEAMSETAUTOMATICAI		= 3;
BIS_WF_TEAMRESPAWN				= 4;
BIS_WF_TEAMFUNDSCHANGED			= 5;
BIS_WF_TEAMSETCOMMANDER			= 6;
BIS_WF_TEAMTOGGLEHUD			= 7;

BIS_WF_DIPLOMACYSIDE0CEASEFIRE	= 0;
BIS_WF_DIPLOMACYSIDE0ALLIANCE	= 1;
BIS_WF_DIPLOMACYSIDE1CEASEFIRE	= 2;
BIS_WF_DIPLOMACYSIDE1ALLIANCE	= 3;
BIS_WF_DIPLOMACYSIDE0SEND		= 4;
BIS_WF_DIPLOMACYSIDE1SEND		= 5;

BIS_WF_UIFactoryMenuActive		= false;
BIS_WF_UIGearMenuActive			= false;
BIS_WF_UIDiplomacyMenuActive	= false;

BIS_WF_UISelectedTeam			= ObjNull;

BIS_WF_UIFactoryFilter = -1;

BIS_WF_DiplomaticMessages = [];

menubutton = -1;
BIS_WF_ShowHelp = true;

BIS_WF_TimeConnected = time;

BIS_WF_FireMissionInProgress = false;
BIS_WF_FireMissionRadius = 50;

BIS_WF_OwnedVehicles = [];
BIS_WF_Salvage = [];
BIS_WF_UnitMarkers = 0;

//Interface
["currentCutDisplay", DisplayNull] call BIS_FNC_GUIset;
["currentTitleDisplay", DisplayNull] call BIS_FNC_GUIset;
BIS_WF_TownCaptureProgressBarActive = false;

BIS_WF_ShowMessagesOnScreen = true;
BIS_WF_MessageLog = [];

BIS_WF_Client SetVariable["lastMapMouseButtonEvent",[-1000,-1,-1,-1,-1]];
BIS_WF_Client SetVariable["lastLocationClicked",ObjNull];

mouseX = 0;
mouseY = 0;
mouseDragged = false;
mouseFocus = false;
onMouseButtonDown = false;
onMouseButtonUp = false;

menuCheckBox = false;

BIS_WF_TimeLastDeath = time;

BIS_WF_RespawnLocation = -1;

BIS_WF_FastTravelDestinationMarker = "FastTravelDestinationMarker";
BIS_WF_FastTravelInProgress = false;

//Init_Client is spawned if player is not local so this is ok. Wait for player to become local.
if (!Local player) then
{
	WaitUntil {Local player};
};

Group player SetVariable ["missionType",-1,true];
Group player SetVariable ["missionLocation",ObjNull,true];

_team = Group player;

for [{_count = Count Waypoints player},{_count >= 0},{_count = _count - 1}] do {DeleteWaypoint[_team,_count]};

//Determine Client's ID (1 - 16).  This is the slot # they joined the game as.  Also determine side ID.
sideJoined = Side player;
sideJoinedText = WFSideText sideJoined;
sideJoinedColor = "";

if (BIS_WF_RelativeSideColors) then
{
	sideJoinedColor = [sideJoined] Call BIS_WF_GetSideColor;
}
else
{
	sideJoinedColor = sideJoined Call BIS_WF_GetSideColorAbsolute;
};

sideJoinedShortText = "E";
if (sideJoined == West) then {sideJoinedShortText = "W"};
if (sideJoined == Resistance) then {sideJoinedShortText = "G"};

//Determine Client's ID (1 - 16).  This is the slot # they joined the game as.  Also determine side ID.
sideID = sideJoined Call GetSideID;
//clientID = player Call GetClientID;//if (clientID < 1) then {player SideChat "Client_Init.sqs: ERROR OCCURRED! Could not determine clientID."};

BIS_WF_CommanderEH1 = -1;
BIS_WF_CommanderEH2 = -1;

commanderTeam = ObjNull;

baseStructures = [];

BIS_WF_ConstructionVehicle = ObjNull;

BIS_WF_BarracksInRange = ObjNull;
BIS_WF_LightFactoryInRange = ObjNull;
BIS_WF_HeavyFactoryInRange = ObjNull;
BIS_WF_AircraftFactoryInRange = ObjNull;
BIS_WF_AirportInRange = ObjNull;
BIS_WF_DepotInRange = ObjNull;
BIS_WF_ServicePointInRange = ObjNull;

BIS_WF_CampInRange = ObjNull;
BIS_WF_HQInRange = ObjNull;
BIS_WF_RepairTruckInRange = ObjNull;

BIS_WF_LastCoinDirection = 0;

_units = Units _team;
_units = _units - [player];
_vehicles = [_team,false] Call BIS_WF_GetTeamVehicles;

//Clear any potential AI units & vehicles.
if (!(BIS_WF_Common GetVariable "coopGame") && !(BIS_WF_Common GetVariable "SPGame")) then
{
	{
		if (!(TypeOf _x In ["AliceManager","AlternativeInjurySimulation","BattleFieldClearance","FirstAidSystem","HighCommand","MartaManager"])) then
		{
			DeleteVehicle _x;
		};
	} ForEach (_units + _vehicles);
};

Private["_delete","_vehicle"];

_vehicle = Vehicle player;

//Remove player from vehicle.
if (_vehicle != player) then
{
	if (!(BIS_WF_Common GetVariable "coopGame") && !(BIS_WF_Common GetVariable "SPGame")) then
	{
		_delete = true;

		//Do not delete vehicle that potentially belongs to another player.
		{
			if (IsPlayer _x || IsPlayer Leader _x) then {_delete = false};
		} ForEach (Crew _vehicle);

		_vehicle Lock false;
		player Action ["Eject",_vehicle];

		if (_delete) then {DeleteVehicle _vehicle};
	};
};

//Init client-side PV command system.
[] Spawn BIS_WF_UpdateCommandsFromServer;
[] Spawn BIS_WF_UpdateTime;

//BIS_WF_Client = BIS_WF_groupLogic createUnit ["Logic", [0,0,0], [], 0, "NONE"]; 
BIS_WF_Client SetVariable ["missionDescription",Localize "STRWFNONE"];
BIS_WF_Client SetVariable ["missionWP",[]];
BIS_WF_Client SetVariable ["missionStartTime",0];
BIS_WF_Client SetVariable ["totalWPs",0];
BIS_WF_Client SetVariable ["missionTasks",[]];
BIS_WF_Client SetVariable ["missionTasksCreated",0];
BIS_WF_Client SetVariable ["showTeamMarkerByType",true];

if (!IsServer) then
{
	//Clear any initialization markers.
	_count = 0;
	_marker = "BIS_WF_InitRegion0";
	_markedPosition = GetMarkerPos _marker;

	while {(_markedPosition Select 0) != 0 && (_markedPosition Select 1) != 0 && _count < 100} do
	{
		DeleteMarkerLocal _marker;

		_count = _count + 1;
		_marker = Format["BIS_WF_InitRegion%1",_count];
		_markedPosition = GetMarkerPos _marker;
	};
};

if (!(BIS_WF_Common GetVariable "disableVoting")) then
{
	player CreateDiarySubject ["Voting", localize "STR_wfvoting"];  //do not localize first item! (not visible, used in scripts)
};

player CreateDiarySubject ["Teams",Localize "STRWFTEAMS"];

if (!BIS_WF_NoFastTravel) then
{
	player CreateDiarySubject ["FastTravel", localize "strwffasttravel"]; //do not localize first item!
	player CreateDiaryRecord ["FastTravel",[localize "str_disp_int_skip" + " " + localize "strwffasttravel", "<execute expression=""BIS_WF_FastTravelSkip = true;  hint (localize &quot;strwffasttravel&quot; + &quot; &quot; + localize &quot;str_cfg_markers_end&quot;);"">"+localize "str_disp_int_skip" + " " + localize "strwffasttravel"+"</execute>"]]; //do not localize first item!
};

if (IsMultiplayer) then
{
	//Create task to disable automatic mission assignments.
	_task = player CreateSimpleTask[Localize "STR_WFNOTASKAUTOASSIGNMENT"];
	_task SetSimpleTaskDescription[Localize "STR_WFNOTASKDESCRIPTION",Localize "STR_WFNOTASKAUTOASSIGNMENT",""];

	//Add Diplomacy Menu if it is a 3-sided game.
	if (!BIS_WF_DisableDiplomacy && Count (BIS_WF_Common GetVariable "inactiveSides") < 1) then
	{
		player CreateDiarySubject ["Diplomacy",Localize "STRWFDIPLOMACY"];
	};
};

if (BIS_WF_SupportLevel != 0) then
{
	player CreateDiarySubject ["Support",Localize "STR_MARKER_SUPPORT"];
//	player CreateDiaryRecord ["Support",Format["%1 (%2)",Localize "STR_SOM_REQUEST_SUPPORT",Localize "STR_ART_H1"]];
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// HIGH COMMAND
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Get factions defined for this mission's team templates.
_factions = Call Compile Format["%1TeamTemplateFactionIndex",sideJoinedText];

//Pre-build static data for HC menu so it does not have to make potentially slow queries.
_teamTypes = [];

{
	if (!(_x In _teamTypes) && _x != "Special") then {_teamTypes = _teamTypes + [_x]};
} ForEach (Call Compile Format["%1TeamTemplateTypes",sideJoinedText]);

//Store list for use by UI so human commanders can set AI team types.
BIS_WF_Client SetVariable["teamTypes",_teamTypes];

//Create list of default team templates for each type for each faction.
_factionTypeIndex = [];

{
	_index = [];
	_faction = _x;

	{
		//Get all templates of type.
		_templates = [sideJoined,_x,_faction] Call BIS_WF_GetTeamTemplates;

		if (Count _templates > 0) then
		{
			//Get index to first template (will be default template for that type).
			_index = _index + [_templates Select 0];
		}
		else
		{
			//If none found then set index to type "None".
			_index = _index + [0];
		};
	} ForEach _teamTypes;

	_factionTypeIndex = _factionTypeIndex + [_index];
} ForEach _factions;

//Store list for use by UI so human commanders can set AI team types.
BIS_WF_Client SetVariable["factionTypeIndex",_factionTypeIndex];

//--- Cycle to fill Mission list in Group menu
[] Spawn
{
	while {true} do {

		//--- High Command bar is visible
		waituntil {Sleep 0.1;hcshownbar};

		//--- Register 'Missions' command menu
		_missions = BIS_WF_Common getvariable format ["%1Missions",WFSideText playerside];
		_missionStrings = [];
		_missionNames = [];
		for "_i" from 0 to (count _missions - 1) do {
			_element = _missions select _i;
			_locationName = "XYZ";
			if (TypeName _location == "OBJECT") then {_locationName = _location GetVariable "name"};
			_missionDesc = format [(_element select 0) call BIS_WF_GetMissionDescription,_locationName];

			//_missionDesc = (_element select 0) call BIS_WF_GetMissionDescription;
			_missionStrings = _missionStrings + [[(_element select 0),(_element select 1)]];
			_missionNames = _missionNames + [_missionDesc];
		};

		[
			Localize "STRWFMISSIONS",	//--- Menu name
			"HC_Missions",				//--- Variable
			_missionNames,				//--- Items
			"",					//--- Submenu
								//--- Expression START
			"
				_mission = %3 select 0;
				_missions = BIS_WF_Common getvariable format ['%1Missions',WFSideText playerside];

				comment '--- Add waypoint';
				_codeWP = bis_hc_mainscope getvariable 'GUI_WP_MOVE';
				[false,_mission select 1,false,true,false] spawn _codeWP;

				comment '--- Set task for each member';
				{
					_group = _x;
					{
						_task = (((BIS_WF_Client getvariable 'missiontasks') select (_mission select 0)) select 0);
						_nic = [nil,_x,rSETCURRENTTASK,_x,_task] call RE;
					} foreach units _group;
					([_group] + _mission) call BIS_WF_AddTeamToMission;
				} foreach hcselected player;
			",					//--- Expression END
			_missionStrings
		] call bis_fnc_createmenu;


		sleep 1; //--- Who needs mission list updated more often than every 1 second?
	};
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Private ["_initClient"];
_initClient = 
{
	//Wait for server to send teams data.
	if (!IsServer) then
	{
		WaitUntil {!IsNil Format["%1Teams",sideJoinedText]};
		WaitUntil {Call Compile Format["Count %1Teams > 0",sideJoinedText]};
		WaitUntil {Call Compile Format["Group player In %1Teams",sideJoinedText]};

		WaitUntil {gameInitialized};
	};

	//Determine client's ID (1 - 32, etc.).  This is the slot # they joined the game as.
	clientID = player Call GetClientID;
	if (clientID < 1) then {player SideChat "Client_Init.sqs: ERROR OCCURRED! Could not determine clientID."};

	//Set up HQ voices for client.
	if (!IsServer) then
	{
		{
			_x SetIdentity ((_x GetVariable "_topic_identity") + Str (Floor Random 3));
			_x SetRank "COLONEL";
			_x SetGroupId [localize "STR_DN_WARFARE_HQ_BASE_UNFOLDED"];
			_x KbAddTopic [(_x getVariable "_topic_identity"),"\ca\warfare2\kb\hq.bikb","\ca\warfare2\kb\hq.fsm",{Call Compile PreprocessFileLineNumbers "\ca\warfare2\kb\hq.sqf"}];
		} ForEach [BIS_WF_HQEAST,BIS_WF_HQWEST,BIS_WF_HQRESISTANCE];
	};

	[0,0,0] Call BIS_WF_InitAllTowns;

	"Respawn_west" SetMarkerTypeLocal "Empty";
	"Respawn_east" SetMarkerTypeLocal "Empty";
	"respawn_guerrila" SetMarkerTypeLocal "Empty";

	if (BIS_WF_Classic) then
	{
		BIS_WF_UseNearbyStructureAction = "if ((_this select 1) In actionKeys ""TeamSwitch"") then {[] Exec (corePath + 'Client\Action\Action_OpenOptionsMenu.sqs')};";
	}
	else
	{
		BIS_WF_UseNearbyStructureAction =
		"
			if ((_this select 1) In actionKeys 'TeamSwitch') then
			{
				_temp = [] Spawn BIS_WF_ActionUseNearbyStructure;
			}
			else
			{
				if ((_this select 1) In actionKeys 'TeamSwitchPrev') then
				{
					if (BIS_WF_BuyGearInRange) then
					{
						if (dialog) then {CloseDialog 0}
						else {CreateDialog ('WFGearMenu' + BIS_WF_UI)};
					};
				};

				if ((_this select 1) In actionKeys 'TeamSwitchNext') then
				{
					{
						_text = Format['[%1] ',_x Call BIS_WF_GetTeamID];

						if (IsPlayer Leader _x) then {_text = _text + Name Leader _x};

						_data = GetGroupIconParams _x;
						_data Set[1,_text];
						_x SetGroupIconParams _data;
					} ForEach (Call Compile Format['%1Teams',sideJoinedText]);
				};
			};
		";
	};

	[] Spawn
	{
		Sleep 2;
		(FindDisplay 46) DisplaySetEventHandler ["keydown",BIS_WF_UseNearbyStructureAction];
	};

	//Game has started so execute any client-side loops.
	[] Exec (corePath + "Client\Client_UpdateRespawn.sqs");
	[] Exec (corePath + "Client\GUI\GUI_UpdateCampCapture.sqs");
	[] Exec (corePath + "Client\Client_UpdateActions.sqs");

	[sideJoined] Call Compile PreprocessFile (corepath + "Client\Init\Init_Markers.sqf");
	[BIS_WF_Common GetVariable Format["All%1Factions",sideJoinedText]] Call Compile PreprocessFile (corepath + "Client\Init\Init_GearData.sqf");
	[BIS_WF_Common GetVariable Format["All%1Factions",sideJoinedText]] Call Compile PreprocessFile (corepath + "Client\Init\Init_FactoryData.sqf");

	if ((BIS_WF_Common GetVariable "coopGame") || (BIS_WF_Common GetVariable "SPGame")) then
	{
		BIS_WF_RespawnWeaponsDefault = Weapons player;
		BIS_WF_RespawnAmmoDefault = Magazines player;
		BIS_WF_RespawnCostDefault = 100;
	}
	else
	{
		BIS_WF_RespawnWeaponsDefault = Call Compile Format["%1DefaultWeapons",sideJoinedText];
		BIS_WF_RespawnAmmoDefault = Call Compile Format["%1DefaultAmmo",sideJoinedText];
		BIS_WF_RespawnCostDefault = 100;
	};

	BIS_WF_RespawnWeapons = BIS_WF_RespawnWeaponsDefault;
	BIS_WF_RespawnAmmo = BIS_WF_RespawnAmmoDefault;
	BIS_WF_RespawnCost = [BIS_WF_RespawnWeapons,BIS_WF_RespawnAmmo] Call BIS_WF_GetGearCost;

	[] Call BIS_WF_PlayerRespawn;

	[] Call Compile PreprocessFile (corepath + "Client\Init\Init_Coin.sqf");

	[] Spawn BIS_WF_UpdateClient;
	[] Spawn BIS_WF_UpdateAvailableActions;
	[] Spawn BIS_WF_UpdateAvailableObjects;
	[] Spawn BIS_WF_UpdateMissionMarker;
	[] Spawn BIS_WF_UpdateMissionMarkers;
	[sideJoined] Spawn BIS_WF_UpdateTeamMarkers;
	[] Spawn BIS_WF_UpdateTownCapture;
	[] Spawn BIS_WF_UpdateTownMarkers;

	_vehicles = player NearEntities ["Car",50];
	{player Reveal _x} ForEach _vehicles;

	Private["_sides"];
	_sides = [East,West,Resistance];
	_sides = _sides - [sideJoined];

	{
		if ([_x,sideJoined] Call BIS_WF_AreAllies) then
		{
			sideJoined SetFriend [_x,1];
			_x SetFriend [sideJoined,1];
		};
	} ForEach _sides;

	if (!IsServer) then
	{
		WaitUntil {!IsNil "bis_fnc_createmenu" && !IsNil "bis_hc_mainscope" && !IsNil "BIS_marta_mainscope"};
	};

	//Create Set Team Type HC menu.
	[
		Localize "STRWFSETTYPE",				//Menu name
		"HC_Custom",							//Variable
		BIS_WF_Client GetVariable "teamTypes",	//Items
		"",										//Submenu
		"[%2] Call BIS_WF_SetHCTeamType;",		//Expression
		BIS_WF_Client GetVariable "teamTypes"
	] Call bis_fnc_createmenu;

	if (!IsNil "bis_hc_mainscope") then
	{
		bis_hc_mainscope SetVariable ["addAllGroups",false];
	};

	if (!IsNil "BIS_marta_mainscope") then
	{
		if (BIS_WF_RelativeSideColors) then
		{
			switch (Side player) do
			{
				case East:
				{
					if (BIS_WF_Classic) then
					{
						player SetVariable ["MARTA_SHOWRULES",["RU",-1,"INS",-1]];
					};

					BIS_marta_mainscope SetVariable ["rules",[["o_",[0,0.7,0,1]],["b_",[1,0,0,1]],["n_",[1,0,0,1]],["n_",[0,0,1,1]]]];
				};

				case West:
				{
					if (BIS_WF_Classic) then
					{
						player SetVariable ["MARTA_SHOWRULES",["USMC",-1,"CDF",-1]];
					};

					BIS_marta_mainscope SetVariable ["rules",[["o_",[1,0,0,1]],["b_",[0,0.7,0,1]],["n_",[1,0,0,1]],["n_",[0,0,1,1]]]];
				};

				case Resistance:
				{
					if (BIS_WF_Classic) then
					{
						player SetVariable ["MARTA_SHOWRULES",["GUE",-1]];
					};

					BIS_marta_mainscope SetVariable ["rules",[["o_",[1,0,0,1]],["b_",[1,0,0,1]],["n_",[0,0.7,0,1]],["n_",[0,0,1,1]]]];
				};

				case Civilian:
				{
					if (BIS_WF_Classic) then
					{
						player SetVariable ["MARTA_SHOWRULES",["CIV",-1]];
					};

					BIS_marta_mainscope SetVariable ["rules",[["o_",[0,0,1,1]],["b_",[0,0,1,1]],["n_",[0,0,1,1]],["n_",[0,0,1,1]]]];
				};
			};
		};

		SetGroupIconsVisible [true,false]; //Show only 2D
		player SetVariable ["MARTA_HIDE",[]];
	};

	if (!IsServer) then
	{
		WaitUntil {!IsNil Format["%1CommanderVoteTime",sideJoinedText]};
	};

	//Check if a vote is in progress.  May not be if player connects after start of game.
	_voteTime = Call Compile Format["%1CommanderVoteTime",sideJoinedText];
	if (_voteTime > 0) then {if (!BIS_WF_Classic) then {CreateDialog "RscDisplayWFVoting";} else {CreateDialog "CommanderVoteMenu";};};

	//All below (in this block) are probably obsolete now.
	eastStarterTeams = [];
	westStarterTeams = [];

	BIS_WF_MissionsCompleted = Call Compile Format["%1Player%2MissionsCompleted",sideJoinedText,clientID];
	BIS_WF_MissionsFailed = Call Compile Format["%1Player%2MissionsFailed",sideJoinedText,clientID];
};

//If this is the server or SP then init client immediately.
if (IsServer) then {[] Call _initClient}
else {[] Spawn _initClient};

WaitUntil {!IsNil {Call Compile Format["%1StartingLocation",sideJoinedText]}};
PreloadCamera (Call Compile Format["GetPos %1StartingLocation",sideJoinedText]);

BIS_WF_CoreClientInitialized = true;

if (sideJoined in (BIS_WF_Common GetVariable "defeatedSides")) then {[sideJoined] Call CLTFNCSideDefeated};

BIS_WF_ShowPlayersOnHUD = false;

//Monitor for display of players on HUD and update if needed.
[] Spawn
{
	Private["_firstUpdate","_lastVisible","_playerTeams","_teams"];

	_firstUpdate = true;

	while {!gameOver} do
	{
		SetGroupIconsVisible [true,false];
		_lastVisible = VisibleMap;
		BIS_marta_mainscope SetVariable["fadeTime",5];

		while {!BIS_WF_ShowPlayersOnHUD} do
		{
			if (_firstUpdate) then
			{
				//Hide all teams on same side except playable ones.
				_teams = [];

				{
					if (Side _x == sideJoined) then {_teams = _teams + [_x]};
				} ForEach allGroups;

				_teams = _teams - (Call Compile Format["%1Teams",sideJoinedText]);
				player SetVariable ["MARTA_HIDE",_teams];
				_firstUpdate = false;
			};

			Sleep 1;
		};

		_firstUpdate = true;

		while {BIS_WF_ShowPlayersOnHUD} do
		{
			SetGroupIconsVisible [true,true];
			BIS_marta_mainscope SetVariable["fadeTime",0.1];

			if (VisibleMap) then
			{
				if (!_lastVisible || _firstUpdate) then
				{
					//Hide all teams on same side except playable ones.
					_teams = [];

					{
						if (Side _x == sideJoined) then {_teams = _teams + [_x]};
					} ForEach allGroups;

					_teams = _teams - (Call Compile Format["%1Teams",sideJoinedText]);
					player SetVariable ["MARTA_HIDE",_teams];
					_firstUpdate = false;
				};
			}
			else
			{
				if (_lastVisible || _firstUpdate) then
				{
					//Show player-only teams on HUD.
					_playerTeams = [];

					{
						if (IsPlayer Leader _x) then {_playerTeams = _playerTeams + [_x]};
					} ForEach Call Compile Format["%1Teams",sideJoinedText];

					player SetVariable ["MARTA_HIDE",allGroups - _playerTeams];
					_firstUpdate = false;
				};
			};
			
			_lastVisible = VisibleMap;
			Sleep 0.1;
		};

		_firstUpdate = true;
	};
};

//*****************************************************************************************
//1/17/7 MM - Created file.