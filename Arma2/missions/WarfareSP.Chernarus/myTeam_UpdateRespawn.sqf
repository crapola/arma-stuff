//Last modified 4/6/10
//*****************************************************************************************
//Description: Handles respawn of AI Leader.
//*****************************************************************************************
Private["_ammo","_barracks","_base","_bodyPosition","_data","_heavy","_heavyTypes","_infantryTypes","_lastBasePosition","_leader","_light","_lightTypes","_list","_position","_replaceUnit","_respawnable","_side","_sideText","_sorted","_team","_teamTemplate","_teamType","_weapons"];

_team = _this Select 0;

diag_log "--> myTeam_UpdateRespawn.sqf";

_side = Side _team;
_sideText = WFSideText _side;

_infantryTypes = Call Compile Format["%1InfantryTeamTypes",_sideText];
_lightTypes = Call Compile Format["%1LightTeamTypes",_sideText];
_heavyTypes = Call Compile Format["%1HeavyTeamTypes",_sideText];

_soldierType = Call Compile Format["%1SOLDIER",_side Call GetSideLetter];

//If default soldier class is not of the same faction as team, then get a new default soldier.
if ((_team GetVariable "faction") != GetText (configFile >> "CfgVehicles" >> _soldierType >> "faction")) then
{
	{
		//Find matching faction in soldiers list.
		if (GetText (configFile >> "CfgVehicles" >> _x >> "faction") == (_team GetVariable "faction")) then
		{
			_soldierType = _x;
		};
	} ForEach Call Compile Format["%1SOLDIERS",_side Call GetSideLetter];
};

_barracks = "";
_light = "";
_heavy = "";

//Query mission config for defined structure types.
_data = [_side,"Barracks"] Call BIS_WF_GetBaseStructureData;
if (Count _data > 0) then {_barracks = _data Select 2};

_data = [_side,"Light"] Call BIS_WF_GetBaseStructureData;
if (Count _data > 0) then {_light = _data Select 2};

_data = [_side,"Heavy"] Call BIS_WF_GetBaseStructureData;
if (Count _data > 0) then {_heavy = _data Select 2};

_weapons = Call Compile Format["%1DefaultWeapons",_sideText];
_ammo = Call Compile Format["%1DefaultAmmo",_sideText];

_specOpsWeapons = Call Compile Format["%1SpecOpDefaultWeapons",_sideText];
_specOpsAmmo = Call Compile Format["%1SpecOpDefaultAmmo",_sideText];

_lastBasePosition = [];

ScopeName "UpdateRespawn";

WaitUntil {Alive Leader _team};

//!
// removed ismulti condtition
//_respawnable = (IsMultiplayer && (Leader _team In playableUnits));
_respawnable = (Leader _team In playableUnits);

while {!gameOver && !(_side In (BIS_WF_Common GetVariable  "defeatedSides"))} do
{
	Sleep 0.5;
	//Remain inactive for player teams and until AI leader is dead.
	while {Alive Leader _team || IsPlayer Leader _team} do {Sleep 0.5};

	_timeToRespawn = time + (BIS_WF_Constants GetVariable "RESPAWNDELAY");
	_bodyPosition = GetPos Leader _team;
	_replaceUnit = false;

	if (_respawnable) then
	{
		//If leader has died then wait for a respawn.
		WaitUntil {Sleep 0.1;Alive Leader _team || IsPlayer Leader _team};

		//If AI then try to replace another team member.
		_replaceUnit = true;
	}
	//If a non-respawnable team leader then create a new leader now.
	else
	{
		Private["_units"];

		_units = Units _team Call GetLiveUnits;
		_transports = [Units _team] Call BIS_WF_GetTransports;

		//Do not use infantry team transport drivers as leaders.
		if (([_team] Call BIS_WF_GetTeamTemplateTypeName) == Localize "STR_WF_TEAMINF") then
		{
			{
				if (!IsNull Driver _x) then {_units = _units - [Driver _x]};
			} ForEach _transports;
		};

		if (Count _units > 0) then
		{
			_team SelectLeader (_units Select 0);
		}
		else
		{
			WaitUntil {Sleep 0.1; time >= _timeToRespawn};

//			if (!([_side] Call BIS_WF_BaseStructuresExist)) then
			if (_side In (BIS_WF_Common GetVariable  "defeatedSides")) then
			{
//Debug
TextLog Format["WF Debug: %1 non-respawnable team %2 (%3) disbanding since base is destroyed.",_sideText,_team Call BIS_WF_GetTeamID,_team,_leader];

				BreakTo "UpdateRespawn";
			};

			_leader = _team CreateUnit[_soldierType,[Call Compile Format["%1Base",_sideText],10,25] Call GetRandomPosition,[],0.7,"FORM"];
			[_leader,_side] Call BIS_WF_InitUnit;
			_team SelectLeader _leader;

			//If AI then try to replace another team member.
			_replaceUnit = true;

			if (Local player) then
			{
				Private["_commanderTeam"];

				_commanderTeam = Call Compile Format["%1CommanderTeam",_sideText];
				if (!IsNull _commanderTeam) then
				{
					if (_commanderTeam == Group player && player == Leader Group player) then
					{
						player HCSetGroup [_team];
					};
				};
			};
		};
	};
	
	if (!IsPlayer Leader _team) then
	{
		Private["_returnToBase","_units"];

		_units = [];
		_returnToBase = false;
		_leader = Leader _team;

		_base = Call Compile Format["%1Base",_sideText];
		if (!IsNull _base) then {_lastBasePosition = Position _base};	

		//If team was wiped out then respawn at nearest factory.
		if (Count (Units _team Call GetLiveUnits) == 1) then
		{
			Private["_structures"];
			_teamTemplate = _team GetVariable "AITemplate";
			_structures = Call Compile Format["%1BaseStructures",_sideText];
			_list = [];

			//Find closest factory to respawn to.
			if (_teamTemplate In _infantryTypes) then
			{
				{if ((TypeOf _x) == _barracks && GetDammage _x < 1) then {_list = _list + [_x]};} ForEach _structures;
			};

			if (_teamTemplate In _lightTypes) then
			{
				{if ((TypeOf _x) == _light && GetDammage _x < 1) then {_list = _list + [_x]};} ForEach _structures;
			};

			if (_teamTemplate In _heavyTypes) then
			{
				{if ((TypeOf _x) == _heavy && GetDammage _x < 1) then {_list = _list + [_x]};} ForEach _structures;
			};

			_position = [Position _base,10,25] Call GetRandomPosition;
			if (IsNull _base) then {_position = [_lastBasePosition,10,25] Call GetRandomPosition;};

			if (Count _list > 0) then
			{
			_sorted = [_bodyPosition,_list] Call BIS_WF_SortByDistance;
				_position = [Position (_sorted Select 0),10,25] Call GetRandomPosition;
			};

TextLog Format["WF: %1 Team %2 (%3) wiped out.",_sideText,_team Call BIS_WF_GetTeamID, _team];

				//If no team member available, then team has been wiped out.
				//Respawn at the base and run all strategic updates.
				_team SetVariable["nextMediumUpdateTime",time + 30];
				_team SetVariable["nextLongUpdateTime",time - 1];

				//Reset current mission so team can select nearest mission after respawn.
				[_team] Call BIS_WF_RemoveTeamFromMissions;

				//Clear previous destination/regroup at position.
				_leader SetPos _position;
				_team Move _position;
		}
		else
		{
			if (_replaceUnit) then
			{
				_templateData = [_team] Call BIS_WF_GetTeamTemplateData;
				if (Count _templateData > 0) then
				{
					//Respawn behavior is based on team type.
					switch (_templateData Select 4) do
					{
						//Try to respawn into passenger in transport first.
						//Then try to respawn in soldier on foot.
						//Then try to respawn into transport driver.
						//If nothing available respawn at base.
						case Localize "STR_WF_TEAMINF":
						{
							Private["_transports"];
	
							//Get closest functional transport with driver.
							_transports = [_bodyPosition,[Units _team] Call BIS_WF_GetTransports] Call BIS_WF_SortByDistance;
							_drivers = [];
	
							//Get all living cargo on same team in all transports.
							{
								if (!IsNull Driver _x) then
								{
									if (Alive Driver _x) then
									{
										_drivers = _drivers + [Driver _x];
									};
								};

								{
									if (Group _x == _team && _x != Vehicle _x) then {_units = _units + [_x]};
								} ForEach (AssignedCargo _x Call GetLiveUnits);
							} ForEach _transports;
	
							//If no passengers in transports then find soldiers on foot.
							if (Count _units < 1) then
							{
								_units = [_bodyPosition,Units _team Call GetLiveUnits] Call BIS_WF_SortByDistance;
							};
	
							_units = _units - _drivers;
							_units = _units - [_leader];

							//If no one on foot, then get transport driver if available.
							if (Count _units < 1) then
							{
								{
									if (!IsNull Driver _x) then
									{
										if (Alive Driver _x) then
										{
											_returnToBase = true;
											_leader SetPos ([_x,15,10] Call BIS_WF_GetSafePosition);
										};
									};

									//_units = _units + [Driver _x];
								} ForEach _transports;
							};
						};
	
						//Try to respawn into crew in nearest vehicle first.
						//Then try to respawn in soldier on foot.
						//If nothing available respawn at base.
						case Localize "STR_WF_TEAMMECH":
						{
							Private["_vehicles"];
							ScopeName "Mechanized";
	
							_vehicles = [_bodyPosition,[Units _team] Call BIS_WF_GetVehicles] Call BIS_WF_SortByDistance;
	
							//Get closest commander, gunner, or driver.
							{
								if (!IsNull Commander _x) then
								{
									if (Alive Commander _x && Group Commander _x == _team) then
									{
										_units = [Commander _x];
										BreakTo "Mechanized";
									};
								};
	
								if (!IsNull Gunner _x) then
								{
									if (Alive Gunner _x && Group Gunner _x == _team) then
									{
										_units = [Gunner _x];
										BreakTo "Mechanized";
									};
								};
	
								if (!IsNull Driver _x) then
								{
									if (Alive Driver _x && Group Driver _x == _team) then
									{
										_units = [Driver _x];
										BreakTo "Mechanized";
									};
								};
							} ForEach _vehicles;
	
							if (Count _units < 1) then
							{
								_units = [_bodyPosition,Units _team Call GetLiveUnits] Call BIS_WF_SortByDistance;
							};
						};
	
						case Localize "STR_WF_TEAMARM":
						{
							Private["_vehicles"];
							ScopeName "Armor";
	
							_vehicles = [_bodyPosition,[Units _team] Call BIS_WF_GetVehicles] Call BIS_WF_SortByDistance;
	
							//Get closest commander, gunner, or driver.
							{
								if (!IsNull Commander _x) then
								{
									if (Alive Commander _x && Group Commander _x == _team) then
									{
										_units = [Commander _x];
										BreakTo "Armor";
									};
								};
	
								if (!IsNull Gunner _x) then
								{
									if (Alive Gunner _x && Group Gunner _x == _team) then
									{
										_units = [Gunner _x];
										BreakTo "Armor";
									};
								};
	
								if (!IsNull Driver _x) then
								{
									if (Alive Driver _x && Group Driver _x == _team) then
									{
										_units = [Driver _x];
										BreakTo "Armor";
									};
								};
							} ForEach _vehicles;
						};
						default
						{
							//Get nearest living team member to death location.
							_units = [_bodyPosition,Units _team Call GetLiveUnits] Call BIS_WF_SortByDistance;
						};
					};
				};

				//Replace available team member with leader.
				if (Count _units > 0) then {[_units Select 0,Leader _team] Spawn BIS_WF_ReplaceTeamMember}
				else
				{
					//If no team member available, then team has been wiped out.
					//Rerun all strategic updates.
					if (!(_team GetVariable "RTB")) then {_team SetVariable["RTB",true,true]};

					_team SetVariable["nextMediumUpdateTime",time + 60];
					_team SetVariable["nextLongUpdateTime",time + 60];
					[_team] Call BIS_WF_RemoveTeamFromMissions;

					if (!_returnToBase) then
					{
						_leader SetPos ([_lastBasePosition,10,25] Call GetRandomPosition);
						_team Move ([_lastBasePosition,10,25] Call GetRandomPosition);

						//If respawning at base then run updates sooner.
						_team SetVariable["nextMediumUpdateTime",time + 30];
						_team SetVariable["nextLongUpdateTime",time - 1];
					};
				};

				if (_returnToBase) then {[_team,[_lastBasePosition,10,25] Call GetRandomPosition] Call BIS_WF_AITeamMove};
			};
		};

		_leader RemoveAllEventHandlers "killed";
		[_leader,_side] Call BIS_WF_InitUnit;

		//Re-equip leader if non-respawnable unit.
		if (!(_team In playableUnits) || !IsMultiplayer) then
		{
			_teamType = [_team] Call BIS_WF_GetTeamTemplateTypeName;
			if (_teamType == Localize "STR_WF_TEAMSPEC") then
			{
				[Leader _team,_specOpsWeapons,_specOpsAmmo] Call EquipLoadout;
				Leader _team SetSkill 0.7;
			}
			else
			{
				[_leader,_weapons,_ammo] Call EquipLoadout;						
				Leader _team SetSkill 0.5;
			};
		};
	};
};

//*****************************************************************************************
//9/2/8 MM - Created file.

