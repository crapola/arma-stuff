scriptName "myClient_PlayerRespawn.sqf";
//Last modified 4/22/10
//*****************************************************************************************
//Description: This is run when a player respawns.
//*****************************************************************************************
Private["_count","_teams"];

diag_log "--> myClient_PlayerRespawn.sqf";

hint "Player Respawn";

if (BIS_WF_Classic) then
{
	player AddAction [Localize "STRWFOPTIONS",corePath + "Client\Action\Action_OpenOptionsMenu.sqs",false,-500,false,false,"TeamSwitch"];
}
else
{
	player AddAction [localize "STR_Client_PlayerRespawn.sqf0",corePath + "Client\GUI\GUI_buyPrepareMenu.sqf", ['gear'], 100, false, true, "", "BIS_WF_BuyGearInRange"]; //"Buy weapons"
	player AddAction [localize "STR_Client_PlayerRespawn.sqf1",corePath + "Client\GUI\GUI_buyPrepareMenu.sqf", ['units'], 100, false, true, "", "BIS_WF_BuyUnitsInRange"]; //"Buy units"
	player AddAction [Localize "STR_COIN_ACTION",corePath + ("Client\Action\Action_OpenCoin.sqf"),BIS_WF_CoinManager,1,false,false,"","!IsNull BIS_WF_CoinVehicle"];

	player AddAction [Localize "STR_WFVOTING",corePath + ("Client\Action\Action_OpenVoteMenu.sqf"),ObjNull,1,false,false,"",Format["%1CommanderVoteTime > 0",sideJoinedText]];
};

[player,BIS_WF_RespawnWeapons,BIS_WF_RespawnAmmo] Call EquipLoadout;

player RemoveAllEventHandlers "killed";

//if (IsMultiplayer) then {player AddEventHandler ["killed",{_this Spawn BIS_WF_ClientKilled}]};
// We remove the IsMultiplayer condition
player AddEventHandler ["killed",{_this Spawn BIS_WF_ClientKilled}];

[player,sideJoined] Call BIS_WF_InitUnit; // this is Common\Functions\Common_InitUnit.sqf

//Make sure player has basic equipment.
{
	if (!(_x In Weapons player)) then {player AddWeapon _x};
} ForEach ["ItemCompass","ItemMap","ItemRadio","ItemWatch","Binocular","NVGoggles"];

//Ensure player has minimum respawn funds.
if (Call BIS_WF_GetPlayerFunds < (BIS_WF_Constants GetVariable "MINRESPAWNFUNDS")) then
{
	(BIS_WF_Constants GetVariable "MINRESPAWNFUNDS") Call BIS_WF_ChangePlayerFunds;
};

//These EHs are being removed after death...
[] Spawn
{
	Sleep 1;

	if (BIS_WF_CommanderEH1 != -1 || BIS_WF_CommanderEH2 != -1) then
	{
		BIS_WF_CommanderEH1 = ((FindDisplay 12) DisplayCtrl 51) CtrlAddEventHandler ["mouseButtonDown","['mouseButtonDown',_this] Call BIS_WF_MapClickEH;"];
		BIS_WF_CommanderEH2 = ((FindDisplay 12) DisplayCtrl 51) CtrlAddEventHandler ["mouseButtonUp","['mouseButtonUp',_this] Call BIS_WF_MapClickEH;"];
	};
};

// Add back map menu stuff. Code from init_client.

if (!(BIS_WF_Common GetVariable "disableVoting")) then
{
	player CreateDiarySubject ["Voting", localize "STR_wfvoting"];  //do not localize first item! (not visible, used in scripts)
};

player CreateDiarySubject ["Teams",Localize "STRWFTEAMS"];

if (!BIS_WF_NoFastTravel) then
{
	player CreateDiarySubject ["FastTravel", localize "strwffasttravel"]; //do not localize first item!
	player CreateDiaryRecord ["FastTravel",[localize "str_disp_int_skip" + " " + localize "strwffasttravel", "<execute expression=""BIS_WF_FastTravelSkip = true;  hint (localize &quot;strwffasttravel&quot; + &quot; &quot; + localize &quot;str_cfg_markers_end&quot;);"">"+localize "str_disp_int_skip" + " " + localize "strwffasttravel"+"</execute>"]]; //do not localize first item!
};