waituntil {!isnil "bis_fnc_init"};
//#define DEBUG
#include "functions.sqf";

player createDiaryRecord ["Diary",["Support"  ,"You can radio a MH-60S on 0-0-1 to distract the enemy."]];
player createDiaryRecord ["Diary",["Execution","You start <marker name='marker_start'>on the beach</marker>, proceed to <marker name='marker_strelka'>Strelka</marker> and follow the road to the <marker name='marker_base'>enemy base</marker> while killing everyone on your path."]];
player createDiaryRecord ["Diary",["Mission"  ,"Our mission is to get rid of a couple ChKDZ strongpoints on Utes."]];

TSK_STRELKA=player createSimpleTask [""];
TSK_STRELKA setSimpleTaskDescription ["Neutralize enemy forces in <marker name='marker_strelka'>Strelka</marker>.","Secure Strelka","Strelka"];
TSK_STRELKA setSimpleTaskDestination (getMarkerPos "marker_strelka");
TSK_BASE=player createSimpleTask [""];
TSK_BASE setSimpleTaskDescription ["Take hold of the <marker name='marker_base'>base</marker>.","Assault base","Enemy base"];
TSK_BASE setSimpleTaskDestination (getMarkerPos "marker_base");

player setcurrenttask TSK_STRELKA;
group player setgroupid ["Honey Badger","GroupColor7"];

sleep 1;
[objnull,objnull,TSK_STRELKA,"current"] call FNC_TASKHINT;
execfsm "hardmode.fsm";

#ifdef DEBUG
player addeventhandler ["handledamage",{}];
onMapSingleClick "vehicle player setPos [_pos select 0,_pos select 1,1];";
player addaction ["Camera","camera.sqs"];
#endif

// Stop rain after a while
[] spawn
{
	waituntil
	{
		sleep (60*12);true;
	};
	70 setfog 0;
	120 setovercast 0;
}