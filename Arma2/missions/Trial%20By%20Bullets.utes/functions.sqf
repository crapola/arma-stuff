// Fade to endmission.
// 0: (string) Endmission parameter
FNC_FADETOEND=
{
	cuttext ["","BLACK OUT",2];
	[_this select 0] spawn
	{
		sleep 2;
		endmission (_this select 0);
	};
};

// Task hint from MPframework.
// 0: caller
// 1: target
// 2: (task) Task
// 3: (string) Can be "created","current","succeeded","failed","canceled"
FNC_TASKHINT=compile preprocessfilelinenumbers "\ca\modules\mp\data\scriptcommands\taskhint.sqf";

// Set task to complete and show hint
FNC_COMPLETETASK=
{
	if (!isnull (_this select 0)) then
	{
		[objNull,objNull,_this select 0,"succeeded"] call FNC_TASKHINT;
		_this select 0 settaskstate "succeeded";
	};
};