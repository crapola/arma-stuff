#include "CommonUI\rscControls.hpp"

class SelectPlaneDialog
{
	idd = -1;
	movingEnable = false;
	enableSimulation = true;

	onLoad="_this execVM \
			'UI\planeDialog_onLoad.sqf'";

	class ControlsBackground
	{
		class Background: RscText
		{
			idc=-1;
			y = 0.05;
			w = 0.59375;
			h = 0.95;
			colorBackground[] = COLOR_BACK_GREEN;
		}
		class Title : RscText
		{
			idc=-1;
			style=ST_CENTER;
			w = 0.59375;
			h = 0.0525;
			text="Pick a plane";
			colorBackground[] = COLOR_BACK_BROWN;
		}
		class BackgroundBorder: RscText
		{
			idc=-1;
			style=ST_FRAME;
			w = 0.59375;
			h = 1;
			colorText[] = COLOR_BLACK;
		}
	}

	class Objects
	{
	}

	class Controls
	{
		class PickSide : RscCombo
		{
			idc = 2000;
			x = 0.03125;
			y = 0.1;
			//w = 0.3125;
			w = 0.375;
			h = 0.05;
			default=true;
			onLBSelChanged = "[0,_this] call compile preprocessfilelinenumbers \
							  'UI\planeDialog_controls.sqf'";
		}
		class List : RscListbox
		{
			idc = 1500;
			x = 0.03125;
			y = 0.2;
			w = 0.53125;
			h = 0.65;
			period = 1;
			colorBackground[] = {0,0,1,1};
			colorSelectBackground[] = {1,0.537,0,0.5};
			colorSelectBackground2[] = {1,0.537,0,1.0};
			onLBSelChanged = "[1,_this] call compile preprocessfilelinenumbers \
							  'UI\planeDialog_controls.sqf'";
		}
		class Start : RscShortcutButton
		{
			idc = 1000;
			text = "Start";
			tooltip = "Spawn as this plane";
			x = 0.40625;
			y = 0.9;
			w = 0.15625;
			action = "[2,_this] call compile preprocessfilelinenumbers \
					  'UI\planeDialog_controls.sqf'";
		}
		class Quit : RscShortcutButton
		{
			idc = 1001;
			text = "Quit";
			tooltip = "Exit the mission";
			x = 0.03125;
			y = 0.9;
			w = 0.15625;
			action = "[3,_this] call compile preprocessfilelinenumbers \
					  'UI\planeDialog_controls.sqf'";
			shortcuts[]={01};
		}
	}
}
