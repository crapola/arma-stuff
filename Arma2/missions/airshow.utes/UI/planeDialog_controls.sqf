scriptname "planeDialog_controls.sqf";
/*
	File:
	planeDialog_controls.sqf

	Description:
	Called by all dialog controls.

	Parameters:
	0:(Number) Index.
	1:(Control) Calling control.

	Returns: Nothing.
*/
disableserialization;
private ["_i","_c","_s","_class","_side"];
_i=_this select 0;
_c=_this select 1;
switch (_i) do
{
	case 0: // Combo item selected
	{
		lbclear 1500;
		_s=_c select 1;
		_names=(PlanesData select 1) select _s;
		if ( (count _names) >0) then
		{
			lbadd [1500,"<Random>"];
			{
				lbadd [1500,_x];
			} foreach _names;
			ctrlenable [1500,true];
			lbsetcursel [1500,0];
		}
		else
		{
			lbadd [1500,"None"];
			ctrlenable [1500,false];
			ctrlenable [1000,false];
		};
	};
	case 1: // List item selected
	{
		ctrlenable [1000,true];
	};
	case 2:	// Start
	{
		_side=lbcursel 2000;
		if (lbcursel 1500>0) then
		{
			_class=((PlanesData select 0) select _side)
				   select (lbcursel 1500)-1;
		} else
		{
			_class="<Random>";
		};
		[_side,_class] execVM "script\respawn.sqf";
		closedialog 0;
	};
	case 3:	// Quit
	{
		closedialog 0;
		execvm "script\end.sqf";
	};
};