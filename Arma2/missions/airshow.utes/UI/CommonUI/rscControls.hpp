#include "styles.hpp"

// Arma palette
#define COLOR_BLACK {0,0,0,1}
#define COLOR_WHITE {1,1,1,1}
#define COLOR_LIGHT_YELLOW {0.8784,0.8471,0.651,1.0}
#define COLOR_BACK_GREEN {"58/255","53/255","25/255","156/255"}
#define COLOR_BACK_BROWN {"49/255","36/255","25/255","173/255"}
//==============================================================================
class RscText
{
	type = CT_STATIC;
	style = ST_SINGLE;

	x = 0;
	y = 0;
	h = 0.037;
	w = 0.3;
	shadow = 2;
	font = "Zeppelin32";
	SizeEx = 0.03921;
	colorText[] = COLOR_LIGHT_YELLOW;
	colorBackground[] = {0,0,0,0};

	text="";
}
//==============================================================================
class RscButton : RscText
{
	type = CT_BUTTON;
	style = ST_CENTER;

	colorDisabled[] = {0.4,0.4,0.4,1};
	colorBackground[] = {1,0.537,0,0.5};
	colorBackgroundActive[] = {1,0.537,0,1};
	colorBackgroundDisabled[] = {0.95,0.95,0.95,0.1};
	offsetX = 0.003;
	offsetY = 0.003;
	offsetPressedX = 0.002;
	offsetPressedY = 0.002;
	colorFocused[] = {1,0.537,0,1};
	colorShadow[] = {0.023529,0,0.0313725,1};
	colorBorder[] = {0.023529,0,1.0313725,1};
	borderSize = 0.0;
	soundEnter[] = {"\ca\ui\data\sound\onover",0.09,1};
	soundPush[] = {"\ca\ui\data\sound\new1",0.0,0};
	soundClick[] = {"\ca\ui\data\sound\onclick",0.07,1};
	soundEscape[] = {"\ca\ui\data\sound\onescape",0.09,1};
	text="Button";
	tooltip="Tooltip";
}

class RscCombo : RscText
{
	type = CT_COMBO;

	shadow = 0;
	colorSelect[] = {0.023529,0,0.0313725,1};
	colorText[] = {0.023529,0,0.0313725,1};
	colorBackground[] = {0.95,0.95,0.95,1};
	colorSelectBackground[] = COLOR_LIGHT_YELLOW;
	colorScrollbar[] = {0.023529,0,0.0313725,1};
	arrowEmpty = "\ca\ui\data\ui_arrow_combo_ca.paa";
	arrowFull = "\ca\ui\data\ui_arrow_combo_active_ca.paa";
	wholeHeight = 0.45;
	color[] = {0,0,0,0.6};
	colorActive[] = {0,0,0,1};
	colorDisabled[] = {0,0,0,0.3};
	soundExpand[] = {"\ca\ui\data\sound\new1",0.0,0};
	soundSelect[] = {"\ca\ui\data\sound\onclick",0.07,1};
	soundCollapse[] = {"\ca\ui\data\sound\onescape",0.09,1};
	maxhistorydelay=1;

	class ScrollBar
	{
		color[] = {1,1,1,0.6};
		colorActive[] = {1,1,1,1};
		colorDisabled[] = {1,1,1,0.3};
		thumb = "\ca\ui\data\ui_scrollbar_thumb_ca.paa";
		arrowFull = "\ca\ui\data\ui_arrow_top_active_ca.paa";
		arrowEmpty = "\ca\ui\data\ui_arrow_top_ca.paa";
		border = "\ca\ui\data\ui_border_scroll_ca.paa";
	};
}

class RscListBox : RscText
{
	type = CT_LISTBOX;
	style = LB_TEXTURES;

	color[] = {1,1,1,1};
	colorScrollbar[] = {0.95,0.95,0.95,1};
	colorSelect[] = {0.95,0.95,0.95,1};
	colorSelect2[] = {0.95,0.95,0.95,1};
	colorSelectBackground[] = COLOR_BLACK;
	colorSelectBackground2[] = COLOR_LIGHT_YELLOW;
	period = 1.2;
	maxHistoryDelay = 1.0;
	autoScrollSpeed = -1;
	autoScrollDelay = 5;
	autoScrollRewind = 0;
	linespacing=1.0;
	soundSelect[] = {"\ca\ui\data\sound\onclick",0.07,1};
	rowHeight = 0;

	colorDisabled[] = {1,0,1,0.3};
	disabledCtrlColor[] = {1,0,1,0.3};// does nothing :/
	disabledKeyColor[] = {1,0,1,0.3};

	class ScrollBar
	{
		color[] = {1,1,1,0.6};
		colorActive[] = {1,1,1,1};
		colorDisabled[] = {1,1,1,0.3};
		thumb = "\ca\ui\data\ui_scrollbar_thumb_ca.paa";
		arrowFull = "\ca\ui\data\ui_arrow_top_active_ca.paa";
		arrowEmpty = "\ca\ui\data\ui_arrow_top_ca.paa";
		border = "\ca\ui\data\ui_border_scroll_ca.paa";
	};
}

class RscShortcutButton : RscText
{
	type=CT_SHORTCUTBUTTON;

	w = 0.183825;
	h = 0.1045752;
	text = "Button";

	animTextureNormal = "\ca\ui\data\ui_button_normal_ca.paa";
	animTextureDisabled = "\ca\ui\data\ui_button_disabled_ca.paa";
	animTextureOver = "\ca\ui\data\ui_button_over_ca.paa";
	animTextureFocused = "\ca\ui\data\ui_button_focus_ca.paa";
	animTexturePressed = "\ca\ui\data\ui_button_down_ca.paa";
	animTextureDefault = "\ca\ui\data\ui_button_default_ca.paa";
	color[] = COLOR_LIGHT_YELLOW;
	color2[] = {0.95,0.95,0.95,1};
	colorbackground[] = COLOR_WHITE;
	colorbackground2[] = {1,1,1,0.4};
	colorDisabled[] = {1,1,1,0.25};
	period = 0.4;
	periodFocus = 1.2;
	periodOver = 0.8;
	shortcuts[]={};
	size = 0.03921;
	soundEnter[] = {"\ca\ui\data\sound\onover",0.09,1};
	soundPush[] = {"\ca\ui\data\sound\new1",0.0,0};
	soundClick[] = {"\ca\ui\data\sound\onclick",0.07,1};
	soundEscape[] = {"\ca\ui\data\sound\onescape",0.09,1};
	textureNoShortcut = "";

	class HitZone
	{
		left = 0.004;
		top = 0.029;
		right = 0.004;
		bottom = 0.029;
	};
	class ShortcutPos
	{
		left = 0.0145;
		top = 0.026;
		w = 0.0392157;
		h = 0.0522876;
	};
	class TextPos
	{
		left = 0.05;
		top = 0.034;
		right = 0.005;
		bottom = 0.005;
	};
};