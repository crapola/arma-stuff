//==============================================================================
// Init
//==============================================================================
diag_log text "Air Show mission started.";
enableteamswitch false;
skiptime random 23;
waituntil {!isnil "bis_fnc_init"};
GameOver=false;
// Global plane names
PlanesData=call compile preprocessfilelinenumbers "script\listPlanes.sqf";
// GC init
FNC_RegisterGC=call compile preprocessfilelinenumbers "garbageman\init.sqf";
// Precompile
FNC_KillFeed=compile preprocessfilelinenumbers "script\killFeed.sqf";
// Start spawners.
execvm "script\spawnFoes.sqf";
execvm "script\groundForces.sqf";
// Intro
cuttext ["","black in",4];
_camera="camera" camcreate [3899.65,3424.42,2.68];
_introscript=_camera execvm "script\flyBy.sqf";
sleep 4;	
createdialog "SelectPlaneDialog";
waituntil {player!=StartingUnit}; // Selected something
terminate _introscript;
_camera cameraeffect ["terminate","back"];
camdestroy _camera;
// Commands
NextRearmTime=0;
execvm "script\myCommandMenu.sqf";
// Scoring
HighScore=0;
execvm "script\killFeedLoop.sqf";