scriptname "killFeedLoop.sqf";
/*
	File:
	killFeedLoop.sqf

	Description:
	Shows player what he killed.

	Parameters: None.
	Returns: Never.
*/
private ["_name","_t"];
KFQ=[];
while {true} do
{
	waituntil {count KFQ>0};
	_name=KFQ call BIS_fnc_arrayPop;
	hintsilent parsetext format ["<t color='#ffff00'>You neutralized a %1.</t><br/><t size='1.2'>Score: <t color='#00ff00'>%2</t></t>",_name,rating player];
	if (rating player>HighScore && side player!=civilian) then
	{
		HighScore=rating player;
		_t=player getvariable "Task";
		if (!isnil "_t") then
		{
			if (HighScore>=10000 && taskstate _t=="assigned") then
			{
				_t settaskstate "succeeded";
				taskhint ["TASK ACCOMPLISHED:\nScore 10,000 points",[0.6,0.839215,0.466666,1],"taskDone"];
				activatekey "SP_Air_Show";
			};
		};
	};
	sleep 1;
};