scriptname "flyBy.sqf";
/*
	File:
	flyBy.sqf

	Description:
	A short intro flyby.

	Parameter: (Object) Camera instance.
	Returns: Nothing.
*/
private ["_camera"];
_camera=_this;
_camera campreload 3;
_camera cameraeffect ["internal","back"];
_camera camsettarget [-67524.59,73411.15,686.71];
_camera camsetfov 0.700;
_camera campreparetarget [-87327.09,44621.14,-1102.36];
_camera campreparepos [3824.50,3510.20,10.27];
_camera campreparefov 0.700;
_camera camcommitprepared 5;
sleep 5;
_camera campreparetarget [-87021.22,45023.35,-4836.41];
_camera campreparepos [3824.50,3510.20,26.27];
_camera campreparefov 0.842;
_camera camcommitprepared 10;