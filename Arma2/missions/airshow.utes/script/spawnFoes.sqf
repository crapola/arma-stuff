scriptname "spawnFoes.sqf";
/*
	File:
	spawnFoes.sqf

	Description:
	Spawns AI planes for each side.

	Parameters: None.
	Returns: Never.
*/
private ["_around","_createplane","_unit_count","_pos"];
_around=compile preprocessfilelinenumbers "script\posAround.sqf";
_createplane=compile preprocessfilelinenumbers "script\createPlane.sqf";
// Create air groups for each side
AirGroups=[];
{
	[AirGroups,creategroup _x] call BIS_fnc_arrayPush;
} foreach [east,west,resistance,civilian];
{
	_x addwaypoint [getmarkerpos "airport",1];
	[_x,0] setwaypointposition [getmarkerpos "airport",100];
	[_x,1] setWaypointType "SAD";
} foreach AirGroups;
(AirGroups select 0) setgroupid ["RU Squadron"];
(AirGroups select 1) setgroupid ["US Squadron"];
(AirGroups select 2) setgroupid ["Chedak'Air"];
(AirGroups select 3) setgroupid ["Tourists"];
while {!GameOver} do
{
	{
		_unit_count=count units _x;
		if (_unit_count<4) then
		{
			_pos=[vehicle player,800] call _around;
			_r=[_pos,_x] call _createplane;
			if (0<count _r) then
			{
				(_r select 0) call FNC_KillFeed;
				{
					_x call FNC_KillFeed;
				} foreach (_r select 1);
			};
		};
	} foreach AirGroups;
	sleep 20;
};