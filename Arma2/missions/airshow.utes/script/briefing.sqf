player creatediaryrecord ["Diary",["Support","
Radio commands:<br/><br/>
5-1-1	Rearm instantly (2 minutes cooldown).<br/>
5-1-2	Advance time 5 hours.<br/>
5-1-3	Clear weather.<br/>
5-1-0	Respawn.<br/>
"]];
player creatediaryrecord ["Diary",["Mission","
Your mission if you accept it is to get a rating of 10,000 points without 
respawning. You cannot perform this task as a civilian.
"]];
player creatediaryrecord ["Diary",["Briefing","
Each faction, if they have aircraft, has an air squadron composed of mixed 
units. 
You can join one of those at any time with the vehicle of your choice.<br/>
Ground forces will spawn from bases indicated by flags and converge towards the 
airport.<br/>
Review Support for a few radio commands.
"]];

private ["_task"];

if (HighScore>=10000) exitwith
{
	_task=player createsimpletask ["ScoreTask"];
	_task setsimpletaskdescription ["You already completed this task.","Score 10,000 points",""];
	_task settaskstate "succeeded";
};

if (side player==civilian) exitwith
{
	_task=player createsimpletask ["ScoreTask"];
	_task setsimpletaskdescription ["This task is unavailable to civilian side.","Score 10,000 points",""];
	_task settaskstate "canceled";
};

_task=player createsimpletask ["ScoreTask"];
_task setsimpletaskdescription ["Get 10,000 points in one life.","Score 10,000 points",""];
player setcurrenttask _task;
player setvariable ["Task",_task];

