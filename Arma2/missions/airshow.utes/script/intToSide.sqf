scriptname "intToSide.sqf";
/*
	File:
	intToSide.sqf

	Description:
	Return a side for corresponding integer.

	Parameter:
	(Number) Number from 0 to 3.

	Returns:
	(Side) Side.
*/
switch (_this) do
{
	case 0:{east;};
	case 1:{west;};
	case 2:{resistance;};
	case 3:{civilian;};
	default{east;};
};