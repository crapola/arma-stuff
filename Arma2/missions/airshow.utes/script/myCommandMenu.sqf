private ["_menu","_rEnabled","_rText","_minutes","_seconds"];


while {true} do
{
waituntil {commandingmenu=="RscCallSupport"};

if (NextRearmTime>time) then
{
	_rEnabled="0";
	_minutes=floor ((NextRearmTime-time)/60);
	_seconds=floor ((NextRearmTime-time) mod 60);
	_rText="Rearm available in "+format ["%1:%2",_minutes,_seconds];
} else
{
	_rEnabled="1";
	_rText="Rearm now";
};

_menu=[
["User menu",false],
[_rText,[2],"",-5,[["expression","hint 'Rearmed!';vehicle player setvehicleammo 1;NextRearmTime=time+120;"]],"VehicleCommander",_rEnabled],
["Advance time",[3],"",-5,[["expression","
	[] spawn {cuttext ['Five hours later...','black out',2];sleep 5;skiptime 5;cuttext ['','black in',2];};
"]],"1","1"],
["Clear weather",[4],"",-5,[["expression","
	hint 'The skies clear up.';5 setfog 0;5 setovercast 0;
"]],"1","1"],
["separator",[0],"",-1,[["expression",""]],"1","1"],
["Respawn",[11],"",-5,[["expression","createdialog 'SelectPlaneDialog'"]],"1","1"]
];

showcommandingmenu "#USER:_menu";
waituntil {commandingmenu!="RscCallSupport"};
};