scriptname "respawn.sqf";
/*
	File:
	respawn.sqf

	Description:
	Creates a new plane for player and respawns him.

	Parameters:
	0:(Number) Side number [0-3].
	1:(String) Unit type.

	Returns: Nothing.
*/
private ["_sideint","_side","_class","_p","_veh"];
_sideint=_this select 0;
_side=_sideint call compile preprocessfilelinenumbers "script\intToSide.sqf";
_class=_this select 1;
//diag_log format ["Respawning... _side=%1 _class=%2",_side,_class];
if ( _class=="<Random>" ) then
{
	//diag_log "Respawn random";
	_class=(PlanesData select 0) select _sideint call BIS_fnc_selectRandom;
	//diag_log format ["Rolled a %1",_class];
};
_p=[getmarkerpos "spawn_east",
	getmarkerpos "spawn_west",
	getmarkerpos "spawn_resistance",
	getmarkerpos "spawn_civilian"] select _sideint;
_p set [2,1000];
_dir=[markerdir "spawn_east",
	  markerdir "spawn_west",
	  markerdir "spawn_resistance",
	  markerdir "spawn_civilian"] select _sideint;
_veh=[_p,_dir,_class,_side] call BIS_fnc_spawnVehicle;
if (!isnull (_veh select 0)) then
{
	(_veh select 0) call FNC_RegisterGC;
	cuttext ["","black in",1];
	selectplayer driver (_veh select 0);
	[player] join (AirGroups select _sideint);
	player execvm "script\briefing.sqf";
	NextRearmTime=0;
}
else
{
	hint "There was a problem";
	createdialog "SelectPlaneDialog";
};