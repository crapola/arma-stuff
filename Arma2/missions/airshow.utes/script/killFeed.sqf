scriptname "killFeed.sqf";
/*
	File:
	killFeed.sqf

	Description:
	Adds a "killed" EH to an object. Upon death its name is pushed to the
	killFeedLoop stack (KFQ).

	Parameter:
	(Object) Object.
*/
//diag_log text format ["Kill feed added for %1 %2",_this,typeof _this];
_this addeventhandler ["killed",
{
	private ["_name"];
	if ( player==(_this select 1) || player in (_this select 1) ) then
	{
		_name=gettext (configfile >> "cfgvehicles" >> typeof (_this select 0) >> "displayname");	
		[KFQ,_name] call BIS_fnc_arrayPush;
	};
}];