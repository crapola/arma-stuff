scriptname "deathCam.sqf";
/*
	File:
	deathCam.sqf

	Description:
	Camera stuff for player death adapted from BIS's onplayerkilled.

	Parameters:
	0:(Object) Killed player.
	1:(Object) Killer.
	2:(Object) Camera.
	Returns: Never.
*/
private ["_player","_killer","_camera",
		"_pos","_flying","_size","_dir","_dx","_dy","_dz",
		"_ksize","_kdir","_kdx","_kdy","_kdz",
		"_delay","_distancecoef"];
_player=_this select 0;
_killer=_this select 1;
_camera=_this select 2;
if (isnull _killer) then {_killer=_player};
//--- 1.60 - Visual states interpolation position was not correct
#define FNC_PLAYERPOS(WHO) \
	(if (isclass (configfile >> "CfgPatches" >> "ca_E")) then { \
		call compile ("visibleposition vehicle " + WHO) \
	} else { \
		call compile ("position vehicle " + WHO) \
	})
//--- Random constants for player
_pos = FNC_PLAYERPOS("_player");
_flying = if ((_pos select 2) > 50) then {true} else {false};
_size = sizeof typeof vehicle _player;
_dir = random 360;
_dx = sin _dir * _size;
_dy = cos _dir * _size;
_dz = if (_flying) then {_size * 2} else {_size};
//--- Random constants for killer
_ksize = sizeof typeof vehicle _killer;
_kdir = random 360;
_kdx = sin _dir * _size;
_kdy = cos _dir * _size;
_kdz = _size;
// PP stuff
[] spawn
{
	// Light blur
	"dynamicblur" ppeffectenable true;
	"dynamicblur" ppeffectadjust [2];
	"dynamicblur" ppeffectcommit 0.2;
	
	"colorcorrections" ppeffectadjust [1,1,0,[0,0,0,0],[1,0,0,0],[0,0,1,1]];
	"colorcorrections" ppeffectcommit 0.5;
	"colorcorrections" ppeffectenable true;
	sleep 1;
	// Full red
	"colorcorrections" ppeffectadjust [1, 1, 0, [0.1, 0.0, 0.0, 1], [1.0, 0.5, 0.5, 0.1], [0.199, 0.587, 0.114, 0.0]];
	"colorcorrections" ppeffectcommit 0.5;
	sleep 1;
	// Normal
	"dynamicblur" ppeffectadjust [0];
	"dynamicblur" ppeffectcommit 5;
	"colorcorrections" ppeffectadjust [1, 1, 0, [0.0, 0.0, 0.0, 0.0], [1.0, 1.0, 1.0, 1.0], [0.199, 0.587, 0.114, 0.0]];
	"colorcorrections" ppeffectcommit 5;
	sleep 5;
	"dynamicblur" ppeffectenable false;
	"colorcorrections" ppeffectenable false;
};
// Wait a bit and camera.
sleep 1.5;
_camera cameraeffect ["internal","back"];
showcinemaborder false;
_camera campreparetarget _pos;
_camera campreparepos [(_pos select 0)+_dx,(_pos select 1)+_dy,(_pos select 2)+_dz];
_camera campreparefov 0.7;
_camera camcommitprepared 0;
//--- Track player
while {velocity vehicle _player distance [0,0,0] > 0 || _flying} do
{
	_pos = FNC_PLAYERPOS("_player");
	if (!_flying || (_flying && (FNC_PLAYERPOS("_player") select 2) > 50)) then
	{
		_camera campreparepos [(_pos select 0) + _dx,(_pos select 1) + _dy,(_pos select 2) + _dz];
	};
	if ((_pos select 2) < 20) then
	{
		_flying=false;
	};
	_camera campreparetarget _pos;
	_camera camcommitprepared 0;
	sleep 0.01;
};
//--- Zoom out from player
_delay = 4 + random 3;
_distancecoef = 1.2;
if (_player == _killer) then
{
	_delay = 100 + random 100;
	_distancecoef = 5 + random 3;
};
_pos = FNC_PLAYERPOS("_player");
_camera campreparepos [(_pos select 0) + _dx * _distancecoef,(_pos select 1) + _dy * _distancecoef,(_pos select 2) + _dz * _distancecoef];
_camera camcommitprepared _delay;
waituntil {camcommitted _camera};
if (_player == _killer) exitwith {};
//--- Show killer
_pos = FNC_PLAYERPOS("_killer");
_camera campreparetarget vehicle _killer;
_camera campreparepos [(_pos select 0) + _kdx,(_pos select 1) + _kdy,(_pos select 2) + _kdz];
_camera camcommitprepared (5 + random 5);
waituntil {camcommitted _camera};
while {true} do {
	_pos = FNC_PLAYERPOS("_killer");
	_kdx = sin _dir * _size;
	_kdy = cos _dir * _size;
	_kdz = _size/2 + (_size/2) * sin (_dir/1.5);
	_dir = _dir + 0.25;
	_camera campreparepos [(_pos select 0) + _kdx,(_pos select 1) + _kdy,(_pos select 2) + _kdz];
	_camera campreparetarget _pos;
	_camera camcommitprepared 0;
	sleep 0.01;
};