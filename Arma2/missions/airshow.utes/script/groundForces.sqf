scriptname "groundForces.sqf";
/*
	File:
	groundForces.sqf

	Description:
	Takes care of AI ground groups.

	Parameters: None.
	Returns: Never.
*/
private ["_str2conf","_groupClasses","_spawn_pos","_wp","_i",
		 "_unit_count","_cfg","_grp","_aatypes","_a","_diceroll"];
_str2conf=compile preprocessfilelinenumbers "\ca\modules\ambient_combat\data\scripts\functions\convertGroupStringToConfig.sqf";
// Allowed classes.
_groupClasses=[
["RU_MotInfSquad","RU_MotInfSection_Recon","RU_MotInfSection_Patrol","RU_MechInfSquad_1","RU_MechInfSquad_2","RU_TankPlatoon"],
["USMC_MotInfSection","USMC_MotInfSection_AT","USMC_MechInfSquad","USMC_MechReconSection","USMC_TankPlatoon"],
["GUE_InfTeam_AT","GUE_MotInfSection","GUE_MotInfSquad","GUE_MechInfSection","GUE_MechInfSquad","GUE_TankSection"]
];
_spawn_pos=[getmarkerpos "Marker_Base_RU",
			getmarkerpos "Marker_Base_US",
			getmarkerpos "Marker_Base_GUE"];
// Create groups for each side
GroundGroups=[];
{
	[GroundGroups,creategroup _x] call BIS_fnc_arrayPush;
} foreach [east,west,resistance];
(GroundGroups select 0) setgroupid ["Russians"];
(GroundGroups select 1) setgroupid ["Americans"];
(GroundGroups select 2) setgroupid ["Chedakis"];
{
	[_x,0] setwaypointposition [getmarkerpos "airport",100];
	_wp=_x addwaypoint [getmarkerpos "airport",1];
	[_x,1] setWaypointType "HOLD";
	_wp setwaypointstatements ["false",""];
} foreach GroundGroups;

// Each will get one AA unit.
AA_Units=[objnull,objnull,objnull];
_aatypes=["2S6M_Tunguska","HMMWV_Avenger","Ural_ZU23_Gue"];

while {!GameOver} do
{
	// Groups
	_i=0;
	{
		_unit_count=count units _x;
		//_i=_foreachindex;		_foreachindex doesn't work in Arma2...
		if (_unit_count<5) then
		{
			_cfg=((_groupClasses select _i) call BIS_fnc_selectRandom) call _str2conf;
			_grp=[_spawn_pos select _i,
				  side _x,
				  _cfg] call BIS_fnc_spawnGroup;
			_grp call FNC_RegisterGC; // New units to GC

			// Add to kill feed
			private ["_vlist"];
			_vlist=[];
			{
				_x call FNC_KillFeed;
				if ( vehicle _x!=_x && !(vehicle _x in _vlist)) then
				{
					_vlist=_vlist+[vehicle _x];
					(vehicle _x) call FNC_KillFeed;
				};
			} foreach units _grp;

			// Join our group and delete temp group.
			units _grp join _x;
			deletegroup _grp;
			if (!isnull _grp) then
			{
				diag_log text format ["Warning: group not deleted: %1",_grp];
			};
		};
		_i=_i+1;
	} foreach GroundGroups;

	// Anti-Air
	_i=0;
	{
		_diceroll=random 3;
		if (!alive _x && _diceroll<1) then
		{
			_a=[_spawn_pos select _i,0,_aatypes select _i,
				GroundGroups select _i] call BIS_fnc_spawnVehicle;
			AA_Units set [_i,_a select 0];
			(AA_Units select _i) call FNC_RegisterGC;

			{
				_x call FNC_KillFeed;
			} foreach crew (AA_Units select _i);
			(AA_Units select _i) call FNC_KillFeed;
		};
		_i=_i+1;
	} foreach AA_Units;

	// Wait
	sleep 30;
};