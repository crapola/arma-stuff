scriptName "posAround.sqf";
/*
	File:
	posAround.sqf

	Description:
	Returns a position around an object on a circle, at a random angle, on the
	same level.

	Parameters:
	0:(Object) Center object
	1:(Number) Radius

	Returns:
	(Position3D) Position
*/
private ["_pos","_u","_r","_a"];
_u=_this select 0;
_r=_this select 1;
_pos=getposATL _u;
_a=random 360;
_pos=[(_pos select 0)+_r*cos _a,(_pos select 1)+_r*sin _a,(_pos select 2)];
_pos;