scriptName "listPlanes.sqf";
/*
	File:
	listPlanes.sqf

	Description:
	Builds a list of all the planes in the game.

	Parameters: None.

	Returns:
	0: (Array (Array (String))) The list as an array of 4 arrays of class
	names, one per side, in order: East, West, Resistance, Civilian.
	1: Same as 0, but display names instead of class names.
*/
private ["_cfg","_planes","_cur","_cn","_cur_side","_temp_arr"];
_cfg=configfile >> "cfgVehicles";
_planes=[[],[],[],[]];
_planeDisplayNames=[[],[],[],[]];
for "_i" from 0 to (count _cfg)-1 do
{
	_cur=_cfg select _i;
	if (isclass _cur) then
	{
		_cn=configname(_cur);
		if (_cn iskindof "Plane" && !(_cn iskindof "UAV")) then
		{
			if (getnumber (_cfg >> _cn >> "scope")==2) then // Public
			{
				if (getnumber (_cfg >> _cn >> "fuelcapacity")>0) then // No parachutes
				{
					/*
					_threat={_x>0.0} count getarray (_cfg >> _cn >> "threat");
					_weap=count getarray (_cfg >> _cn >> "weapons");
					if ( _threat>0 && _weap>1) then // Threat +more than 1 weapon (C130 has a flare) problem:su34
					{
						_cur_side=getnumber (_cfg >> _cn >> "side");
						diag_log format ["%1 , %2 , %3",_cur,_cn,_cur_side];
						_temp_arr=(_planes select _cur_side) + [_cn];
						_planes set [_cur_side,_temp_arr];
					};
					*/
					_cur_side=getnumber (_cfg >> _cn >> "side");
					//diag_log format ["%1 , %2 , %3",_cur,_cn,_cur_side];
					_temp_arr=(_planes select _cur_side) + [_cn];
					_planes set [_cur_side,_temp_arr];

					_dname=gettext (_cfg >> _cn >> "displayname");
					_temp_arr=(_planeDisplayNames select _cur_side) + [_dname];
					_planeDisplayNames set [_cur_side,_temp_arr];
					//diag_log _dname;

				};
			};
		};
	};
};

[_planes,_planeDisplayNames];