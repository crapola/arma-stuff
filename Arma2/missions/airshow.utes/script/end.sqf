// End mission.
player enablesimulation false;
"dynamicblur" ppeffectadjust [3];
"dynamicblur" ppeffectcommit 2.0;
"dynamicblur" ppeffectenable true;
"colorcorrections" ppeffectadjust [1,1,0,[0,0,0,0],[1,0.9,0.8,0],[1,1,1,1]];
"colorcorrections" ppeffectcommit 2.0;
"colorcorrections" ppeffectenable true;
sleep 2;

// Problem: When dead addrating doesn't work.
if (!isnil "HighScore") then
{
	player addrating (-rating player);
	player addrating HighScore;
};
endmission "end1";