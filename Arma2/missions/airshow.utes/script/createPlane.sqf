scriptname "createPlane.sqf";
/*
	File:
	createPlane.sqf

	Description:
	Creates a random plane.

	Parameters:
	0:(Position) Position.
	1:(Group) Group.

	Returns:
	0:(Object) Vehicle.
	1:(Array) Crew.
	2:(Group) Group.
*/
private ["_pos","_side","_group","_type","_obj"];
_pos=_this select 0;
_group=_this select 1;
_side=switch(side _group) do
{
	case east:{0};
	case west:{1};
	case resistance:{2};
	case civilian:{3};
};
if (count ((PlanesData select 0) select _side)==0) exitwith
{
	[];
};
_type=((PlanesData select 0) select _side) call BIS_fnc_selectRandom;
_pos set [2,600];
_obj=[_pos, 360, _type,_group] call BIS_fnc_spawnVehicle;
(_obj select 0) call FNC_RegisterGC;
_obj;