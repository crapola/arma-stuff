scriptname "onPlayerKilled.sqf";
/*
	File:
	onPlayerKilled.sqf

	Description:
	Runs camera and show dialog, waits until something selected.

	Parameters:
	0:(Object) Killed player.
	1:(Object) Killer.

	Returns: Nothing.
*/
private ["_camera","_dc_script"];
closedialog 0;
_camera="camera" camcreate getposatl (_this select 0);
_dc_script=[_this select 0,_this select 1,_camera] execvm "script\deathCam.sqf";
sleep 5;
createdialog "SelectPlaneDialog";
waituntil {player!=_this select 0};
terminate _dc_script;
_camera cameraeffect ["terminate","back"];
camdestroy _camera;