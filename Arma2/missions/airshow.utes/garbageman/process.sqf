scriptname "garbageman\process.sqf";
/*	============================================================================
	process
	============================================================================

	Description:
	Process the garbage queue for the GarbageMan FSM.
*/
#include "debugstuff.hpp"
DB("GMan processing...")
private ["_queue","_i","_look","_acceptable_dist"];
_queue = GarbageMan getvariable "Queue";
for "_i" from 0 to ((count _queue) - 1) do
{
	private ["_entry", "_time"];
	_entry = _queue select _i;
	_time = _entry select 1;
	//Check the expiry date.
	if (_time <= time) then
	{
		private ["_object"];
		_object = _entry select 0;
		switch (typename _object) do
		{
			case (typename objnull):
			{
				_look=[position player,getdir player,100,position _object]
					  call BIS_fnc_inAngleSector;
				_acceptable_dist=if (_look) then {1000} else {200};
				if ( (player distance _object) >= _acceptable_dist ) then
				{
					deletevehicle _object;
					_queue set [_i, -1];
					DBF("GMan deleted an object: %1",_object,nil)
				};
			};
			case (typename grpnull):
			{
				//Make sure the group is empty.
				if (({alive _x} count (units _object)) == 0) then
				{
					deletegroup _object;
					_queue set [_i, -1];
					DBF("GMan deleted a group: %1",_object,nil)
				};
			};
			default {};
		};
	};
};
_queue = _queue - [-1];
DBF("GMan queue (%2 items): %1",_queue,count _queue)
GarbageMan setvariable ["Queue", _queue];
true;