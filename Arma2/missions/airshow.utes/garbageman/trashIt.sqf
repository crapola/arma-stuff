scriptname "garbageman\trashIt.sqf";
/*	============================================================================
	trashIt
	============================================================================

	Description:
	Sends an object to the garbage collection queue.

	Parameters:
	0:(Object) The object.

	Returns:
	Success flag (Boolean).
*/
//Valid parameter count
if (isnil "_this") exitwith {diag_log "Log: [trashIt] There should be 1 mandatory parameter!"; false};
private ["_object", "_queue", "_timeToDie"];
_object=(_this select 0);
_queue=GarbageMan getvariable "Queue";
switch (typename _object) do
{
	case (typename objnull):
	{
		if (alive _object) then
		{
			_timeToDie=time+60;
		}
		else
		{
			_timeToDie=time+60*5;
		};
	};
	case (typename grpnull):
	{
		_timeToDie=time+60;
	};
	default
	{
		_timeToDie=time;
	};
};
_queue = _queue + [[_object, _timeToDie]];
GarbageMan setvariable ["Queue",_queue];
true;