// Taken from Warfare. Destroys abandoned vehicle after 5 min.
// Parameter: _this = the vehicle.
private ["_eh"];
//diag_log text format ["Adding abandoned vehicle handler for %1",_this];
_eh=
{
_this spawn
	{
		private["_destroy","_timeleft","_vehicle"];
		_vehicle = _this select 0;
		_destroy = true;
		if (count crew _vehicle>0) exitwith {};
		// Unlock abandoned AI vehicles...
		_vehicle lock false;
		// If vehicle stays empty for 5 minutes then remove it.
		_timeleft = time + 300;
		while {time < _timeleft} do
		{
			sleep 30;
			if (count crew _vehicle>0) exitwith {_destroy = false};
		};
		if (_destroy && !isnull _vehicle) then
		{
			//diag_log text format ["Killing abandoned %1",_vehicle];
			_vehicle setammocargo 0;
			_vehicle setfuelcargo 0;
			_vehicle setdamage 1;
		};
	};
};
_this addeventhandler ["getout",_eh];