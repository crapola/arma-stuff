//#define DEBUG

#ifdef DEBUG
	#define DB(X) diag_log text X;
	#define DBF(A,B,C) diag_log text format [A,B,C];
#else
	#define DB(X)
	#define DBF(A,B,C)
#endif