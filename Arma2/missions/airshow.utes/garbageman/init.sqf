scriptname "garbageman\init.sqf";
/*	============================================================================
	init
	============================================================================

	Description:
	Creates GC logic for GarbageMan (based on BIS GC).
	GarbageMan variables:
		Queue
		FNC_Trash
		FNC_Register
		FNC_UnitEH
		FNC_EmptyVehicleEH

	Parameters:
	None.

	Returns:
	Handle to registering function.
*/
if (isserver) then
{
	if (isnil "GarbageMan") then
	{
		private ["_grp","_gman_fsm","_trash_func","_ueh","_register_func"];
		_grp=creategroup sidelogic;
		if (isnull _grp) then
		{
			createcenter sidelogic;
			_grp=creategroup sidelogic;
		};
		GarbageMan = _grp createunit ["logic",[0,0,0],[],0,""];
		_gman_fsm=GarbageMan execfsm "garbageman\gman.fsm";
		waituntil
		{
			_trash_func=GarbageMan getvariable "FNC_Trash";
			!isnil "_trash_func";
		};
		
		// Compile handler for object death.
		_ueh=
		{
			_this call (GarbageMan getvariable "FNC_Trash");
		};
		GarbageMan setvariable ["FNC_UnitEH",_ueh];
		
		// Compile handler for abandoned vehicles.
		_emptyvh=compile preprocessfilelinenumbers
					   "garbageman\addEmptyVehicleHandler.sqf";
		GarbageMan setvariable ["FNC_EmptyVehicleEH",_emptyvh];
		
		// Compile registration function.
		_register_func=compile preprocessfilelinenumbers
					   "garbageman\register.sqf";
		GarbageMan setvariable ["FNC_Register",_register_func];
		_register_func;
	} else
	{
		diag_log text "Warning: GarbageMan already exists.";
		GarbageMan getvariable "FNC_Register";
	}
};