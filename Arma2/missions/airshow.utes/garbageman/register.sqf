scriptname "garbageman\register.sqf";
/*	============================================================================
	register
	============================================================================

	Description:
	Register units to GarbageMan. Passed units will get a "killed" EH where they
	are added to the queue. In addition vehicles get a "getout" EH for abandon.
	
	Parameter:
	(Object) Must be a group, an unit, a vehicle with or without crew.
		The group itself won't be deleted.

	Returns:
	(Boolean) Success flag.
*/
#include "debugstuff.hpp"
DBF("Registering %1 to GC. ========",_this,nil)
private ["_event_handler","_vhandler","_vlist"];
_event_handler=GarbageMan getvariable "FNC_UnitEH";
_vhandler=GarbageMan getvariable "FNC_EmptyVehicleEH";
switch (typename _this) do
{
	case (typename grpnull):
	{
		DB("is group")
		_vlist=[];
		{
			_x addeventhandler ["killed",_event_handler];
			DBF("Added event for %1 %2",_x,typeof _x)
			if ((_x!=vehicle _x) && !(vehicle _x in _vlist)) then
			{
				_vlist=_vlist+[vehicle _x];				
			};
		} foreach units _this;
		{
			_x addeventhandler ["killed",_event_handler];
			DBF("Added event for vehicle %1 of type %2",_x,typeof _x)
		} foreach _vlist;
		true;
	};
	case (typename objnull):
	{
		DB("is object")
		_this addeventhandler ["killed",_event_handler];
		DBF("Added event for %1 %2",_this,typeof _this)
		if (count crew _this>0) then
		{
			DBF("%1 is crewed vehicle.",_this,nil)
			_this call _vhandler;
			{
				_x addeventhandler ["killed",_event_handler];
				DBF("Added event for crew %1 %2",_x,typeof _x)
			} foreach crew _this;
		};
		true;
	};
	default
	{
		diag_log text format ["Warning: GC ignored %1",_this];
		false;
	};
};
DB("========")