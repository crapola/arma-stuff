/*
	kf_fnc_register

	Description:
	Register objects to kill feed.
	Adds a "MPkilled" EH to the object. Upon death its name is pushed to the
	client killFeedLoop queue (kfq).

	Parameter:
	OBJECT|ARRAY - Object(s) getting the EH.
*/

if (typename _this != typename []) then {_this = [_this];};
{
	if (!(typename _x isequalto typename objnull)) exitwith
	{
		["Invalid parameter: %1",_x] call bis_fnc_error;
	};
	diag_log text format ["Kill feed added for %1 %2",_x,typeof _x];
	_x addmpeventhandler ["mpkilled",{_this call kf_fnc_onkill}];
} foreach _this;