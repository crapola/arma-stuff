/*
	Shows player what he killed.

	Parameters: None.
	Returns: Never.
*/
private ["_name","_t"];
kfq=[];
highscore=0;
while {true} do
{
	waituntil {sleep 0.5;count kfq>0};
	_name=kfq call bis_fnc_arraypop;
	if (count kfq>1) then {_name=_name+".."};
	hint/*silent*/ parsetext format ["<t color='#ffff00'>You neutralized a %1.</t><br/><t size='1.2'>Score: <t color='#00ff00'>%2</t></t>",_name,rating player];
	/*if (rating player>highscore && side player!=civilian) then
	{
		highscore=rating player;
		_t=player getvariable "task";
		if (!isnil "_t") then
		{
			if (highscore>=10000 && taskstate _t=="assigned") then
			{
				_t settaskstate "succeeded";
				taskhint ["task accomplished:\nscore 10,000 points",[0.6,0.839215,0.466666,1],"taskdone"];
				activatekey "sp_air_show";
			};
		};
	};*/
	sleep 1;
};