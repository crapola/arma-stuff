/*
	kf_fnc_onkill
	Update local queue when unit killed.
	Parameters forwarded from MPkilled.
*/
diag_log format ["Received MPkilled %1",_this];
if (isdedicated) exitwith{diag_log "nop";};
if (player isequalto (_this select 1)) then
{
	_name=gettext (configfile >> "cfgvehicles" >> typeof (_this select 0) >> "displayname");	
	kfq pushback _name;
};