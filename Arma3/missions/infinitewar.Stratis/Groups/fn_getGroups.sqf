/*
	Get groups configs according to side/faction/category indices.

	Parameters:
	0:VALUE(optional) - Side
		West=0
		East=1
		Indep=2
		Empty=3
	1:VALUE(optional) - Faction
	2:VALUE(optional) - Category

	If a parameter is ommited it is chosen at random.

	Returns:
	ARRAY - Array of configs.

	Examples:
	[0,1,2] call grp_fnc_getgroups;
	will give groups under West(0)>>Guerrilla(1)>>Motorized_MTP(2)
	[1] call grp_fnc_getgroups;
	will give East groups for a random faction and category.

	Side effects:
	This function creates a global cached_group_tree where all game groups are
	stored.
*/
if (isnil "cached_group_tree") then
{
	diag_log "Building group tree";
	_sides="true" configclasses (configfile>>"cfgGroups");
	cached_group_tree=[];
	{
		_a=[];
		_b=[];
		_factions=("true" configclasses _x);
		{
			_a=[];
			_types=("true" configclasses _x);
			{
				_a pushback ("true" configclasses _x);
				// todo analyze...where are tanks..etc
			} foreach _types;
			_b pushback _a;
		} foreach _factions;
		cached_group_tree pushback _b;
	} foreach _sides;

	{
		diag_log format ["side %1",_foreachindex];
		{
			diag_log format ["faction %1",_foreachindex];
			{
				diag_log format ["type %1",_foreachindex];
				{
					diag_log _x;
				} foreach _x;
			} foreach _x;
		} foreach _x;
	} foreach cached_group_tree;
};
// Params
if (isnil "_this") then {_this=[]};
_s=[_this,0,floor(random(count cached_group_tree)),[0]] call bis_fnc_param;
_f=[_this,1,floor(random(count (cached_group_tree select _s))),[0]] call bis_fnc_param;
_c=[_this,2,floor(random(count ((cached_group_tree select _s) select _f))),[0]] call bis_fnc_param;

(((cached_group_tree select _s) select _f) select _c);