/*
	Description:
	Get a subset of configs matching class names.

	Parameters:
	0:CONFIG[] - Array of configs to search.
	1:STRING[] - Array of class names to match. Case sensitive.

	Returns:
	CONFIG[] - Result. Empty array if no match.
*/
private ["_data","_names","_num_params","_returned"];
// Validate parameters
_data=[_this,0,[],[[]]] call bis_fnc_param;
_names=[_this,1,[],[[]]] call bis_fnc_param;
_num_params=count _names;
if (0==count _data || 0==_num_params) exitwith
{
	diag_log "empty params";
	[];
};
{
	if (!("CONFIG" isequalto typename _x)) exitwith
	{
		["Invalid type: %1, config expected.",_x] call bis_fnc_error;
	};
} foreach _data;
{
	if (!("STRING" isequalto typename _x)) exitwith
	{
		["Invalid type: %1, string expected.",_x] call bis_fnc_error;
	};
} foreach _names;
// Search

/*
{
//_test=configproperties [_x,"(gettext (_x>>'faction')=='OPF_F');",true];
_test="(gettext (_x>>'faction')=='OPF_F');" configclasses _x;
diag_log "test";
diag_log _test;
} foreach ([(configfile>>"cfgGroups"),2,false] call bis_fnc_returnchildren);
*/

// use configproperty when available

_returned=[];
{
	

	_str=[_x,[]] call bis_fnc_configpath;
	_found={(_str find _x)>0} count _names;
	if (_found==_num_params) then
	{
		_returned pushback _x;
		diag_log format ["Found: %1",_x];
	};
} foreach _data;
diag_log _returned;
_returned;