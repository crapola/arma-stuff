/*
	Retrieve a list of groups according to constraints.

	Parameters:
	0:SIDE(optional) - Can be blufor,opfor,independent.
		Use nil/nothing to accept any side.
	1:VALUE[2](optional) - Range [min,max] of unit count.
	2:STRING(optional) - Faction or nothing for any faction.
		BLU_F,IND_G_F....
	3:STRING(optional) - Vehicle type that must be present.
		Valid values are "Car","Tank","Air"... (parent class names)
		This is case sensitive!
		Use "None" to get groups without vehicles.
		Use nil/nothing to get everything.

	Returns:
	CONFIG[] - Array of group config entries.
*/
private ["_p_s","_p_c","_p_f","_p_v","_no_vehicle","_allgroups","_result"];
_p_s=[_this,0,sideunknown,[blufor]] call bis_fnc_param;
switch (_p_s) do
{
	case opfor:{_p_s=0;};
	case blufor:{_p_s=1;};
	case independent:{_p_s=2;};
	default{_p_s=-1;};
};
_p_c=[_this,1,[0,100],[[]],2] call bis_fnc_param;
_p_f=[_this,2,"",[""]] call bis_fnc_param;
_p_v=[_this,3,"All",[""]] call bis_fnc_param;
_no_vehicle=(_p_v=="None"); // Not case sensitive
_allgroups=[(configfile>>"cfgGroups"),3,false] call bis_fnc_returnchildren;
_result=[];
{
	if (true) then { // Ugly trick to allow continue/skip by exitwith
	// Side
	_side=getnumber(_x>>"side");
	if (_p_s!=-1 && _p_s!=_side) exitwith{};
	// Faction
	_faction=gettext(_x>>"faction");
	if ( !(_p_f isequalto "") && _p_f!=_faction) exitwith{};
	// Unit count
	_u="true" configclasses _x;
	_count=count _u;
	if (_count<_p_c select 0 || _count>_p_c select 1) exitwith{};
	// Check vehicles in group
	_hasv=false;
	_men=0;
	{
		_vehicle_name=gettext(_x>>"vehicle");
		_cfgv=(configfile>>"cfgvehicles">>_vehicle_name);
		_b=[_cfgv,true] call bis_fnc_returnparents;//string[] class names
		if ("Man" in _b) then
		{
			_men=_men+1;
		};
		_hasv=(_p_v in _b);
		if (_hasv) exitwith {};
	} foreach _u;
	if (_hasv || ((_men==_count) && _no_vehicle)) then
	{
		_result pushback _x;
	};

	}; //true
} foreach _allgroups;
diag_log _result;
_result;