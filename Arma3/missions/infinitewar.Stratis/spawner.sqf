/*
	_this=SpawnInfo[]
	Where SpawnInfo is [side,pos2d,groupconfigs,current group]
*/

while {true} do
{
	{
		_grp=_x select 3;
		if (isnull _grp || 0=={alive _x} count units _grp) then
		{
			_c=(_x select 2) call bis_fnc_selectrandom;
			diag_log format["Spawning: %1",_c];
			_grp=[
			_x select 1,
			_x select 0,
			_c,nil,nil,[0,0.75],[0.5,0.1]] call bis_fnc_spawngroup;
			_grp setgroupid ["Enemies"];
			units _grp call kf_fnc_register;
			_x set [3,_grp];
			_grp allowfleeing 0;
		};
		sleep 10;
	} foreach _this;
	sleep 10;
/* gc should handle that
{
	deletevehicle _x;
} foreach units _grp;
sleep 1;
deletegroup _grp;

sleep 1;*/
};