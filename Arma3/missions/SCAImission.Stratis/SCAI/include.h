class CfgFunctions
{
	class SCAI
	{
		class category
		{
			file="SCAI";
			class deleteGroupWaypoints{};
			class isVehicleArmed{};
			class onRespawn{};
			class sectorCaptured{};
			class waypointsCompleted{};
			class groupInfo{};
		};
	};
};