// [module,ownderside,previousownerside]
diag_log "sectorCaptured by "+str(_this select 1);
_module=_this select 0;
_ownerside=_this select 1;

_sectorpos=position ((_module getvariable "areas") select 0);

_enemysides=(_module getvariable "sides")-[_ownerside];
diag_log _enemysides;

// For each opposing side, send best suited group

{
	// Find best group according to distance and busyness

	_side=_x;
	_groups=[];
	{
		if (side _x==_side && !isplayer (leader _x)) then
		{
			_cost=(leader _x) distance _sectorpos;
			if !(_x call scai_fnc_waypointscompleted) then
			{
				_cost=_cost+1000;
			};
			_groups pushback [_cost,_x];
		};
	} foreach allgroups;

	_groups sort true;
	diag_log _groups;

	_pick=(_groups select 0) select 1;

	_pick call scai_fnc_deletegroupwaypoints;
	_wp=_pick addWaypoint [_sectorpos,0];
	_wp setwaypointcompletionradius 10;
	leader _pick globalchat format ["%1 moves to %2",_pick,_sectorpos];
	diag_log format ["%1 moves to %2",_pick,_sectorpos];

} foreach _enemysides;