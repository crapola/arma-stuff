/*
We can't use tasks because bot groups don't get them online.


*/

_ais=allUnits-allPlayers;
{
	_x addeventhandler ["respawn",{_this call scai_fnc_onrespawn}];
	_x setCaptive true;
} foreach _ais;


/*
{
	switch(side _x) do
	{
		case east:
		{
			_x addvehicle the_marid;
		};
		case west:
		{
			_x addvehicle the_marshall;
			_x addvehicle the_hunter;
		};
	};
} foreach allgroups;*/

// Get list of sectors.
// a3\modules_f\multiplayer\functions\fn_moduleSector.sqf
_sectors=[true] call bis_fnc_modulesector;

_getClosestEnemySectorPos=
{
	private ["_sp","_d","_side","_dist","_position"];
	_side=side _this;
	_dist=90000;
	_position=[];
	{
		if ( (_x getvariable "owner")!=_side) then
		{
			_sp=position ((_x getvariable "areas") select 0);
			_d=_this distance2D _sp;
			if (_d<_dist) then {_position=_sp;_dist=_d;};
		};
	} foreach _sectors;
	_position;
};

_findGoodVehicle=
{
	private ["_p","_cars","_car"];
	/*if (_this!=vehicle _this) exitwith
	{
		vehicle _this;
	};*/
	_p=position _this;
	_cars=nearestObjects [_p,["tank","car"],150];
	if (_cars isequalto []) exitwith {objnull;};
	_car=objnull;
	{
		if ((canmove _x) &&
			(isnull assigneddriver _x) &&
			(isnull assignedcommander _x) &&
			(isnull assignedgunner _x)			) exitwith
		{
			_car=_x;
		};
	} foreach _cars;
	_car;
};

_assignAItoVehicle=
{
	private ["_u","_v","_positions","_free"];
	_u=_this select 0;
	_v=_this select 1;
	if (isplayer _u) exitwith{};
	_positions=["commander","driver","gunner","cargo"];
	{
		_free=_v emptypositions _x;
		if (_free>0) exitwith
		{
			diag_log _x;
			diag_log _free;
			call compile ("_u assignas"+_x+" _v;");
		};
	} foreach _positions;
};