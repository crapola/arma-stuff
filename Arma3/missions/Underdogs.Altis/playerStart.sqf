// Setup starting position
// =======================
// Get roads within first center
_cpos=getposatl (ud_centers select 0);
_roads=_cpos nearroads 500;
// Sort by distance
_roads=[_roads,[_cpos],
{ [getposatl _x,_input0] call bis_fnc_distance2d; },"DESCEND"
] call bis_fnc_sortby;
// Pick the farthest one
_pick=getposatl (_roads select 0);
// Move
{
	_x setvehicleposition [_pick,[],10,"NONE"];
} foreach units group player;
_dir=[_pick,_cpos] call bis_fnc_dirto;
p_car setdir _dir;
p_truck setdir _dir;
p_car setvehicleposition [_pick,[],9];
p_truck setvehicleposition [_pick,[],9];
