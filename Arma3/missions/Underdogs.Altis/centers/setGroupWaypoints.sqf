/*
	Executed from fn_addDefense.
	Setup enemy group's waypoints.

	Parameters:
	0:GROUP
	1:OBJECT - Center

	Returns:
	Nothing
*/
private ["_grp","_data","_pos","_radius","_strategy"];
_grp=_this select 0;
_data=(_this select 1) call ud_fnc_centerdata;
_pos=_data select 1;
_radius=(markersize (_data select 3)) select 0;
// Fix
sleep 2;
// Find first live unit and set wp here
{
	if (alive _x) exitwith
	{
		[_grp,(currentwaypoint _grp)] setwppos position _x;
		//diag_log text format ["setwwp %1 %2",_x,position _x];
	};
} foreach units _grp;
sleep 2;
while {(count (waypoints _grp))>0} do
{
	deletewaypoint ((waypoints _grp) select 0);
};

_strategy=floor(random 2);
switch (_strategy) do
{
	case 0: // Hold
	{
		private "_wp";
		_wp=_grp addwaypoint [_pos,random(_radius*0.6)];
		_wp setwaypointposition [_pos,20];
		_wp setwaypointtype "hold";
		_grp setcurrentwaypoint _wp;
	};
	case 1: // Cycle walk
	{
		private ["_r","_px","_py","_wp"];
		_r=20+random(_radius*0.6);
		_px=_pos select 0;
		_py=_pos select 1;
		_pos=[_px+cos(120*0)*_r,_py+sin(120*0)*_r];
		_wp=_grp addwaypoint [_pos,10];
		_wp setwaypointtype "move";
		_pos=[_px+cos(120*1)*_r,_py+sin(120*1)*_r];
		_wp=_grp addwaypoint [_pos,10];
		_wp setwaypointtype "move";
		_pos=[_px+cos(120*2)*_r,_py+sin(120*2)*_r];
		_wp=_grp addwaypoint [_pos,10];
		_wp setwaypointtype "cycle";
		[_grp,0] setwaypointbehaviour "safe";
		_grp setcurrentwaypoint [_grp,0];
	};
};
//diag_log text format ["Group %1 uses WP Strategy %2",_grp,_strategy];
//_grp call ud_fnc_debugwp;
