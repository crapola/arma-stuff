/*
	Spawns a heli.
	_this is PositionASL center/loiter pos
*/
private ["_dir_to_p","_helipos","_ret","_wp","_grp"];
_dir_to_p=[getposasl player,_this] call bis_fnc_vectorfromxtoy;
_helipos=[_dir_to_p,1000] call bis_fnc_vectormultiply;
_helipos=[_this,_helipos] call bis_fnc_vectoradd;
_helipos set [2,200];
_ret=[_helipos,0,"O_Heli_Attack_02_F",east] call bis_fnc_spawnvehicle;
_grp=_ret select 2;
_wp=_grp addwaypoint [_this,50];
_wp setwaypointtype "loiter";
_grp setcurrentwaypoint _wp;