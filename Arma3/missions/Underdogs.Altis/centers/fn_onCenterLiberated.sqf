/*
	Called from a center's trigger when no enemies left.
	_this is index in ud_centers indicating caller.
*/
private ["_c","_data","_groups","_centers_left","_reinforced","_grp","_rdef"];
_c=ud_centers select _this;
_data=_c call ud_fnc_centerdata;
// Delete trigger
deletevehicle (_data select 2);
// Set area blue
(_data select 3) setmarkercolor "colorblue";
// Reassign group(s)
_c setvariable ["defense",[]];
_groups=_data select 4;
// Get occupied center left
_centers_left=[];
{
	if ( count ((_x call ud_fnc_centerdata) select 4)>0 ) then
	{
		_centers_left=_centers_left+[_x];
	};
} foreach ud_centers;
diag_log text format ["Centers left: %1",_centers_left];
if (count _centers_left>0) then
{
	/* Heli removed
	if (count _centers_left==1) then
	{
		// If down to last center, heli appears
		(getposasl (_centers_left select 0)) execvm "centers\heli.sqf";
	};
	*/
	{
		// Any units left?
		if (count units _x>0) then
		{
			// Make sure dead
			{_x setdamage 1;} foreach units _x;
		};
		// Pick a random center to reinforce
		_reinforced=_centers_left call bis_fnc_selectrandom;
		_grp=[_reinforced,_x] call ud_fnc_adddefense;
		diag_log text format ["%1 will be reinforced by %2.",(_reinforced call ud_fnc_centerdata) select 0,_grp];
		//hint format ["%1 will be reinforced by %2.",(_reinforced call ud_fnc_centerdata) select 0,_grp];
		// Add group to its data
		_rdef=(_reinforced call ud_fnc_centerdata) select 4;
		[_rdef,_grp] call bis_fnc_arraypush;
		_reinforced setvariable ["defense",_rdef];
	} foreach _groups;
	// Do the rest
	_this execvm "centerLiberated.sqf";
}
else
{
	_this spawn
	{
		sleep 1;
		["center",_this] call bis_fnc_missiontasks;
		sleep 3;
		"win" call bis_fnc_endmission;
	};
};