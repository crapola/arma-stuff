/*
	Get useful variables from a center.

	Parameter:
	OBJECT - Center object

	Returns:
	0:STRING - Name
	1:POSITION3D - Position ATL
	2:TRIGGER - Trigger
	3:MARKER - Area marker
	4:ARRAY - Array of groups assigned to this center
*/
[
_this getvariable "name",
getposatl _this,
_this getvariable "trigger",
_this getvariable "marker_area",
_this getvariable "defense"
];