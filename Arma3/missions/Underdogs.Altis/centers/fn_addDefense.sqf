/*
	Add enemy defense to a center.

	Parameter:
	0:OBJECT - Center
	1:GROUP (optional) - Group to recycle

	Returns:
	GROUP - Created group
*/
private ["_center","_pos","_grp","_new_grp","_class"];
_center=[_this,0,objnull] call bis_fnc_param;
_grp=[_this,1,grpnull] call bis_fnc_param;

if (isnull _center) exitwith
{
	"Cannot add defense" call bis_fnc_error;
};

// Pick group class
_dice=floor(random 5);
if (player_renown>200) then
{
	_dice=_dice+floor(random 3);
};
_class=(ud_enemy_classes select _dice) call bis_fnc_selectrandom;
diag_log text format ["Picked class: %1",_class];
_pos=[_center call ud_fnc_centerdata select 1,1,30,1,0,10,0] call bis_fnc_findsafepos;
_new_grp=[_pos,east,_class,nil,nil,[0,0.25]] call bis_fnc_spawngroup;

//_new_grp=creategroup east;_new_grp createunit ["O_Soldier_F",_pos,[],0,"form"];// debug:just one guy.

if (!isnull _grp) then
{
	diag_log "Reusing group";
	units _new_grp join _grp;
	deletegroup _new_grp;
}
else
{
	diag_log "New group";
	_grp=_new_grp;
};
//{addswitchableunit _x } foreach units _grp;//debug
// Waypoints
[_grp,_center] execvm "centers\setGroupWaypoints.sqf";
// Return
_grp;