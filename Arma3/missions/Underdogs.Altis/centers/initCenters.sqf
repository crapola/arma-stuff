/*
	Initialize centers and defense.
*/

// Globals ---------------------------------------------------------------------
ud_centers=[]; // List of centers

/* All OPFOR Groups cfg paths
0: Infantry
1: UInfantry
2: SpecOps
3: Support
4: Motorized
5: Mechanized
6: Armored
*/ 
ud_enemy_classes=[];
{
	ud_enemy_classes set
	[
		_foreachindex,
		[
			(configfile>>"cfggroups">>"east">>"opf_f">>_x),
			0,false
		] call bis_fnc_returnchildren
	];
	//diag_log (ud_enemy_classes select _foreachindex);
} foreach
((configfile>>"cfggroups">>"east">>"opf_f") call bis_fnc_getcfgsubclasses);
// Remove divers
[ud_enemy_classes,(configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "SpecOps" >> "OI_diverTeam")] call bis_fnc_removenestedelement;
[ud_enemy_classes,(configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "SpecOps" >> "OI_diverTeam_boat")] call bis_fnc_removenestedelement;
[ud_enemy_classes,(configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "SpecOps" >> "OI_diverTeam_SDV")] call bis_fnc_removenestedelement;

// -----------------------------------------------------------------------------
private ["_class","_center_pool","_center_data","_fnc_createcenter"];

_class="Land_BagBunker_Large_F";
_center_pool=[
["Agios Dionysos",[9275,15960],250],
["Syrta",[8540,18350],200],
["Lakka",[12430,15700],220],
["Athira",[13830,18670],400],
["Galati",[10332,19060],250],
["Orino",[10485,17232],180],
["Gravia",[14563,17650],250]
];
// Pick 4 random cities from pool
_center_data=[_center_pool call bis_fnc_arrayshuffle,0,3] call bis_fnc_subselect;

_fnc_createcenter=compile preprocessfilelinenumbers "centers\createCenter.sqf";
{
	private ["_name","_pos","_radius","_v","_grp"];
	_name=_x select 0;
	_pos=_x select 1;
	_radius=_x select 2;
	_v=[_name,_pos,_radius,_class,_foreachindex] call _fnc_createcenter;
	_grp=[_v] call ud_fnc_adddefense;
	_grp setgroupid [format["OPDEF_%1",_foreachindex]];
	_v setvariable ["defense",[_grp]];
	[ud_centers,_v] call bis_fnc_arraypush;
} foreach _center_data;

diag_log ud_centers;
