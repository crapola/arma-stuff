/*
	Creates one center.

	Parameters:
	0:STRING - Name
	1:POSITION - Position
	2:NUMBER - Area radius
	3:STRING - Vehicle class
	4:NUMBER - Global index

	Returns:
	OBJECT - The bunker vehicle
*/
private ["_name","_pos","_radius","_class","_index","_v","_mn","_ma","_t"];
_name=_this select 0;
_pos=_this select 1;
_radius=_this select 2;
_class=_this select 3;
_index=_this select 4;
// Place bunker
_v=createvehicle [_class,_pos,[],0,"none"];
// Marker icon and name
_mn=createmarker ["marker_center_name_"+str _index,_pos];
_mn setmarkershape "icon";
_mn setmarkertype "mil_box_noshadow";
//_mn setmarkertext _name; make optinal
// Marker area
_ma=createmarker ["marker_center_area_"+str _index,_pos];
_ma setmarkershape "ellipse";
_ma setmarkerbrush "solid";
_ma setmarkersize [_radius,_radius];
_ma setmarkeralpha 0.5;
_ma setmarkercolor "colorred";
// Trigger
_t=createtrigger ["EmptyDetector",_pos];
_t settriggerarea [_radius,_radius,0,false];
_t settriggeractivation ["east","not present",false];
_statement=format["%1 call ud_fnc_oncenterliberated",_index];
_t settriggerstatements ["this",_statement,""];
// Save it
_v setvariable ["name",_name];
_v setvariable ["marker_name",_mn];
_v setvariable ["marker_area",_ma];
_v setvariable ["trigger",_t];
diag_log text format ["Center created: %1",_v];
_v;