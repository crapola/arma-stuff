/*
	Give tasks and briefing to a new unit.

	Parameter:
	UNIT

	Returns:
	Nothing
*/
private ["_data","_name","_marker"];
{
	_data=_x call ud_fnc_centerdata;
	_name=_data select 0;
	_marker=_data select 3;
	[
		format ["center_%1",_foreachindex],
		_this,
		[
			format ["Eliminate all enemies near <marker name='%2'>%1</marker>.",_name,_marker],
			format ["Capture %1",_name],
			"CAPTURE"
		],
		_data select 1, // Destination
		false, // Current
		_foreachindex, // Priority
		false, // Don't notify
		true // Globally
	]
	call bis_fnc_settask;
} foreach ud_centers;
// Commanding
_this creatediaryrecord ["diary",["Commanding",
"High Command is enabled in this mission. You can have up to four squads.<br/>
You can also teamswitch to any of your men.<br/>
If the commander dies, the next unit will take that role."
]];
// Mission
private["_names_list","_sep","_str","_markers"];
_names_list=[];
_markers=[];
{
	_names_list set [_foreachindex,(_x call ud_fnc_centerdata) select 0];
	_markers set [_foreachindex,(_x call ud_fnc_centerdata) select 3];
} foreach ud_centers;
_sep="";
_str="";
{
	_str=_str+_sep+format ["<marker name='%1'>",_markers select _foreachindex]+_x+"</marker>";
	if (_foreachindex==(count _names_list)-2) then
	{
		_sep=" and ";
	}
	else
	{
		_sep=", ";
	};
} foreach _names_list;
_str="Your mission is to capture "+_str+".";
_this creatediaryrecord ["diary",["Mission",_str]];
// Situation
_this creatediaryrecord ["diary",["Situation","
As the leader of a small resistance group you decide to take the fight to the \
CSAT. While you are outnumbered you hope that your actions will inspire locals \
to offer the help they can and become new recruits.<br/>
If you are too reckless with the life of your men however, they might be more \
reluctant to do so."]];