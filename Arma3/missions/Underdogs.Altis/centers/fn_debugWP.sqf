/*
	Log waypoint information for a group.

	Parameter:
	GROUP

	Returns:
	Nothing
*/
diag_log text format ["Waypoint information for group %1:",_this];
{
	diag_log text format ["%1: Position=%2, Type=%3, Behavior=%4",_foreachindex,waypointposition _x,waypointtype _x,waypointbehaviour _x];
} foreach waypoints _this;