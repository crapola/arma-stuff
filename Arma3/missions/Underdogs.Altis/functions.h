class CfgFunctions
{
	class UD
	{
		class centers
		{
			file="centers";
			class centerData{};
#ifdef DBG
			class debugWP{};
			class debugKill{};
#endif
			class giveTasks{};
			class onCenterLiberated{};
			class addDefense{};
		};
		class prizes
		{
			file="prizes";
			class addMarker{};
			class collectBodies{};
			class commanderKilled{};
			class countPlayerTroops{};
			class createMisc{};
			class createTourist{};
			class giveUnits{};
			class giveSwag{};
			class rewardPlayer{};
			class interpolateTables{};
			class inverseOdds{};
			class pickClass{};
		};
	};
};