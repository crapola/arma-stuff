/*
	Executed when a center was liberated.
	Bestows prizes and shows dialog.

	Parameter:
	NUMBER: Center index
*/
_center_data=(ud_centers select _this) call ud_fnc_centerdata;
_center_pos=_center_data select 1;
// Trigger is deleted at this point.
_center_radius=(markersize (_center_data select 3)) select 0;
["center",_this] call bis_fnc_missiontasks;
enablesaving [false,false];
sleep 2;
1 cuttext ["","black"];
sleep 1;
// Update stats
call ud_fnc_rewardplayer;

// Prizes
[_center_pos,_center_radius] call ud_fnc_collectbodies;
_units_text=_center_pos call ud_fnc_giveunits;

// Vehicles and misc objects
_swag_text=_center_pos call ud_fnc_giveswag;

player_men=(count units group player);
_o=random 1;
60 setovercast _o;
60 setrain _o;
60 setfog _o/3;
skiptime 2;
sleep 1;
// Show dialog
1 cuttext ["","black in"];
sleep 1.25;
_msg=format["%1 has been liberated!<br/>
The place has been cleaned up and enemy weapons were collected in crates.<br/>
%2<br/>
In addition, you get the following:<br/>
%3<br/>
New items and troops are avaible near %1's bunker."
,_center_data select 0,_units_text,_swag_text];
uinamespace setvariable ["info_prize",[_msg]];
createdialog "infoPrize";
sleep 2;
enablesaving [true,true];
player call bis_fnc_savegame;

