// Init HC
if (isnil "bis_hc_mainscope") then
{
	_hc_group=creategroup sidelogic;
	bis_hc_mainscope = _hc_group createunit ["HighCommand",[0,0],[],0,"none"];
	bis_hc_mainscope synchronizeobjectsadd [player]; 
};
// Commander killed EH
player addeventhandler ["killed",ud_fnc_commanderkilled];
player setrank "captain";

player_renown=0; // Used by centers,prizes

call compile preprocessfilelinenumbers "centers\initCenters.sqf";
call compile preprocessfilelinenumbers "prizes\initPrizes.sqf";

#include "playerStart.sqf"

// Tasks and briefing
{ _x call ud_fnc_givetasks; } foreach units group player;
["center_0"] call bis_fnc_tasksetcurrent;

// Set time
_date=date;
_date set [3,6+random 4];
_date set [4,random 60];
diag_log text format ["Setting date: %1",_date];
setdate _date;

// Starting equipment
//player addweapon "binocular";
{
_x addprimaryweaponitem "acc_flashlight";
} foreach units group player;

// Wait sim
sleep 1;

// onTeamSwitch only works after simulation starts
onteamswitch
{
	// This fixes a bug with HC and teamswitching... sometimes
	// Taken from http://forums.bistudio.com/showthread.php?144208-High-Command-playable-units-possible&p=2271475&viewfull=1#post2271475
	selectplayer (leader _from);
	if (vehicle _from == _from) then
	{
		{_x dofollow leader _from} foreach units _from;
		unassignvehicle _from;
	};
	selectplayer _to;
};