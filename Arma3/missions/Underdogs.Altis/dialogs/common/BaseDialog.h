// Dialog with orange titlebar and dark background.

#include "common.h"

class BaseDialog
{
	idd = -1;
	movingEnable = true;
	onload="";

	class ControlsBackground
	{
		class TitleBar: RscText
		{
			idc = 10;
			text = "";
			moving = true;
			shadow = 0;
			x = 0.5-GW(8);
			y = 0;
			w = GW(16);
			h = GH(1);
			colorbackground[] = ORANGE;
		};
		class DarkBackground: IGUIBack
		{
			idc = 11;
			x = 0.5-GW(8);
			y = GH(1.2);
			w = GW(16);
			h = GH(23);	
		};
	};

	class Controls
	{
		class Cancel: RscButtonMenuCancel
		{
			text = "Close";
			x = GW(16-3.5-0.2);
			y = GH(23);
			w = GW(3.5);
			h = GH(1);
		};
	};
};