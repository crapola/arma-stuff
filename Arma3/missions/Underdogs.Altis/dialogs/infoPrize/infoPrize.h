class InfoPrize: BaseDialog
{
	onload="_this execvm 'dialogs\infoPrize\infoPrize.sqf'";
	enablesimulation=false;
	class Controls: Controls
	{
		class Info: RscStructuredText
		{
			idc = 1000;
			x = 0.5-GW(8)+GW(0.2);
			y = GH(1.4);
			w = GW(16-0.4);
			h = GH(23-0.2*8);
			colorBackground[] = BLACK_ALPHA;
			size = GH(0.75);
		};
		class Cancel: Cancel
		{
			text = "Continue";
			x = 0.5+GW(8-4.5-0.2);
			y = GH(23);
			w = GW(4.5);
			h = GH(1);
		};
	};
};