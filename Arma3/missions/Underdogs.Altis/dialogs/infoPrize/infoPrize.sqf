disableserialization;

with uinamespace do
{
	_display=_this select 0;
	ctrlsettext [10,"Center Liberated"];

	if (!isnil "info_prize") then
	{
		_ct=_display displayctrl 1000;
		_ct ctrlsetstructuredtext parsetext (info_prize select 0);
	}
	else
	{
		["info_prize was nil!"] call bis_fnc_error;
	};
	info_prize=nil;
};