/*
	Give vehicles.

	Parameter:
	POSITION - Center location
	Returns:
	STRING - Description for the dialog
*/
private ["_numswag","_list","_class","_displayname","_safepos","_mk"];
_numswag=floor(2+random (player_renown/50));
_list="";

private ["_array_best","_inter","_array"];
// Ideal odds
_array_best=ud_vehicles call ud_fnc_inverseodds;
// Interpolate
_inter=100 min (player_renown*0.125);
_array=[ud_vehicles,_array_best,_inter] call ud_fnc_interpolatetables;
// Debug
//diag_log text "Worse:";{diag_log text format ["%1 , %2",_x select 0,_x select 1];} foreach ud_vehicles;
//diag_log text "Best:";{diag_log text format ["%1 , %2",_x select 0,_x select 1];} foreach _array_best;
//diag_log text format ["With %1:",_inter];{diag_log text format ["%1 , %2",_x select 0,_x select 1];} foreach _array;

for "_z" from 1 to _numswag do
{
	_safepos=[_this,2,50,2,0,4,0] call bis_fnc_findsafepos;
	_class=_array call ud_fnc_pickclass;
	if (_class=="SillyItem") then
	{
		_class=_safepos call ud_fnc_createmisc;
	}
	else
	{
		createvehicle [_class,_safepos,[],0,"none"];
	};
	_displayname=(configfile>>"cfgvehicles">>_class) call bis_fnc_displayname;
	_list=_list+"- "+_displayname+"<br/>";
	[_safepos,_displayname] call ud_fnc_addmarker;
};
_list;