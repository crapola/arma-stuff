/*
	Give new units.

	Parameter:
	POSITION - Center location
	Returns:
	STRING - Description for the dialog
*/
private ["_numtroops","_unit","_troop_list","_cur_squad","_i","_ps",
"_array_best","_grp","_displayname","_class","_array","_inter","_u",
"_safe_pos"];
// Check if we got room
_numtroops=47-call ud_fnc_countplayertroops;
diag_log text format ["Player has %1 troops. Room for %2",call ud_fnc_countplayertroops,_numtroops];

if (_numtroops==0) exitwith
{
	diag_log "No more room";
	"Nobody joined because your squads are already at full capacity.";
};
if (side player==sideenemy) exitwith
{
	"Nobody joined because you are a pariah!";
};
if (!alive player) exitwith
{
	"Nobody joined because you are dead.";
};

// Ideal odds
_array_best=ud_soldiers call ud_fnc_inverseodds;
// Interpolate
_inter=100 min (player_renown*0.125);
_array=[ud_soldiers,_array_best,_inter] call ud_fnc_interpolatetables;

// Debug
//diag_log text "Worse:";{diag_log text format ["%1 , %2",_x select 0,_x select 1];} foreach ud_soldiers;
//diag_log text "Best:";{diag_log text format ["%1 , %2",_x select 0,_x select 1];} foreach _array_best;
//diag_log text format ["With %1:",_inter];{diag_log text format ["%1 , %2",_x select 0,_x select 1];} foreach _array;

_troop_list="";
_displayname="";
_numtroops=_numtroops min floor(2+random (2+(6 min (player_renown/100))));
diag_log text format ["Adding %1 troops",_numtroops];
_grp=creategroup side player;
assert(!isnull _grp); // happens if sideenemy
_unit=objnull;
for "_z" from 1 to _numtroops do
{
	_safe_pos=[_this,1,30,1,0,10,0] call bis_fnc_findsafepos;
	_class= _array call ud_fnc_pickclass;
	if (_class=="") then
	{
		_class="B_G_Soldier_F";
	};

	if (_class=="WarTourist") then
	{
		_unit=[_safe_pos,_grp] call ud_fnc_createtourist;
		_displayname="War tourist";
	}
	else
	{
		_displayname=(configfile>>"cfgvehicles">>_class) call bis_fnc_displayname;
		_unit=_grp createunit [_class,_safe_pos,[],0,"form"];
	};
	_troop_list=_troop_list+"- "+_displayname+"<br/>";
	addswitchableunit _unit;
	_unit call ud_fnc_givetasks;
};

// Split into the players squads
private ["_cur_squad","_ps","_i","_u"];
_cur_squad=0;
_i=0;
while {_i<_numtroops} do
{
	_u=(units _grp) select 0;
	_ps=player_squads select _cur_squad;
	if ((count units _ps)<12) then
	{
		[_u] joinsilent _ps;
		diag_log text format ["%1 joins squad %2 side %3 hcleader=%4",_u,_ps,side _ps,hcleader _ps];
		_i=_i+1;
	}
	else
	{
		_cur_squad=_cur_squad+1;
	};
	if (_cur_squad>=4) exitwith
	{
		["Too many troops"] call bis_fnc_error;
	};
};
// Clean up
deletegroup _grp;
// Return
format ["%1 soldiers are willing to join your cause:<br/>%2",_numtroops,_troop_list];