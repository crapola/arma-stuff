/*
	Create one War Tourist.

	Parameter:
	0: POSITION - Position
	1: GROUP - Group he spawns in

	Returns:
	UNIT - The guy
*/
private "_unit";
_unit=(_this select 1) createunit ["C_man_1",(_this select 0),[],0,"form"];
removeallitems _unit;
// They get a revolver
_unit addweapon "hgun_Pistol_heavy_02_F";
// with 2 to 3 mags
_unit addmagazine "6Rnd_45ACP_Cylinder"; 
_unit addmagazine "6Rnd_45ACP_Cylinder"; 
if (0.5>random 1) then {_unit addmagazine "6Rnd_45ACP_Cylinder";};
//...and a hat
_unit addheadgear "H_Hat_blue";
_unit;