/*
	Add renown after capture.

	+50 city
	+[0,100] casualties
	+[0,20] luck
*/
player_renown=player_renown+50;
_cur_men=call ud_fnc_countplayertroops;
// Can be neg if deads no discovered
_casualties=0 max ((player_men-_cur_men)/player_men*100);
//diag_log text format ["Player had %1 troops and now has %2, casualties is %3 percent.",player_men,_cur_men,_casualties];
player_renown=player_renown+100-_casualties;
_dice=random 100;
if (_dice<player_luck) then
{
	player_renown=player_renown+(random player_luck)/5;
	player_luck=0 max (player_luck-20);
};
player_renown=floor(player_renown);
//diag_log text format ["Renown is now %1",player_renown];