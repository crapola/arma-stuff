/*
	Delete the dead and accumulate stuff in crates.

	Paramaters:
	0:POSITION - Desired crate position
	1:NUMBER - Radius of cleanup
*/
_pos=[(_this select 0),0,10,1.5,0,1,0] call bis_fnc_findsafepos;
_rad=_this select 1;
_crate=createvehicle ["Box_IND_Wps_F",_pos,[],0,"none"];
_mk=[_pos,"Crates"] call ud_fnc_addmarker;
_mk setmarkercolor "colorgreen";
clearweaponcargo _crate;
clearmagazinecargo _crate;
clearitemcargo _crate;
_pos=getposatl _crate;

_blacklist=[
// cfgmagazines smokes
"1Rnd_Smoke_Grenade_shell",
"1Rnd_SmokeBlue_Grenade_shell",
"1Rnd_SmokeGreen_Grenade_shell",
"1Rnd_SmokeOrange_Grenade_shell",
"1Rnd_SmokePurple_Grenade_shell",
"1Rnd_SmokeRed_Grenade_shell",
"1Rnd_SmokeYellow_Grenade_shell",
"SmokeShell",
"SmokeShellBlue",
"SmokeShellGreen",
"SmokeShellOrange",
"SmokeShellPurple",
"SmokeShellRed",
"SmokeShellYellow"
];

_fn_tocrate=
{
	/*
		Add to current crate or new crate if no room.
		Returns current crate.
		_blacklist,_pos pass through
	*/
	private ["_crate","_item"];
	_crate=_this select 0;
	_item=_this select 1;

	if (_item in _blacklist) exitwith
	{
		_crate;
	};

	if ((isnull _crate) || !(_crate canadd _item)) then
	{
		_pos set [1,0.75+(_pos select 1)];
		_crate=createvehicle ["Box_IND_Wps_F",_pos,[],0,"can_collide"];
		clearweaponcargo _crate;
		clearmagazinecargo _crate;
		clearitemcargo _crate;
	};
	_crate additemcargo [_item,1];
	_crate;
};

// Dropped weapons
_holders=_pos nearobjects ["weaponholdersimulated",_rad];
{
	_cargo=getweaponcargo _x;
	_items=_cargo select 0;
	_count=_cargo select 1;
	{
		_crate addweaponcargo [_x,_count select _foreachindex];
	} foreach _items;

} foreach _holders;

// Dead units
private "_stuff";
{
	if (""!=backpack _x) then
	{
		_crate addbackpackcargo [backpack _x,1];
	};
	_stuff=weapons _x;
	[_stuff,(itemswithmagazines _x)] call bis_fnc_arraypushstack;
	[_stuff,(assigneditems _x)] call bis_fnc_arraypushstack; // Map,NV...
	{
		// Chance of object being "broken"
		if ((random 90)<player_luck) then
		{
			_crate=[_crate,_x] call _fn_tocrate;
		};
	} foreach _stuff;
	deletevehicle _x;
} foreach alldeadmen;