/*
	Initialize prizes database.
*/

ud_soldiers=[
["WarTourist"			,300],// War tourist
["B_G_Soldier_F"		,450],// Rifleman
["B_G_Soldier_lite_F"	,550],// Rifleman (lite)
["B_G_Soldier_AR_F"		,650],// Autorifleman
["B_G_medic_F"			,690],// Combat life saver
["B_G_engineer_F"		,730],// Engineer
["B_G_Soldier_exp_F"	,780],// Explosive specialist
["B_G_Soldier_GL_F"		,850],// Grenadier
["B_G_Soldier_M_F"		,900],// Marxman
["B_G_Soldier_LAT_F"	,940],// Rifleman (AT)
["B_G_Soldier_A_F"		,1000]// Ammo Bearer
];

ud_vehicles=[
["SillyItem"				,200],// Useless object
["B_G_Van_01_transport_F"	,400],// Simple transport
["B_G_Van_01_fuel_F"		,510],
["B_G_Quadbike_01_F"		,630],
["B_G_Offroad_01_F"			,750],
["B_G_Offroad_01_armed_F"	,850],
["I_APC_tracked_03_cannon_F",900],// APC
["I_MBT_03_cannon_F"		,950],// Tank
["B_G_Mortar_01_F"			,1000] // Mortar
];

ud_marker_iterator=0; // For unique names

player_luck=100;
// Player has 4 squads including his group
player_squads=
[
group player,
creategroup side player,
creategroup side player,
creategroup side player
];
player hcsetgroup [player_squads select 0,"Alpha"];
player hcsetgroup [player_squads select 1,"Bravo"];
player hcsetgroup [player_squads select 2,"Charlie"];
player hcsetgroup [player_squads select 3,"Delta"];
(player_squads select 0) setgroupid ["Alpha"];
(player_squads select 1) setgroupid ["Bravo"];
(player_squads select 2) setgroupid ["Charlie"];
(player_squads select 3) setgroupid ["Delta"];
player_men=call ud_fnc_countplayertroops;