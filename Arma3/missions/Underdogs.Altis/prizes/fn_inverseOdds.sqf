/*
	Get table with inversed odds.

	Parameter:
	ARRAY - Source table

	Returns:
	ARRAY - Copy with new odds
*/
private ["_array","_num","_above","_val","_i"];
_array=+_this;
_num=count _array-1;
_above=0;
for "_i" from 0 to _num do
{
	_back=_num-_i;
	if (_back>0) then
	{
		_above=(_this select (_back-1)) select 1;
	}
	else
	{
		_above=0;
	};
	_val=((_this select _back) select 1)-_above;
	(_array select _i) set [1,_val];
};
_above=0;
{
	_above=(_x select 1)+_above;
	_x set [1,_above];
} foreach _array;
_array;