/*
	"Killed" event handler attached to High Commander.
	If he dies we pick another unit.

	Parameters:
	0: OBJECT - Killed
	1: OBJECT - Killer
*/
private ["_killed","_new"];
_killed=_this select 0;
_new=objnull;
// Find a candidate
scopename "main";
{
	{
		if (alive _x) then
		{
			_new=_x;
			breakto "main";
		};
	} foreach units _x;
} foreach player_squads;

if (isnull _new) exitwith
{
	diag_log text "No unit alive to be new commander.";
};

hcremoveallgroups _killed;
_new hcsetgroup [player_squads select 0];
_new hcsetgroup [player_squads select 1];
_new hcsetgroup [player_squads select 2];
_new hcsetgroup [player_squads select 3];
_new setvariable ["BIS_HC_scope",bis_hc_mainscope,true];
_new addeventhandler ["killed",ud_fnc_commanderkilled];
_new setrank "captain";

diag_log text format ["Commander killed. New one is %1.",_new];
