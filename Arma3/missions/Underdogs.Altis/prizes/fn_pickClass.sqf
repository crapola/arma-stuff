/*
	Pick one random item from table.

	Parameter:
	ARRAY - Table
	Returns:
	STRING - Class name
*/
private ["_dice","_class","_val"];
_dice=floor(random 1000);
_class="";
{
	_val=_x select 1;
	if (_dice<=_val) exitwith
	{
		_class=_x select 0;
	};
} foreach _this;
if (_class=="") then
{
	["No item with %1 !",_dice] call bis_fnc_error;
	diag_log text format ["Error: no item after rolling %1.",_dice];
};
_class;