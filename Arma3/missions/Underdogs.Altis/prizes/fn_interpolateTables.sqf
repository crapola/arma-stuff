/*
	Interpolate two tables by a factor.

	Parameter:
	0:ARRAY - Source table
	1:ARRAY - Target
	1:NUMBER - Factor in range [0,100]
	Returns:
	ARRAY - Copy with new odds
*/
private ["_src","_tgt","_power","_src_part","_dest_part","_val"];
_src=+_this select 0;
_tgt=_this select 1;
_power=_this select 2;
{
	_src_part=(_x select 1)*(100-_power);
	_dest_part=((_tgt select _foreachindex) select 1)*_power;
	_val=floor((_src_part+_dest_part)/100);
	_x set [1,_val];
} foreach _src;
_src;