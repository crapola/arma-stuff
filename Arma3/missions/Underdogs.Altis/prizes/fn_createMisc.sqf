/*
	Creates a random useless thing or animal.

	Parameter:
	POSITION - Safe position
	Returns:
	STRING - Class name
*/
private ["_stuff","_pick","_ret"];
// [ClassName,isUnit]
_stuff=[
["Land_TacticalBacon_F",false],
["Land_BottlePlastic_V1_F",false],
["Land_Can_V2_F",false],
["Alsatian_Random_F",true],
["Turtle_F",true],
["Cock_random_F",true]
];
_pick=_stuff call bis_fnc_selectrandom;
_ret=_pick select 0;
if (_pick select 1) then
{
	if (isnil "ud_misc_group") then
	{
		ud_misc_group=creategroup civilian;
		ud_misc_group setgroupid ["Misc"];
	};
	ud_misc_group createunit [_ret,_this,[],0,"none"];
}
else
{
	createvehicle [_ret,_this,[],0,"none"];
};
_ret;