/*
	Total number of troops across squads, not including player.
*/
private "_count";
_count=0;
{
	_count=_count+count units _x;
} foreach player_squads;
_count-1;