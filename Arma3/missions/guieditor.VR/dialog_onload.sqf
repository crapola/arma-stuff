disableserialization;

_d=_this select 0;
hint str _this;
_list=_d displayctrl 1500;
uinamespace setvariable ["_desc",_d displayctrl 2000];

_w = "arifle_MX_F";
_wc = configfile >> "cfgweapons" >> _w;
_dname = gettext (_wc >> "displayname");
_icon = gettext (_wc >> "picture");

//_icon = _icon call bis_fnc_textureVehicleIcon;
if (_icon == "") then {_icon = "#(argb,8,8,3)color(1,0,1,0)";};

_i=_list lbadd _dname;
_list lbsetdata [_i,_w];
_list lbsetpicture [_i,_icon];

_list lbadd "Item1";
_list lbadd "Item2";

_list lbsetdata [2,"swag"];
