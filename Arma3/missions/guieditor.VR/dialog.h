#include "classes.h"

#define GUI_GRID_X	(0)
#define GUI_GRID_Y	(0)
#define GUI_GRID_W	(0.025)
#define GUI_GRID_H	(0.04)
#define GUI_GRID_WAbs	(1)
#define GUI_GRID_HAbs	(1)

// Macro to specify in grid units
#define GW(x) ((x)*GUI_GRID_W)
#define GH(x) ((x)*GUI_GRID_H)

// Spacing 4th of a grid
#define SX (0.00625)
#define SY (0.01)

class Dialog
{
	idd=-1;
	movingenable=true;
	onload="_this execvm 'dialog_onload.sqf'";
	class Controls
	{
		class RscText_1000: RscText
		{
			moving=true;
			idc = 1000;
			text = "Hello"; //--- ToDo: Localize;
			x = 0;
			y = 0;
			w = 1;
			h = GH(1);
			colorBackground[] = {1,0,0,1};
		};
		class IGUIBack_2200: IGUIBack
		{
			idc = 2200;
			x = 0;
			y = GH(1)+SY;
			w = 1;
			h = 1-GH(1)-SY;
		};
		class RscListbox_1500: RscListbox
		{
			idc = 1500;
			x = SX;
			y = GH(1)+SY+SY;
			w = GW(12);
			h = GH(20);
			onlbselchanged="_this execvm 'dialog_list_onlbselchanged.sqf'";
		};
		class Desc: RscStructuredText
		{
			idc = 2000;
			text="Selected:";
			x = SX+GW(12)+SX;
			y = GH(1)+SY+SY;
			w = 1-GW(12)-SX-SX-SX;
			h = GH(20);
			colorBackground[] = {0,0,0.25,0.5};
		};
	};
};