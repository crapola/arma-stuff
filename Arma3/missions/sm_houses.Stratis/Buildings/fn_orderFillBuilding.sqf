/*
	Orders a group to go and occupy a building then stay in it.

 	Parameters:
	0: GROUP
	1: OBJECT - Building.

	Returns:
	Nothing.
 */
 private ["_g","_b","_rooms"];
 _g=_this select 0;
 _b=_this select 1;
_rooms=_b call bld_fnc_buildingrooms;
_unitsMoved=[];
{
	if (_foreachindex<count _rooms) then
	{
		_x domove (_rooms select _foreachindex);
		_unitsMoved pushback _x;
	};
} foreach units _g;
_unitsMoved execfsm "Buildings\orderFill.fsm";