/*
	Instantly teleports group units to building rooms.
	If it's full extra soldiers will wait nearby.

 	Parameters:
	0: GROUP
	1: OBJECT - Building.

	Returns:
	Nothing.
 */
 private ["_g","_b","_rooms"];
 _g=_this select 0;
 _b=_this select 1;
_rooms=_b call bld_fnc_buildingrooms;
commandstop (units _g);
{
	if (_foreachindex<count _rooms) then
	{
		_x setposatl (_rooms select _foreachindex);
	} else
	{
		_x setpos ([getpos (leader _g),1,20,1,0,1,0] call bis_fnc_findsafepos);
	};
} foreach units _g;



