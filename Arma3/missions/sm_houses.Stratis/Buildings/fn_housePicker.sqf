/*
	Called from trigger with synchronized units.
	Assigns synced groups to a random building in a circle covered by
	the trigger (radius is (a+b)/2).

	Parameters:
	0: OBJECT - Trigger.
	1: BOOLEAN (optional) - True for instant teleport, false to make them walk
		to it.

	Returns:
	Nothing.

	Usage:
	_x=[thistrigger,true] call bld_fnc_housepicker;deletevehicle this;
*/
_trig=[_this,0,objnull,[objnull]] call bis_fnc_param;
_instant=[_this,1,false,[false]] call bis_fnc_param;
_trigger_props=triggerarea _trig;
_radius=((_trigger_props select 0)+(_trigger_props select 1))/2;
_buildings=[position _trig,_radius] call bld_fnc_gethouses;
//diag_log format ["Buildings: %1",_buildings];
{
	_random_building=_buildings call bis_fnc_selectrandom;
	//diag_log format["Picked %1 for %2.",_random_building,_x];
	if (_instant) then
	{
		[group _x,_random_building] call bld_fnc_fillbuilding;
	} else
	{
		[group _x,_random_building] call bld_fnc_orderfillbuilding;
	};
} foreach synchronizedobjects _trig;