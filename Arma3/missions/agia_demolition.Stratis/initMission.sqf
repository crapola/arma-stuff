// Compile stuff
fnc_buildingrooms=compile preprocessfilelinenumbers "fnc_buildingrooms.sqf";
fnc_fillbuilding=compile preprocessfilelinenumbers "fnc_fillbuilding.sqf";
fnc_gethouses=compile preprocessfilelinenumbers "fnc_gethouses.sqf";
// Create weapon cache
_result=call compile preprocessfilelinenumbers "fnc_placeammo.sqf";
ammo_house=_result select 0;
ammo_crates=_result select 1;
// Place defenders
[group elite_leader,ammo_house] call fnc_fillbuilding;
// Cache detection
_trg=createtrigger["emptydetector",position ammo_house];
_trg settriggerarea[10,10,0,false];
_trg settriggeractivation["west","present",false];
_trg settriggerstatements["this","thislist execvm 'revealcache.sqf'",""];
// Hide extraction
"marker_extract" setmarkeralpha 0;
// Setup end trigger
trigger_extract triggerattachvehicle [player];
trigger_extract settriggeractivation ["member","present",false];
// Identities
player setidentity "armstrong";
p_rifle setidentity "levine";
p_auto setidentity "pb_b_givens";
p_medic setidentity "miller";
p_arson setidentity "pb_b_campbell";
p_snipo setidentity "frost";
// Extra charges
clearallitemsfrombackpack p_arson;
p_arson additemtobackpack "MineDetector";
p_arson additemtobackpack "ToolKit";
for "_i" from 1 to 8 do
{
	p_arson additemtobackpack "DemoCharge_Remote_Mag";
};

// Time and weather
cuttext ["","black faded"];
call compile preprocessfilelinenumbers "settimeandweather.sqf";
// Wait for date to take effect...
sleep 2;
cuttext ["","black in",2];
// Show first task
"task_ammo_cache" call bis_fnc_missiontasks;
// Music
if (((((date select 3)-6)%24)+24)%24>14) then
{
	0 fadeMusic 0.25;
	playMusic "Track09_Night_percussions";
	60 fadeMusic 0;
};

