case "task_ammo_cache": {
	if (!_taskExists)  then
	{
		[
			group player,
			_taskid,
			[
				"Destroy the weapon cache in <marker name='marker_city'>Agia Marina</marker>.",
				"Find and destroy the weapon cache",
				"DESTROY"
			],
			objnull,// position
			true
		] call bis_fnc_taskcreate
	} else
	{
		[_taskID,"succeeded"] call bis_fnc_tasksetstate;
	};
};

case "task_exit": {
	if (!_taskExists)  then
	{
		[
			group player,
			_taskid,
			[
				"Reach the <marker name='marker_extract'>extraction point</marker>.",
				"Extract",
				"EXTRACT"
			],
			"marker_extract",
			true
		] call bis_fnc_taskcreate
	} else
	{
		[_taskID,"succeeded"] call bis_fnc_tasksetstate;
	};
};