/*
Called from trigger with synchronized units.
Assigns synced groups to a random building in a circle covered by
the trigger (radius is (a+b)/2).
Usage:
_x=thistrigger execvm "housepicker.sqf";
*/
_trigger_props=triggerarea _this;
_radius=((_trigger_props select 0)+(_trigger_props select 1))/2;
_buildings=[position _this,_radius] call fnc_gethouses;
//diag_log format ["Buildings: %1",_buildings];
{
	_random_building=_buildings call bis_fnc_selectrandom;
	diag_log format["Picked %1 for %2.",_random_building,_x];
	[group _x,_random_building] call fnc_fillbuilding;
} foreach synchronizedobjects _this;
