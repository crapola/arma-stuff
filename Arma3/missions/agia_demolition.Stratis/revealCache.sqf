_mark=createmarker ["ammo_marker",position ammo_house];
_mark setmarkercolor "colorred";
_mark setmarkertype "mil_destroy";
_mark setmarkertext "Weapon cache";
["task_ammo_cache",ammo_house] call bis_fnc_tasksetdestination;
_unit=_this select 0;// thislist is passed
if (_unit==p_leader || isnull _unit) then
{
	p_leader groupchat "Here it is!";
}
else
{
	_unit groupchat "I found the weapon cache!";
};
playsound ["defaultNotification",true];