/*
	Places the ammo cache in Agia Marina.
	Returns:
	ARRAY
	0: OBJECT - Chosen building
	1: ARRAY - List of created ammo crates
*/
scriptname "fnc_placeAmmo";
private ["_h","_ammo_house","_r","_pos","_crate_type","_ammo_crates",
"_num_to_create","_tp"];
_h=[[2950,6030],150] call fnc_gethouses;
_ammo_house=_h call bis_fnc_selectrandom;
_r=_ammo_house call fnc_buildingrooms;
_pos=_r call bis_fnc_selectrandom;
_crate_types=[
"Box_East_Ammo_F",
"Box_East_AmmoOrd_F",
"Box_East_AmmoVeh_F",
"Box_East_WpsLaunch_F"
];
_ammo_crates=[];
_num_to_create=2+random 4;
{
	if (_num_to_create>0) then
	{
		_ammo_crates set [_foreachindex,
		createvehicle [_crate_types call bis_fnc_selectrandom,_x,[],0,"can_collide"]
		];
		_num_to_create=_num_to_create-1;
	}
} foreach _r;
// Truck as a hint
_tp=[position _ammo_house,5,20,1,0,30,0] call bis_fnc_findsafepos;
createvehicle ["O_G_Van_01_transport_F",_tp,[],0,"none"];
_tp=[_tp,1,3,1,0,30,0] call bis_fnc_findsafepos;
createvehicle ["Box_East_AmmoVeh_F",_tp,[],0,"none"];

[_ammo_house,_ammo_crates];