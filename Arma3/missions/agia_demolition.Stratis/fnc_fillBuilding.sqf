/*
	Orders a group to fill a building
 	Parameters:
	0: GROUP
	1: OBJECT - Building
	Returns:
	Nothing
 */
 scriptname "fnc_fillBuilding";
 private ["_g","_b","_rooms"];
 _g=_this select 0;
 _b=_this select 1;
_rooms=_b call fnc_buildingrooms;
{
	if (_foreachindex<count _rooms) then
	{
		_x commandmove (_rooms select _foreachindex);
		_x spawn
		{
			waituntil {not unitready _this}; // Wait start moving...
			waituntil {unitready _this}; // Wait for stop.
			dostop _this;
		};
	}
	else
	{
		// nothing
		//_x commandmove (_b buildingexit 5);
	};
} foreach units _g;



