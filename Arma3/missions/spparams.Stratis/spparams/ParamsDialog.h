#include "common\common.h"

class ParamText: RscText
{
	x = SX;
	y = SY;
	w = GW(16);
	h = GH(1);
	colorbackground[] = BLACK_ALPHA;
};

class ParamCombo: RscCombo
{
	x = 2*SX+GW(16);
	y = SY;
	w = GW(15);
	h = GH(1);
};

#define PARAMCONTROLS(x) \
class p_title_##x: ParamText\
{\
	idc=1000+x*2;\
	y=SY+GH(x)+SY*x;\
};\
class p_value_##x: ParamCombo\
{\
	idc=1001+x*2;\
	onlbselchanged=__EVAL("[_this,"+str x+"] execvm 'spparams\controls.sqf'");\
	y=SY+GH(x)+SY*x;\
};

class ParamsDialog
{
	idd=-1;
	movingEnable = false;
	onload="_this execvm 'spparams\dialog.sqf'";
	onunload="_this execvm 'spparams\onclose.sqf'";
	onkeydown="diag_log _this;";

	enablesimulation = false;

	class ControlsBackground
	{
		class TitleBar: RscText
		{
			idc = 10;
			text = "Mission Parameters";
			moving = true;
			shadow = 0;
			x = 0;
			y = 0;
			w = 1;
			h = GH(1);
			colorbackground[] = ORANGE;
		};
		class DarkBackground: IGUIBack
		{
			idc = 11;
			x = 0;
			y = GH(1)+SY;
			w = 1;
			h = GH(23);
		};
	};

	class Controls
	{
		class Accept: RscButtonMenuOK
		{
			text = "Continue";
			x = 1-GW(5)-SX;
			y = GH(23);
			w = GW(5);
			h = GH(1);
		};

		class Group: RscControlsGroup
		{
			x = 0;
			y = GH(1)+SY;
			w = 1;
			h = GH(22);

			class Controls
			{
				PARAMCONTROLS(0)
				PARAMCONTROLS(1)
				PARAMCONTROLS(2)
				PARAMCONTROLS(3)
				PARAMCONTROLS(4)
				PARAMCONTROLS(5)
				PARAMCONTROLS(6)
				PARAMCONTROLS(7)
				PARAMCONTROLS(8)
				PARAMCONTROLS(9)
				PARAMCONTROLS(10)
				PARAMCONTROLS(11)
				PARAMCONTROLS(12)
				PARAMCONTROLS(13)
				PARAMCONTROLS(14)
				PARAMCONTROLS(15)
				// if you need more, continue the list.
			};
		};
	};
};