if (!ismultiplayer) then // SP only
{
	waituntil{!isnil "bis_fnc_init"};
	cuttext ["","black faded",0]; // Instant black screen
	// We shouldn't need a sleep but esc->restart bugs out without it
	sleep 0.125;

	call spp_fnc_showdialog; // Show dialog and wait for closing

	// This re-calls the param functions.
	call bis_fnc_initparams;

	cuttext ["","black in"]; // Remove black screen

	getparamvalue=spp_fnc_getparamvalue;// We'll read from the SP array
}
else
{
	getparamvalue=bis_fnc_getparamvalue;// Do as usual
};

// Use params, using getparamvalue function defined above
if (isserver) then
{
	// Set car color to favorite
	_carcolor=["FavoriteColor"] call getparamvalue;
	_cartxt=["blue","red","yellow"];
	_colors=["#(argb,8,8,3)color(0,0,1,1,CO)","#(argb,8,8,3)color(1,0,0,1,CO)","#(argb,8,8,3)color(1,1,0,1,CO)"];
	_v=createvehicle ["c_hatchback_01_f",position p1,[],0,"none"];
	_v setobjecttextureglobal [0,_colors select _carcolor];
	p1 sidechat format ["Got me a %1 car.",_cartxt select _carcolor];

	// Update view distance
	_vd=["ViewDistance",viewdistance] call getparamvalue;
	p1 sidechat format ["Setting viewdistance to %1.",_vd];
	setviewdistance _vd;

	// BIS_fnc_initParams_paramsArray is a compilefinale version of paramsArray
	p1 sidechat format ["paramsArray=%1",paramsArray]; // OK
	p1 sidechat format ["BIS_fnc_initParams_paramsArray=%1",BIS_fnc_initParams_paramsArray]; // Always shows default values in SP
};


