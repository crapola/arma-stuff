_regulars=['O_Soldier_F',1,'O_Soldier_lite_F',1,'O_Soldier_GL_F',1,'O_Soldier_AR_F',1,'O_Soldier_TL_F',1,'O_soldier_M_F',1,'O_Soldier_AT_F',1,'O_medic_F',1,'',0];
_A3_Armor_F_T100K = ['O_MBT_02_cannon_F',2.9,'',0];
_A3_Boat_F_Boat_Armed_01 = ['O_Boat_Armed_01_hmg_F',2,'',0];
_A3_Soft_F_MRAP_02 = ['O_MRAP_02_hmg_F',3.5,'O_MRAP_02_gmg_F',4];

_crew = ['O_crew_F',1,'O_helipilot_F',1,'O_Pilot_F',1,'O_helicrew_F',1,'',0];

_OPFOR =
	_regulars +
	_A3_Soft_F_MRAP_02 +
	_A3_Boat_F_Boat_Armed_01 +
	_crew;

/*
Register curator object costs from a table.
Must be executed where the logic is local
(on the machine of the player it is assigned to).
*/
[game_master,_OPFOR] call bis_fnc_curatorobjectregisteredtable;