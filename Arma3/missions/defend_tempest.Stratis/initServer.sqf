/*if (isdedicated) then
{
	#include "initCurator.sqf"
};*/
#include "initCurator.sqf"

// Some scripts expect 'bis_curator'...
bis_curator=game_master;

// Set forbidden zone triggers to cover markers
[tzone0,"zone0"] call bis_fnc_triggertomarker;
[tzone1,"zone1"] call bis_fnc_triggertomarker;

// Set allowed addons for Zeus
_east0=["CuratorOnly_Characters_F_OPFOR"];
_east1=["CuratorOnly_Soft_F_MRAP_02","CuratorOnly_Boat_F_Boat_Armed_01"];
[game_master,_east0] call bis_fnc_managecuratoraddons;
[game_master,_east1,{bis_fnc_modulemptypedefense_tier > 2}] call bis_fnc_managecuratoraddons;

// Loadouts
[west,"w1"] call bis_fnc_addrespawninventory;
[west,"w2"] call bis_fnc_addrespawninventory;
[west,"w3"] call bis_fnc_addrespawninventory;
[west,"w4"] call bis_fnc_addrespawninventory;
[west,"w5"] call bis_fnc_addrespawninventory;

// SP Params
if (!ismultiplayer) then
{
	waituntil{!isnil "bis_fnc_init"};
	cuttext ["","black faded",0];
	sleep 0.125;
	call spp_fnc_showdialog;
	call bis_fnc_initparams; // This adds tickets but whatever
	cuttext ["","black in"];

	bis_mission setvariable ["cheatCoef",1.125];

	// Fix max wave in flow FSM
	BIS_fnc_moduleMPTypeDefense_missionFlow setfsmvariable ["_tierMax",paramsArray select 0];
} else
{
	bis_mission setvariable ["cheatCoef",0.5];
};

// Apply wave count from params
_tierMax = paramsArray select 0;
if (isnil "_tierMax" || _tierMax == 0) then {_tierMax = ceil random 6;};
bis_mission setvariable ["EvacWave",_tierMax,true];