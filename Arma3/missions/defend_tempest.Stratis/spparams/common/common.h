#include "classes.h"

// Square grid
#define GUI_GRID_W	((((safezoneW/safezoneH) min 1.2)/40))
#define GUI_GRID_H	(((((safezoneW/safezoneH) min 1.2)/1.2)/25))

// Colors
#define ORANGE {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.69])",\
	"(profilenamespace getvariable ['GUI_BCG_RGB_G',0.75])",\
	"(profilenamespace getvariable ['GUI_BCG_RGB_B',0.5])",\
	"(profilenamespace getvariable ['GUI_BCG_RGB_A',0.8])"}

#define BLACK_ALPHA	{"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])",\
	"(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])",\
	"(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])",\
	"(profilenamespace getvariable ['IGUI_BCG_RGB_A',0.8])"}

// Macro to specify in grid units
#define GW(x) ((x)*GUI_GRID_W)
#define GH(x) ((x)*GUI_GRID_H)

// Spacing
#define SX GW(0.2)
#define SY GH(0.2)