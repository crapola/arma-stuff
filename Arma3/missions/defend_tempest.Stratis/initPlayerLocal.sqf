//todo:see if needed...
//#include "initCurator.sqf";

// Editor mode debug stuff
//#define DEBUG

#ifdef DEBUG
hint "Debug";
player allowdamage false;

player addaction
["Kill all enemies",
{
	{
		if ((side _x)==east) then {_x setdamage 1;};
	} foreach allunits;
}];

player addaction
["Curator points",
{
	hint format ["Zeus has %1 points",curatorpoints bis_curator];
}];
#endif