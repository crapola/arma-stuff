// Relocate AI to closest spawn

_u=_this select 0;
if (isplayer _u) exitwith {};
_spawns=_u call bis_fnc_getrespawnpositions;
_leader=leader group _u;
_shortest=90000000;
_index=-1;
{
	_d=(getposatl _leader) vectordistancesqr (getposatl _x);
	if (_d<_shortest) then {_shortest=_d;_index=_foreachindex;};
} foreach _spawns;
if (_index!=-1) then
{
	//[_u,(_spawns select _index)] call bis_fnc_movetorespawnposition;
	_p=getposatl (_spawns select _index);
	_p set [2,0];
	_safep=[_p,0.25,10,0,0,1,0] call bis_fnc_findsafepos;
	_u setpos _safep;
	diag_log [_p,_safep];
};