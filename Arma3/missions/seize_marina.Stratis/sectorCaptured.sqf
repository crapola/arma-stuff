/*
	Executed upon sector capture.
	0=Sector module
	1=Side capturer
	2=Previous side
*/
// Required because defenders "capture" at the start
if ((_this select 1) isequalto east) exitwith{};

// Add tickets
_sectorTickets="Respawn" call bis_fnc_getparamvalue;
if (_sectorTickets>=0) then
{
	[missionnamespace,_sectorTickets] call bis_fnc_respawntickets;
};

// Add time
_sectorTime="Time" call bis_fnc_getparamvalue;
if (_sectorTime>=0) then
{
	estimatedtimeleft (estimatedendservertime-servertime+(_sectorTime*60));
};

switch (_this select 0) do
{
	case BIS_fnc_moduleSector_sector1: // Outpost
	{
		[west,"b_soldier_at_f"] call bis_fnc_addrespawninventory;
	};
	case BIS_fnc_moduleSector_sector2: // Tower
	{
		[west,"b_sniper_f"] call bis_fnc_addrespawninventory;
		[west,"b_soldier_m_f"] call bis_fnc_addrespawninventory;
	};
};

/*

sleep 3;

if (!ismultiplayer) then
{
	[player] call bis_fnc_savegame;
};

*/