// Try to fix loss of leadership with AIs

_newunit=_this select 0;
_grp=group _newunit;
if (!isplayer (leader _grp)) then
{
	[_newunit,"set_leader"] call bis_fnc_mp;
};