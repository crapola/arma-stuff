/*
	Returns positions in a building.
	Parameter:
	OBJECT - The building
	Returns:
	ARRAY - List of Position3D
*/
private ["_bpos","_p","_i"];
_bpos=[];
_p=[];
_i=0;
while {_p=_this buildingpos _i;(_p select 0)>0} do
{
	_bpos set [_i,_p];
	_i=_i+1;
};
_bpos;