/*
	Orders a group to go and occupy a building then stay in it.

 	Parameters:
	0: GROUP
	1: OBJECT - Building.

	Returns:
	Nothing.
 */
 private ["_g","_b","_rooms"];
 _g=_this select 0;
 _b=_this select 1;
_rooms=_b call bld_fnc_buildingrooms;
{
	if (_foreachindex<count _rooms) then
	{
		_x commandmove (_rooms select _foreachindex);
		_x spawn
		{
			waituntil {not unitready _this}; // Wait start moving...
			waituntil {unitready _this}; // Wait for stop.
			dostop _this;
		};
	};
} foreach units _g;