disableserialization;
_display=_this select 0;
"ParamsDialog.onLoad" call bis_fnc_log;
spp_dialog=true;
for "_index" from 1000 to 1080 step 2 do
{
	_t=_display displayctrl _index;
	if (isnull _t) exitwith {};
	_c=_display displayctrl _index+1;
	_t ctrlshow false;
	_c ctrlshow false;
};

_index=1000;
{
	_t=_display displayctrl _index;
	_c=_display displayctrl _index+1;
	_title=(_x>>"title") call bis_fnc_getcfgdata;
	_default=(_x>>"default") call bis_fnc_getcfgdata;
	_values=(_x>>"values") call bis_fnc_getcfgdata;
	_texts=(_x>>"texts") call bis_fnc_getcfgdata;
	if (isnil "_title") exitwith
	{
		["Param '%1' is missing the 'title' field.",_x] call bis_fnc_error;
	};
	if (isnil "_values") exitwith
	{
		["Param '%1' is missing the 'values[]' field.",_x] call bis_fnc_error;
	};
	if (typename _default!=typename (_values select 0)) exitwith
	{
		["Param '%1' has inconsistent types of 'values[]' and 'default'.",_x] call bis_fnc_error;
	};
	_t ctrlsettext _title;
	if (isnil "_texts") then
	{
		_texts=_values;
	};
	{
		_c lbadd format ["%1",_texts select _foreachindex];
		if (typename _x=="SCALAR") then
		{
			_c lbsetvalue [(lbsize _c-1),_x];
		}
		else
		{
			["Param '%1' is not scalar.",_x] call bis_fnc_error;
		};
		if (!isnil "_default" && {_x==_default}) then
		{
			_c lbsetcursel _foreachindex;
		};
	} foreach _values;
	_t ctrlshow true;
	_c ctrlshow true;
	_index=_index+2;
} foreach ((missionconfigfile>>"params") call bis_fnc_returnchildren);
