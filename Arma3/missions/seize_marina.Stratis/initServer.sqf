enablesaving [false,false];
/*
if (ismultiplayer) then
{
	enablesaving [false,false];
};
*/

{
	_x addmpeventhandler ["mprespawn",{_this call respawneh}];
} foreach units player_group;

// Parameters ------------------------------------------------------------------

_sectorTickets = "Respawn" call bis_fnc_getparamvalue;
if (_sectorTickets>=0) then
{
	[missionnamespace,_sectorTickets] call bis_fnc_respawntickets;
};
_sectorTime = "Time" call bis_fnc_getparamvalue;
if (_sectorTime>0) then
{
	estimatedtimeleft (_sectorTime*60);
};

// Loadouts --------------------------------------------------------------------

[west,"b_soldier_f"] call bis_fnc_addrespawninventory;
[west,"b_soldier_lite_f"] call bis_fnc_addrespawninventory;
[west,"b_soldier_gl_f"] call bis_fnc_addrespawninventory;
[west,"b_soldier_repair_f"] call bis_fnc_addrespawninventory;

// Curator ---------------------------------------------------------------------

_attackers_faction="BLU_F";
_defenders_faction="OPF_F";

// 1. Get addons according to roles
_addon_soldiers=[_defenders_faction,[0]] call tst_fnc_queryaddons;
_addon_mraps=[_defenders_faction,[2]] call tst_fnc_queryaddons;
_addon_apcs=[_defenders_faction,[3]] call tst_fnc_queryaddons;
_addon_tanks=[_defenders_faction,[4]] call tst_fnc_queryaddons;

// 2. Get unit lists from addons
_soldiers=_addon_soldiers call tst_fnc_unitsfromaddons;
_mraps=_addon_mraps call tst_fnc_unitsfromaddons;
_apcs=_addon_apcs call tst_fnc_unitsfromaddons;
_tanks=_addon_tanks call tst_fnc_unitsfromaddons;

// Remove divers
_soldiers=_soldiers-["O_diver_F","O_diver_TL_F","O_diver_exp_F"];

// 3. Build cost table
_OPFOR=[_soldiers,1] call tst_fnc_insertcosts;
_OPFOR=_OPFOR+([_mraps,1.5] call tst_fnc_insertcosts);
_OPFOR=_OPFOR+([_apcs,2] call tst_fnc_insertcosts);
_OPFOR=_OPFOR+([_tanks,3] call tst_fnc_insertcosts);

[curator,_OPFOR] call bis_fnc_curatorobjectregisteredtable;

[curator,_addon_soldiers] call bis_fnc_managecuratoraddons;
[curator,_addon_mraps,{(west call bis_fnc_modulesector)>=1}] call bis_fnc_managecuratoraddons;
[curator,_addon_apcs+_addon_tanks,{(west call bis_fnc_modulesector)>=2}] call bis_fnc_managecuratoraddons;

curator setcuratorcoef ["Place",-0.06];

// Event on placed stuff
_curatorObjectPlaced={
	_o=_this select 1;
	_veh = vehicle _o;
	// diag_log format["Placed %1 ; %2",_o,typeof _o];
	_o call kf_fnc_register;
	// Currently autocurator forgets to trigger for vehicles...
	if (!(_veh isequalto _o)) then
	{
		_veh call kf_fnc_register;//some vehs will get it more than once.
	};
	//diag_log format["Placed %1 %2 veh=%3 %4",_o,typeof _o,_veh,typeof _veh];
};
[curator,"curatorObjectPlaced",_curatorObjectPlaced] call bis_fnc_addScriptedEventHandler; // auto zeus
curator addeventhandler ["curatorObjectPlaced",_curatorObjectPlaced]; // human zeus