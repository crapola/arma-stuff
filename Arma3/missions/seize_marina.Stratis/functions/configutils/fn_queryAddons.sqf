/*
	Gives addon(s) where units matching certain properties can be found.

	Parameters:
	0: STRING - Faction class name.
	1: INTEGER[] - Flags.
		0:Man
		1:Anti-Tank
		2:Car
		3:Armored+Wheels
		4:Armored
		5:Air
	Returns:
	STRING[] - Array of addon class names.
*/
private ["_faction","_flags","_side","_predicates","_query","_vclasses","_returned","_a","_find"];

_faction=[_this,0,"",[""]] call bis_fnc_param;
_flags=[_this,1,[],[[]]] call bis_fnc_param;

if (_faction isequalto "") exitwith{[];};
if (count _flags==0) exitwith {[];};

_side="(gettext(_x>>'faction') isequalto '"+_faction+"')";

_predicates=[
"(getnumber(_x>>'isman')==1)",
"((getarray(_x>>'threat') select 1)>0.5)",
"(gettext(_x>>'vehicleclass') isequalto 'Car')",
"(gettext(_x>>'vehicleclass') isequalto 'Armored')&&(gettext(_x>>'wheelMask') isequalto 'wheel_X_X')",
"(gettext(_x>>'vehicleclass') isequalto 'Armored')",
"(gettext(_x>>'vehicleclass') isequalto 'Air')"
];

_query="(true)";
{
	_query=_query+"&&"+(_predicates select _x);
} foreach _flags;

_vclasses=(_side+"&&"+_query) configclasses (configfile>>"cfgvehicles");

_returned=[];
{
	_a=(unitaddons configname _x) select 0;
	if (!isnil "_a") then
	{
		_find=_returned find _a;
		if (_find==-1) then
		{
			_returned pushback _a;
		}
	};
} foreach _vclasses;
_returned;