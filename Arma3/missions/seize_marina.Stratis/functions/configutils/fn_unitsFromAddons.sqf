/*
	Get list of units from cfgPatches addon(s).

	Parameter:
	STRING[] - Flat array of unit names.
*/
_returned=[];
{
	_c=(configfile >> "cfgpatches" >> _x >> "units");
	[_returned,getarray _c] call bis_fnc_arraypushstack;
} foreach _this;
_returned;