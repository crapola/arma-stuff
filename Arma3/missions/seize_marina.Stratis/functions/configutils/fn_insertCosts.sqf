/*
	Insert costs.

	Parameters:
	0:STRING[] - List of unit class names.
	1:VALUE- Cost for those units.

	Returns:
	ARRAY - Cost table.
*/
private ["_array","_costs","_r"];
_array=[_this,0,[],[[]]] call bis_fnc_param;
_cost=[_this,1,1,[1]] call bis_fnc_param;
_r=[];
{
	_r pushback _x;
	_r pushback _cost;
} foreach (_this select 0);
_r;