/*
	Shows player what he killed.

	Parameters: None.
	Returns: Never.
*/
private ["_name","_t"];
kfq=[];
while {true} do
{
	waituntil {sleep 0.5;count kfq>0};
	_name=kfq call bis_fnc_arraypop;
	if (count kfq>1) then {_name=_name+".."};
	hint/*silent*/ parsetext format ["<t color='#ffff00'>You neutralized a %1.</t><br/><t size='1.2'>Score: <t color='#00ff00'>%2</t></t>",_name,rating player];
	sleep 1;
};