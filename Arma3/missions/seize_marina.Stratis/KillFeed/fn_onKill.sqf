/*
	kf_fnc_onkill
	Update local queue when unit killed.
	Parameters forwarded from MPkilled.
*/
if (isdedicated) exitwith{};
// In littblebird real gunner is driver...
_gunner=gunner (_this select 1);
_driver=driver (_this select 1);
//diag_log format ["%1 Received MPkilled %2 ; gunner=%3 ; driver=%4",player,_this,_gunner,_driver];
if ( player in [_gunner,_driver]) then
{
	private ["_name"];
	_name=gettext (configfile >> "cfgvehicles" >> typeof (_this select 0) >> "displayname");
	kfq pushback _name;
};