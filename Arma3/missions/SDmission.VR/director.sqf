/*
We can't use tasks because bot groups don't get them online.


*/

// Get list of sectors.
// a3\modules_f\multiplayer\functions\fn_moduleSector.sqf
_sectors=[true] call bis_fnc_modulesector;
diag_log _sectors;
{
_areas = _x getvariable ["areas",[]];
diag_log _areas;
{
diag_log position _x
} foreach _areas;

diag_log (_x getvariable ["sides","..."]);
diag_log (_x getvariable ["owner","..."]);
} foreach _sectors;

_getClosestEnemySectorPos=
{
	_side=side _this;
	_dist=9000;
	_position=[];
	{
		if ( (_x getvariable "owner")!=_side) then
		{
			_sp=position ((_x getvariable "areas") select 0);
			_d=_this distance2D _sp;
			if (_d<_dist) then {_position=_sp;_dist=_d;};
		};
	} foreach _sectors;
	_position;
};

while {true} do
{
	{
		sleep 4;
		_leader=leader _x;
		if (!isplayer _leader) then
		{
			_p=_leader call _getClosestEnemySectorPos;
			if !(_p isequalto []) then
			{
				_x move _p;
				hint format ["%1 moves to %2",_x,_p];
			};
		};
	} foreach allgroups;
};

/*
_selectNewTask=
{
	_u=_this;
	_tasks=simpletasks _l;
	_ok=false;
	{
		if (!taskcompleted _x) exitwith
		{
			_u setCurrentTask _x;
			_ok=true;
		};
	} foreach _tasks;
	if (!_ok) then
	{
		hint format ["No task left for %1",group _u];
	};
};

while {true} do
{
	{
		sleep 4;
		_l=leader _x;
		if (!isplayer _l) then
		{
			_t=currenttask _l;
			if (!isnull _t) then
			{
				if (!taskcompleted _t) then
				{
					_dest=taskdestination _t;
					_x move _dest;
					hint format ["%1 moves to %2 at %3",_x,taskdescription _t select 1,_dest];
				} else
				{
					hint format ["Select new task for %1",_x];
					_l call _selectNewTask;
				};
			};
		};
	} foreach allgroups;
}*/