#include "initBriefing.sqf"
#include "initUAV.sqf"

setviewdistance (2000 min viewdistance);

[player, "Altis - Stadium",100,50,0,1,
[["\a3\ui_f\data\map\markers\nato\b_air.paa",east call bis_fnc_sidecolor,player,1,1,0,"",0]]
]
spawn bis_fnc_establishingshot;

// Airfield alarm
[] spawn
{
	waituntil{sleep 5;!(alive m5_1&&alive m5_2&&alive m5_3)};
	alarm=true;
};

// Setup spy
_old_group=group spy;
_new_group=creategroup west;
_new_group copywaypoints _old_group;
[spy] join _new_group; // Needs to be after copy
deletegroup _old_group;
sleep 1;
removeheadgear spy;
spy addheadgear "H_Hat_checker";
[] spawn
{
	waituntil{sleep 5;(!alive spy && !alive spy_car)};
	["spy","succeeded"] call bis_fnc_missiontasks;
};


