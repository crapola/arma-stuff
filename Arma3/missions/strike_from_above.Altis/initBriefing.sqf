player creatediaryrecord ["diary",["Intel",
"M5 Sandstorm MLRS<br/>
<img image='intelM5.paa' width='200' height='200'/><br/>
The spy<br/>
<img image='intelSpy.paa' width='200' height='200'/>
"
]];
player creatediaryrecord ["diary",["Signal",
"<marker name='m_base'>Griffin</marker> - Your team.<br/>\
<marker name='m_recon'>Viper Black</marker> - Recon."
]];
player creatediaryrecord ["diary",["Support",
"You can return to <marker name='m_base'>base</marker> anytime you need to resupply or repair."
]];
player creatediaryrecord ["diary",["Execution",
"You will move to <marker name='m_firepos'>Amoni peak</marker> and elevate your 
helicopter to an altitude of about 
500 m to make sure your missiles have a clear path.<br/>
Once you are in position, you may enable auto-hover and switch to the \
Tayran AR-2 UAV.<br/>\
This UAV is able to remotely file the Kajman's Skalpel ATGM missiles. \
To do so, take control of the turret, enable the laser designator and use \
the ""Fire missile"" action."
]];
player creatediaryrecord ["diary",["Mission",
"Your main objective is to destroy three M5 Sandstorm MLRS. They are 
stationed somewhere near the <marker name='m_airport'>Altis airport</marker> for maintenance.<br/>
<br/>
During your operation an enemy spy will be visiting a contact at the airport. 
He is known to drive a sport hatchback. Try to eliminate both 
him and his car, for the latter might contain compromising documents. He will 
only be there for a short time, so be quick."
]];