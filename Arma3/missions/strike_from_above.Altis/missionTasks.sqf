case "move":
{
	if (!_taskexists)  then
	{
		[
			player,
			_taskid,
			[
				"Move your Mi-48 Kajman to a good <marker name='m_firepos'>firing position</marker>.",
				"Move to position",
				"MOVE"
			],
			getpos fire_position, //dest
			true //state or bool set current
		] call bis_fnc_taskcreate
	} else
	{
		[_taskid,"succeeded"] call bis_fnc_tasksetstate;
	};
};

case "destroy":
{
	if (!_taskexists)  then
	{
		[
			player,
			_taskid,
			[
				"Locate and destroy the three M5 Sandstorm MLRS.",
				"Destroy the artillery",
				nil
			],
			nil,
			false
		] call bis_fnc_taskcreate
	} else
	{
		[_taskid,"succeeded"] call bis_fnc_tasksetstate;
	};
};

case "uav":
{
	if (!_taskexists)  then
	{
		[
			player,
			_taskid,
			[
				"Do not lose the UAV before your main objective is completed.",
				"Keep UAV intact",
				nil
			],
			nil,
			false
		] call bis_fnc_taskcreate
	} else
	{
		[_taskid,_this] call bis_fnc_tasksetstate;
	};
};

case "rtb":
{
	if (!_taskexists)  then
	{
		[
			player,
			_taskid,
			[
				"Land on the <marker name='m_base'>helipad</marker> and turn off your engine to complete the mission.",
				"Return to base",
				"LAND"
			],
			"m_base",
			true
		] call bis_fnc_taskcreate
	} else
	{
		[_taskid,"succeeded"] call bis_fnc_tasksetstate;
	};
};

case "spy":
{
	if (!_taskexists)  then
	{
		[
			player,
			_taskid,
			[
				"His car must burn as well.",
				"Eliminate spy (optional)",
				nil
			],
			nil,
			false
		] call bis_fnc_taskcreate
	} else
	{
		[_taskid,_this] call bis_fnc_tasksetstate;
	};
};