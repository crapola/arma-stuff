disableserialization;
_display=uinamespace getvariable "UAV_display";
(_display displayctrl 1) ctrlshow false;

_objpos_to_ui=
{
	private ["_pos"];
	_pos=worldtoscreen _this;
	if (2==count _pos) then // Valid, facing
	{
		_pos set [0,0 max (_pos select 0)];
		_pos set [1,0 max (_pos select 1)];
		_pos set [0,1 min (_pos select 0)];
		_pos set [1,1 min (_pos select 1)];
		_pos;
	} else // Invalid so we calc
	{
		_turret_dir=the_uav weapondirection "Laserdesignator_mounted";
		_vector=[(position the_uav select 0)-(_this select 0),(position the_uav select 1)-(_this select 1),_this select 2];
		_vu=_vector call bis_fnc_unitvector;
		_at=( (_vu select 1) atan2 (_vu select 0))-((_turret_dir select 1) atan2 (_turret_dir select 0));
		_pos set [0,_at/180+0.5];
		_pos set [0,0 max (_pos select 0)];
		_pos set [0,1 min (_pos select 0)];
		_pos set [1,1];
		_pos;
	};
};

while {true} do
{
	_scalpel_count=call
	{
		if (magazinesammo vehicle player select 2 select 0!="8Rnd_LG_Scalpel")
		then {0;}
		else {magazinesammo vehicle player select 2 select 1;};
	};
	(_display displayctrl 0) ctrlsettext "SKALPEL: "+str(_scalpel_count);

	if (!isnull inbound_missile) then
	{
		(_display displayctrl 1) ctrlshow true;
		(_display displayctrl 3) ctrlshow true;
		_dist=call
		{
			_tgt=lasertarget the_uav;
			if (!isnull _tgt) then
			{
				str(round ([inbound_missile,lasertarget the_uav] call bis_fnc_distance2D))+" m";
			} else {"NO TARGET"};
		};
		(_display displayctrl 1) ctrlsettext format ["MISSILE INBOUND\nDIST: %1",_dist];
		// Draw
		(_display displayctrl 3) ctrlsetposition ((position inbound_missile) call _objpos_to_ui);
		(_display displayctrl 3) ctrlcommit 0.25;
	} else
	{
		(_display displayctrl 1) ctrlshow false;
		(_display displayctrl 3) ctrlshow false;
	};

	_helipos=(position player) call _objpos_to_ui;

	(_display displayctrl 2) ctrlsetposition _helipos;
	(_display displayctrl 2) ctrlcommit 0.25;

	sleep 0.25;
};