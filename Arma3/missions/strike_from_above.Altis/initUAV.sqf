_fire_ze_missile=
{
	_ld=lasertarget the_uav;
	group player reveal [_ld,4];
	gunner (vehicle player) dotarget _ld;
	gunner (vehicle player) fireattarget [_ld,"missiles_SCALPEL"];
};

// UAV display loop
[] spawn
{
	while {true} do
	{
		waituntil {sleep 0.125;uavcontrol the_uav select 1=="GUNNER" && cameraview=="GUNNER" && !visiblemap};
		0 cutrsc ["MissileInfo","plain"];
		_loop=execvm "UAVLoop.sqf";
		waituntil {uavcontrol the_uav select 1!="GUNNER" || cameraview!="GUNNER" || visiblemap};
		0 cutfadeout 0;
		terminate _loop;
	};
};

_shot=
{
	if (typeof(_this select 6)=="M_SCALPEL_AT") then
	{
		inbound_missile=_this select 6;
	};
};

player addweapon "o_uavterminal";
player connectterminaltouav the_uav;
the_uav addaction
[
	"Fire missile",
	_fire_ze_missile,
	nil, // arguments
	5, // priority
	true, // show
	false, // hide on use
	"", // shortcut
	"uavcontrol the_uav select 1=='GUNNER' && magazinesammo vehicle player select 2 select 0=='8Rnd_LG_Scalpel'"
];
inbound_missile=objnull;
(vehicle player) addeventhandler ["Fired",_shot];