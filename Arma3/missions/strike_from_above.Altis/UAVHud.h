#define GUI_GRID_W	((((safezoneW/safezoneH) min 1.2)/40))
#define GUI_GRID_H	(((((safezoneW / safezoneH) min 1.2)/1.2)/25))
#define GW(x) ((x)*GUI_GRID_W)
#define GH(x) ((x)*GUI_GRID_H)

class RscTextUAV
{
	access = 0;
	type = 0;
	idc = -1;
	colorBackground[] = { 0,0,0,0 };
	colorText[] = { 1,1,1,1 };
	text = "";
	fixedWidth = 0;
	x = 0;
	y = 0;
	h = GH(1);
	w = 0.3;
	style = 0;
	shadow = 0;
	colorShadow[] = { 0,0,0,0.5 };
	font = "EtelkaMonospacePro";
	sizeEx = "0.02*SafezoneH";
	linespacing = 1;
};

class RscPicture
{
	access = 0;
	type = 0;
	idc = -1;
	style = 48;
	colorBackground[] = { 0,0,0,0 };
	colorText[] = { 1,1,1,1 };
	font = "TahomaB";
	sizeEx = 0;
	lineSpacing = 0;
	text = "";
	fixedWidth = 0;
	shadow = 0;
	x = 0.5;
	y = 0.5;
	w = 0.2;
	h = 0.15;
};

class RscTitles
{
	class MissileInfo
	{
		idd=11;
		duration=6000;
		fadein=0;
		fadeout=0;
		onload="uinamespace setvariable ['UAV_display',_this select 0]";

		class Controls
		{
			class Text: RscTextUAV
			{
				idc=0;
				text="SCALPEL: 8";
				x=0;
				y=0;
				w=GW(10);
			};
			class Dist: RscTextUAV
			{
				idc=1;
				style=0x10+0x200;
				text="MISSILE INBOUND\nDIST: 0000 m";
				x=0;
				y=GH(1);
				w=GW(10);
				h=GH(2);
			};
			class GunshipPos: RscPicture
			{
				idc=2;
				text="\a3\ui_f\data\map\VehicleIcons\iconhelicopter_ca.paa";
				w=GW(2);
				h=GH(2);
			};
			class MissilePos: RscPicture
			{
				idc=3;
				text="\a3\ui_f\data\map\Markers\Military\circle_ca.paa";
				w=GW(2);
				h=GH(2);
			};
		};
	};
};